Topocule
--------

*Warning: you should switch the git branch to develop (ironically that's where the maintenance is happening actively) unless you know what you are doing.*

*Topocule* provides reusable components for analyzing
data mainly from a topological perspective.
It takes into account rather-high dimensional (4 to 10D) topological analysis.

This software therefore provides data structures for
storing arbitrary dimensional topology and geometry.
And it tries to do so in a flexible manner where I can
experiment with different internals quickly with different dimensions.
The code also generally does not assume a specific dimensionality
of the geometrical and topological objects.
Sometimes it even targets non-manifolds that are 4-D in some region
while 2-D in another.

Unfortunately, these prevented me from using mainframes from
established libraries I am familiar with.
I don't use VTK's beautiful pipeline system, nor TTK's powerful triangulation data flow,
although topocule uses these libraries as much as I see fit.

Installation
------------

### Download Topocule
~~~
$ git clone url_to_topocule
$ cd topocule
$ git submodule update --init --recursive .
~~~

We assume you create a directory named `build` under the toopcule project root.
(The build directory can be actually anywhere as long as you adjust the paths properly.)
~~~
$ mkdir build 
~~~

### Installing Dependencies

Dependencies that weren't trivial to be included 
are not included in this project.
If you build the GUI, you must install Qt5 and OpenSceneGraph
even if you choose to use the Conan package manager as described below.

You can use the conan package manager to install other dependencies.
Those are currently Boost, GMP, MPFR and Eigen3.
You can also install them on your own and pass it to CMake.
If you fail to compile due to missing dependencies you may consult `conanfile.py` .

If you choose to use the conan package manager, install it now, e.g.
~~~
$ pip3 install --user conan
~~~

Then install the dependencies:
~~~
$ cd build
$ conan install .. --profile=<PROFILE>
~~~
Here, replace <PROFILE> with an appropriate profile for your system.
Mac users with clang 11.0 can use `../resources/conan/profiles/macos_x86_64_clang_11_0`
If you are not using Mac or want to use a different compiler, create a new profile by editing that file and/or
consult the [official documentation](https://docs.conan.io/en/latest/reference/profiles.html).

On macOS, some conan packages have bugs in rpath support.
Indeed, conan's default approach seems to be to set the binary directory (`build/bin`) into
the `DYLD_LIBRARY_PATH` environment path.
(Also, somehow `bin` is preferred over `lib` by the Conan community...)
This is unfortunate but a good fix is currently out of scope for topocule.

As GUI apps cannot rely on `DYLD_LIBRARY_PATH`, we may provide some kind of work around
in the near future.

### Installing Topocule

From the build directory,
~~~
$ cmake path_to_topocule <FLAGS>
~~~
Replace <FLAGS> with appropriate flags.
It should contain `-DCMAKE_BUILD_TYPE=RelWithDebInfo` if you want to use topocule for release.
If you are using Mac and the clang shipped with XCode, there's no easy OpenMP support.
Therefore you can set `-DTPCL_ENABLE_OPENMP=No`.

Then build the system.
~~~
$ make
~~~


On License
----------

Topocule with GPL-ed components is GPL-protected just because
they depend on external GPL-licensed components.
You can redistribute any part or entirety of topocule as GPL-free
after you have removed such dependencies to GPL appropriately.

Notes on the Code – There Are Reasons Why My Code Appears Strange
-----------------

I have to maintain this code project almost always alone in a restricted
amount of time.
Yet, code for topological analysis tends to be very complex...
It involves expert-traditions established in more than a decade,
coming from visualization, computer graphics, and computational geometry,
which are all tricky and distinct in their own ways.

To proceed efficiently, the code became selfish and
thus a bit unkind when you have to read the code.

Another weirdness of this code is that it takes advantage of modern compilers'
optimization techniques as well as recent C++'s standards to keep the code easier
for me to read.
One of the examples that may outrage old-school C++ programmers is the *return-value optimization*,
in which a function returns memory-heavy objects *by value* without
any performance penalty.
And, of course, I take advantage of the `auto`s disliked by traditional coders,
and templates and C++20 concepts and such.

The code assumes that you use IDEs to deep-dive in the source.
This is because I find it costly to maintain a specific coding convention
when I am basically the guy who writes the code.
I want to spend time mainly on proceeding and testing.

Optimization is also not attempted enthusiastically. 
As research code to deliver poof-of-concepts,
the focus is more in readablity and ease of maintenance (by me).

As high-dimensional data structures easily
suffer from the curse of dimensionality,
the code prefers flexibility of the memory representation of data
so that application-specific performance bottlenecks can be
relaxed by changing the internals.

Topocule encourages the separation of concern.
There are several classes *concerned* about realizing its own
mathematical concept.
For example, a field is broken into smaller concepts
like field values and tesselation, the latter broken down into
the connectivity of cells and the list of cells in each dimension.
