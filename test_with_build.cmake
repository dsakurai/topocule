# Build the dependencies before executing tests
function(add_ctest_target_with_build)

    # Parse keyword arguments
    set(options "")
    set(oneValueArgs NAME BUILD WORKING_DIRECTORY)
    # The COMMAND option is the test command, basically the executable.
    set(multiValueArgs COMMAND)
    cmake_parse_arguments(add_ctest_target_with_build "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    # Actually add the test
    if (add_ctest_target_with_build_WORKING_DIRECTORY)
        # todo: we can use generator expression to evaluate if add_ctest_target_with_build_WORKING_DIRECTORY is set.
        add_test(NAME ${add_ctest_target_with_build_NAME} COMMAND ${add_ctest_target_with_build_COMMAND} WORKING_DIRECTORY ${add_ctest_target_with_build_WORKING_DIRECTORY})
    else()
        add_test(NAME ${add_ctest_target_with_build_NAME} COMMAND ${add_ctest_target_with_build_COMMAND})
    endif()

    # Build the dependencies if specified by the user
    if (add_ctest_target_with_build_BUILD)
        # This "test" builds a dependency
        add_test("${add_ctest_target_with_build_NAME}_build"
                "${CMAKE_COMMAND}"
                --build "${CMAKE_BINARY_DIR}"
                --config $<CONFIG>
                --target "${add_ctest_target_with_build_BUILD}"
                )

        # Use the test fixture to trigger the build of dependencies
        set_tests_properties("${add_ctest_target_with_build_NAME}"
                PROPERTIES FIXTURES_REQUIRED "${add_ctest_target_with_build_NAME}_fixture"
                )
        set_tests_properties("${add_ctest_target_with_build_NAME}_build"
                PROPERTIES FIXTURES_SETUP    "${add_ctest_target_with_build_NAME}_fixture"
                )
    endif (add_ctest_target_with_build_BUILD)

endfunction(add_ctest_target_with_build)
