
if (TPCL_ENABLE_TESTS)
    add_subdirectory(TestTools)
endif (TPCL_ENABLE_TESTS)

add_subdirectory(Backend)
add_subdirectory(Frontend)
