
#get_filename_component(Lib ${CMAKE_CURRENT_SOURCE_DIR} NAME)

add_library(May_handle_simplicial_complex)

target_include_directories(May_handle_simplicial_complex PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")

target_sources(May_handle_simplicial_complex
        PUBLIC
        May_handle_simplicial_complex.h
        May_handle_simplicial_complex.cpp
        )
