//
// Created by Daisuke Sakurai on 2020-08-23.
//

#include <May_handle_simplicial_complex.h>

#include <stdexcept>

void tpcl::May_handle_simplicial_complex::verify_simplicial_complex_quickly() const {
    if (!is_for_simplicial_complex()) throw Not_simplex_error {"Not a triangulation."};
}

bool tpcl::Has_another_simplicial_complex_handler::delegate_simplicial_complex_verification(May_handle_simplicial_complex *another) const {
    if(!another) throw std::runtime_error{"Could not access the simplicial complex handler."};
    return another->is_for_simplicial_complex();
}
