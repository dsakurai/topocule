//
// Created by Daisuke Sakurai on 2020-08-23.
//

#ifndef TOPOCULE_MAY_HANDLE_SIMPLICIAL_COMPLEX_H
#define TOPOCULE_MAY_HANDLE_SIMPLICIAL_COMPLEX_H

#include <stdexcept>

namespace tpcl {

    /**
     * An interface for classes that may handle simplicial complexes.
     * Examples include `Cell_incidence`, `Tessellation` and `Multi_fields`.
     *
     */
    class May_handle_simplicial_complex {
    public:
        /**
         *  Throw on tpcl::May_handle_simplicial_complex::Not_simplex_error error if the tessellation is not triangulation.
         */
        void verify_simplicial_complex_quickly() const;

        class Not_simplex_error: public std::runtime_error {
        public:
            explicit
            Not_simplex_error (const char* message ): std::runtime_error {message} {
            }
        };

    protected:
        /**
         * Same as `verify_simplicial_complex_quickly`,
         * but return a bool instead.
         * We prefer exceptions, therefore this method should be called only when necessary,
         * and only by subclasses of `May_handle_simplicial_complex`.
         */
        virtual bool is_for_simplicial_complex() const = 0;

        /**
         * Classes who inherits this class can another instance.
         * Especially important for `is_for_simplicial_complex`.
         * For this to work, this class must have no data member.
         * Otherwise, the friend can modify data...
         *
         */
        friend class Has_another_simplicial_complex_handler;
    };

    /**
     * This is for
     *
     * Use it like:
     * ~~~
     * class User:
     *     public May_handle_simplicial_complex,
     *     public Has_another_simplicial_complex_handler
     *     {
     * public:
     *    bool is_for_simplicial_complex() const override {
     *        return delegate_simplicial_complex_verification(&simplicial_complex);
     *    }
     * private:
     *     Another simplicial_complex;
     * };
     * ~~~
     */
    class Has_another_simplicial_complex_handler {
    public:
        /**
         * Subclasses can return bool by using this
         * @tparam T
         * @param another
         * @return
         */
        bool delegate_simplicial_complex_verification(May_handle_simplicial_complex* another) const;
    };

    /**
     * Use it like:
     * ~~~
     *  class User:
     *      public May_handle_simplicial_complex,
     *      public Simplicial_complex_validator_quick {
     *  private:
     *      bool is_for_simplicial_complex() const override {
     *          return Simplicial_complex_validator_quick::is_for_simplicial_complex();
     *      }
     *  };
     * ~~~
     *
     * Currently this class is not a subclass of `May_handle_simplicial_complex`.
     * This is conceptually weird, but avoids diamond inheritance
     * to simplify debugging.
     */
    class Simplicial_complex_validator_quick {
    public:
        explicit
        Simplicial_complex_validator_quick (bool for_simplicial_complex) :
                for_simplicial_complex{for_simplicial_complex}
        {}

        void set_for_simplicial_complex(bool yes_or_no) {
            for_simplicial_complex = yes_or_no;
        }

        [[nodiscard]]
        bool is_for_simplicial_complex() const {
            return for_simplicial_complex;
        }
        bool for_simplicial_complex = false;
    };

}

#endif //TOPOCULE_MAY_HANDLE_SIMPLICIAL_COMPLEX_H
