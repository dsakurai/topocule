//
// Created by Daisuke Sakurai on 2020-08-23.
//

#include <Compute_incidence_for_simplices.h>
#include <Incidence_outputs.h>
#include "internals.h"

#include <Basics.h>
#include <Cell_incidence.h>
#include <Tessellation.h>

#include <discreture.hpp>
#include <set>
#include <tuple>

namespace {
    struct Containers: public std::tuple<
            std::vector<tpcl::Cell_id> > {
        enum container_name {
            intersection
        };
    };
}

namespace tpcl {

    template<class Container_a, class Container_b, class Out_container>
    void set_intersection(const Container_a& container_a, const Container_b& container_b, Out_container& out) {
        using element_type = typename Out_container::value_type;

        std::vector<element_type> copy_a, copy_b;
        tpcl::copy_from_beginning(container_a, copy_a);
        tpcl::copy_from_beginning(container_b, copy_b);

        std::sort(copy_a.begin(), copy_a.end());
        std::sort(copy_b.begin(), copy_b.end());

        std::set_intersection(
                copy_a.begin(), copy_a.end(),
                copy_b.begin(), copy_b.end(),
                std::back_inserter(out));
    }

    namespace Compute_incidence_for_simplices {

        void get_face_vertices(
                const std::vector<Cell_id>& fullcell_2_vertices,
                const std::vector<int>& face,
                std::vector<Cell_id>& face_vertices) {
            face_vertices.resize(0);

            for (const auto& v: face) {
                face_vertices.push_back(fullcell_2_vertices[v]);
            }
        }

        /**
         * The dimension of faces must be that of cofaces minus 1.
         * @param face_to_coface
         * @return
         */
        void compute_adjacency_for_simplices(
                const Multi_vector<2, Cell_id> &face_to_coface,
                std::set<std::pair<Cell_id, Cell_id>>& adjacencies
        ) {
            for (auto& face: face_to_coface) {
                for (auto combi: discreture::combinations(face.size(), 2)) {
                    adjacencies.emplace(face[combi[0]], face[combi[1]]);
                }
            }
        }

        std::shared_ptr<Cell_adjacency_in_manifold> compute_adjacency_for_simplices(
                const Tessellation &nd_tessellation,
                int dim_cell,
                Incidence_outputs& outputs
                ) {
            const auto& incidence = *nd_tessellation.get_cell_incidence();
            const int dim_minus_1 = dim_cell - 1;

            incidence.verify_simplicial_complex_quickly();

            // If faces are not available
            if (! incidence.is_incidence_set(dim_cell, dim_minus_1)) {
                if (! incidence.is_incidence_set(dim_minus_1, dim_cell)) {

                    // create temporary storage even if the back-link is not requested.
                    // TODO better done with shared pointers.
                    outputs.get_out_or_temporary(dim_minus_1, dim_cell);

                    if (!incidence.is_incidence_set(dim_minus_1, 0)) {
                        // Compute the faces
                        create_faces_for_simplices_using_vertices(
                                nd_tessellation,
                                dim_cell,
                                dim_minus_1,
                                outputs
                        );
                    } else {
                        // Using pre-set incidence: currently not supported.
                        throw std::runtime_error{"Face vertices already set."};
                    }
                } else {
                    throw std::runtime_error{"Co-faces already set."};
                }
            } else {
                // Using pre-set incidence: currently not supported.
                throw std::runtime_error{"Faces already set."};
            }

            auto* face_to_coface = outputs.get(dim_minus_1, dim_cell);
            if (!face_to_coface) throw std::runtime_error {"Co-face to face not found. This is a bug in `compute_adjacency_for_simplices`"};

            std::set<std::pair<Cell_id, Cell_id>> adjacencies_as_set;
            compute_adjacency_for_simplices(*face_to_coface, adjacencies_as_set);

            auto adjacency = std::make_shared<Cell_adjacency_in_manifold>(
                    dim_cell,
                    nd_tessellation
            );
            adjacency->set_is_sorted(true);

            // Copy the output
            for (auto[c0, c1]: adjacencies_as_set) adjacency->insert_adjacency(c0, c1);

            return adjacency;
        }

        namespace details {

            /**
             * Check if a cell is recorded in the incidence.
             *
             * @param vertices_sorted: cell made of (0)-faces, with their IDs sorted.
             * @param cells: find the cell in this data structure
             *
             */
            bool recorded(const details::Face &vertices_sorted,
                              const Face_vertices_set &cells,
                              typename Face_vertices_set::const_iterator& pos
            ) {

                // try inserting the face
                pos = cells.lower_bound(vertices_sorted);
                return pos != cells.end() && (pos->face_vertices == vertices_sorted.face_vertices);
            }
        }

        void create_faces_for_simplices_using_vertices(
                const Tessellation &tessellation,
                int dim_coface,
                int dim_face,
                Incidence_outputs& outputs
                ) {
            using namespace details;

           tessellation.verify_simplicial_complex_quickly();

            const int dim_0 = 0;
            const Cell_incidence& incidence = *tessellation.get_cell_incidence();
            incidence.verify_simplicial_complex_quickly();

            if (!incidence.is_incidence_set(dim_coface, dim_0)) throw std::runtime_error {"Co-face vertices are unknown."};

            if (incidence.is_incidence_set(dim_face, dim_0)) throw Exception_output_dirty {"incidence for face " + std::to_string(dim_face) + " -> vertex : output already set."};
            if (incidence.is_incidence_set(dim_coface, dim_face)) throw Exception_output_dirty {"incidence for coface" + std::to_string(dim_coface) + " -> " + std::to_string(dim_face) + " : output already set."};

            // get fullcells
            const Cell_id num_higher_cells = incidence.get_number_of_incidence(dim_coface, dim_0);
            const Cell_id num_higher_cell_vertices = dim_coface + 1;
            const Cell_id num_newface_vertices = dim_face + 1;

            Face_vertices_container face_vertices;
            Face_vertices_set faces;

            //
            // Compute faces and keep it in a temporary storage
            // specialized for querying
            //
            // for each higher-dimensional cell
            for (Cell_id high_cell_id = 0; high_cell_id < num_higher_cells; ++high_cell_id) {
                // vertices of the cell
                auto& higher_dim_cell_vertices = incidence.get_incidence(dim_coface, dim_0, high_cell_id);

                // for its faces
                for (auto& face_as_combination: discreture::combinations(num_higher_cell_vertices, num_newface_vertices)) {
                    face_vertices.clear();

                    // get the face as its vertices
                    get_face_vertices(higher_dim_cell_vertices, face_as_combination, face_vertices);

                    std::sort(face_vertices.begin(), face_vertices.end());

                    Face_vertices_set::iterator face_pos;

                    // Record the face with its coface (the higher cell)

                    details::Face face {face_vertices, Face_vertices_container{}};

                    if (!recorded(face, faces, face_pos)) {
                        face_pos = faces.insert(face_pos, face);
                    }
                    face_pos->cofaces.emplace_back(high_cell_id);
                }
            }

            //
            // Save the cell incidence in the output data structure
            // specialized for random access
            //
            auto for_faces = [&faces](auto&& func) {
                Cell_id face_id = 0;
                for (auto& face: faces) {
                    func(face_id, face);

                    // Next face
                    ++face_id;
                }
            };
            // This invalidates face.face_vertices due to std::move
            if (auto out = outputs.get(dim_face, dim_0)) {
                for_faces([&](Cell_id face_id, auto &face) {

                    // TODO we may do this in one resize call
                    if (out->size() <= face_id) out->resize(face_id + 1);

                    (*out)[face_id] = std::move(face.face_vertices);
                });
            }
            if (auto out = outputs.get(dim_coface, dim_face)) {
                for_faces([&](Cell_id face_id, auto &face) {

                    // TODO we may do this in one resize call
                    for (Cell_id& coface: face.cofaces) {
                        if (out->size() <= coface) out->resize(coface + 1);
                        (*out)[coface].emplace_back(face_id);
                    }

                });
            }
            if (auto out = outputs.get(dim_face, dim_coface)) {
                for_faces([&](Cell_id face_id, auto &face) {
                    // TODO we may do this in one resize call
                    if (out->size() <= face_id) out->resize(face_id + 1);

                    (*out)[face_id] = std::move(face.cofaces);
                });
            }
        }
    }
}
