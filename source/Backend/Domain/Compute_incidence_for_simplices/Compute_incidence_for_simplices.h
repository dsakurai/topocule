//
// Created by Daisuke Sakurai on 2020-08-23.
//

#ifndef TOPOCULE_COMPUTE_INCIDENCE_FOR_SIMPLICES_H
#define TOPOCULE_COMPUTE_INCIDENCE_FOR_SIMPLICES_H

#include <Cell_adjacency_in_manifold.h>

#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace tpcl {
    class Tessellation;
    class Cell_incidence;
    class Incidence_outputs;

    /**
     * There are two kinds of sorting:
     * 1. Faces (technically their IDs) of each cell
     * 2. Cells stored in the container
     *
     * Cells in the container, when sorted, are done so according to the tuple of face IDs.
     * The lexicographical of the tuples are used for this sorting.
     *
     * The rule of thumb: only the highest cell's incidence to vertices needs *no* pre-sorting.
     * See each method's documentation.
     */
    namespace Compute_incidence_for_simplices {
        void create_faces_for_simplices_using_vertices(
                const Tessellation &tessellation,
                int dim_coface,
                int dim_face,
                Incidence_outputs& outputs
                );

        inline void create_faces_for_simplices_using_vertices(
                const Tessellation &tessellation,
                int dim_coface,
                const int dim_face,
                Incidence_outputs&& outputs
        ) {
            return create_faces_for_simplices_using_vertices(
                    tessellation,
                    dim_coface,
                    dim_face,
                    outputs
                    );
        }
        /**
         *
         * @param nd_tessellation
         * @param dim_cell: dimension of the cell to compute the adjacency for.
         * @param both_directions
         *
         * Cells of dimension dim_cell do not have to be sorted.
         */
        [[nodiscard]]
        std::shared_ptr<Cell_adjacency_in_manifold> compute_adjacency_for_simplices(
                const Tessellation &nd_tessellation,
                int dim_cell,
                Incidence_outputs& outputs
        );
        [[nodiscard]]
        inline std::shared_ptr<Cell_adjacency_in_manifold> compute_adjacency_for_simplices(
                const Tessellation &nd_tessellation,
                int dim_cell,
                Incidence_outputs&& outputs
        ) {
            return compute_adjacency_for_simplices(
                    nd_tessellation,
                    dim_cell,
                    outputs
            );
        }

        class Exception_output_dirty: std::runtime_error {
        public:
            Exception_output_dirty (std::string message):
                    std::runtime_error{message}
            {}
        };
    }
}

#endif //TOPOCULE_COMPUTE_INCIDENCE_FOR_SIMPLICES_H
