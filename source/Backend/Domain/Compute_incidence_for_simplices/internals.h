//
// Created by Daisuke Sakurai on 2020-09-22.
//

// Expose some names and such mainly for tests

#ifndef TOPOCULE_INTERNAL_H
#define TOPOCULE_INTERNAL_H

#include <set>

namespace tpcl {
    namespace Compute_incidence_for_simplices {
        namespace details {
            using Face_vertices_container = std::vector<Cell_id>;
            using Cofaces = std::vector<Cell_id>;

            struct Face {
                // Actual identity of the face
                Face_vertices_container face_vertices;

                // Mutable because this doesn't matter for the identity of the face
                mutable Cofaces cofaces;

                bool operator==(const Face& other) const {
                    return this->face_vertices == other.face_vertices;
                }
                bool operator!=(const Face& other) const {
                    return !(*this == other);
                }

                bool operator<(const Face& other) const {
                    return this->face_vertices < other.face_vertices;
                }
            };

            using Face_vertices_set = std::set<Face>;

            bool recorded(const details::Face &vertices_sorted,
                          const Face_vertices_set &cells,
                          typename Face_vertices_set::const_iterator& pos
            );
        }
    }
}

#endif //TOPOCULE_INTERNAL_H
