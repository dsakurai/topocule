//
// Created by Daisuke Sakurai on 2020-09-20.
//

#include <Basics.h>
#include <Compute_incidence_for_simplices.h>
#include <Incidence_outputs.h>

#include <test.h>

#include "internals.h"

std::ostream& operator<<(std::ostream& os, const tpcl::Cell_adjacency_in_manifold& a)
{
    for (auto pair: a) {
        os << pair;
    }
    return os;
}

TEST_CASE ("Test Compute_incidence_for_simplices") {
    using namespace tpcl;
    using namespace Example_dataset_reader;

    SUBCASE ("Test small functions") {

        using Compute_incidence_for_simplices::details::Face;
        using Compute_incidence_for_simplices::details::Face_vertices_set;

        Face_vertices_set faces_in_set = {
                Compute_incidence_for_simplices::details::Face {
                        .face_vertices = {0, 1, 2},
                        .cofaces = {100, 101} // this should be ignored
                },
                Compute_incidence_for_simplices::details::Face {
                        .face_vertices = {0, 2, 3},
                        .cofaces = {}
                },
        };

        Face face;
        Face_vertices_set::const_iterator position;
        WHEN("Face exists"){
            face = {
                .face_vertices = {0, 1, 2},
                .cofaces = {}
            };
            bool found = Compute_incidence_for_simplices::details::recorded(face, faces_in_set, position);
            CHECK(found);
            CHECK(position != faces_in_set.end());
            CHECK(*position == face);
        };
        WHEN("Face does not exist"){
            face = {
                    .face_vertices = {0, 1, 9999},
                    .cofaces = {}
            };
            bool found = Compute_incidence_for_simplices::details::recorded(face, faces_in_set, position);
            CHECK(!found);
            CHECK(position != faces_in_set.end());
            CHECK(*position != face);
        }
    }

    std::shared_ptr<Tessellation> tessellation;

    Incidence_outputs incidence_outputs {};

    SUBCASE ("Test un-supported dimensions") {
        tessellation = std::make_shared<Tessellation>();
        tessellation->set_maximal_dimension(dim<1>);
        auto incidence = std::make_shared<Cell_incidence>(tessellation->get_maximal_dimension(), /*simplicial*/true);
        incidence->insert_incidence(dim<1>, dim<0>, cell<0>, cell<0>);
        tessellation->set_cell_incidence(incidence);

        CHECK_THROWS_AS(Compute_incidence_for_simplices::create_faces_for_simplices_using_vertices(
                *tessellation,
                dim<1>,dim<0>,
                incidence_outputs
                ), Compute_incidence_for_simplices::Exception_output_dirty);
    }

    const std::string config = TWO_TETS_VTK_CONFIG;
    tessellation = read_example_dataset(config)->get_tessellation();
    std::shared_ptr<Cell_incidence> incidence = tessellation->get_cell_incidence();

    SUBCASE ("Test face generation") {

        WHEN ("Initializing simplices") {

            REQUIRE(incidence->is_incidence_set(dim<3>,dim<0>));
            CHECK(incidence->get_incidence(dim<3>,dim<0>) == Multi_vector<2,int>{
                                                                      {0, 1, 2, 3},
                                                                      {0, 2, 4, 3}
                                                              });

            REQUIRE(!incidence->is_incidence_set(dim<2>,dim<0>));
            REQUIRE(!incidence->is_incidence_set(dim<3>,dim<2>));
            tpcl::Multi_vector<2, Cell_id> two_to_zero, three_to_two;
            Incidence_outputs io =
                    {{Incidence_output{3, 2, three_to_two},
                      Incidence_output{2, 0, two_to_zero}
                    }};

            Compute_incidence_for_simplices::create_faces_for_simplices_using_vertices(
            *tessellation,
            3,
            2,
            io
            );

            incidence->allocate_incidence(dim<3>, dim<2>);
            incidence->allocate_incidence(dim<2>, dim<0>);
            incidence->get_incidence_mutable(3, 2) = std::move(three_to_two);
            incidence->get_incidence_mutable(2, 0) = std::move(two_to_zero);

            {
                INFO("Face vertices are sorted.");
                INFO("Faces themselves, too, in the lexicographical order.");
                CHECK(incidence->get_incidence(dim<2>,dim<0>) == Multi_vector<2,int>{
                        {0, 1, 2}, /* 0 */ // 2-face of tetrahedron (0, 1, 2, 3)
                        {0, 1, 3}, /* 1 */ // 2-face of tetrahedron (0, 1, 2, 3)
                        {0, 2, 3}, /* 2 */ // 2-face of tetrahedron (0, 1, 2, 3)
                        {0, 2, 4}, /* 3 */ // 2-face of tetrahedron (0, 2, 4, 3)
                        {0, 3, 4}, /* 4 */ // 2-face of tetrahedron (0, 2, 4, 3) and (0, 2, 4, 3)
                        {1, 2, 3}, /* 5 */ // 2-face of tetrahedra  (0, 1, 2, 3)
                        {2, 3, 4}  /* 6 */ // 2-face of tetrahedron (0, 2, 4, 3)
                });
            }

            {
                INFO("Faces are sorted in the lexicographical order.");
                CHECK(incidence->get_incidence(dim<3>,dim<2>) == Multi_vector<2,int>{
                        {0, 1, 2, 5},
                        {2, 3, 4, 6}
                });
            }
        }
    }
    SUBCASE ("Test adjacency computation") {
        INFO("Test compute_adjacency_for_simplices!");

        // This tests the face -> co-face incidence, too,
        // because adjacency computation relies on it.
        // (And we test the latter here.)

        auto check = [&](auto adjacency) {
                    CHECK  (adjacency->get_is_sorted() == true);
                    REQUIRE(adjacency->get_number_of_adjacent_cells(0) == 1);
                    CHECK  (adjacency->get_adjacent_cell(0, 0) == cell<1>);
                    REQUIRE(adjacency->get_number_of_adjacent_cells(1) == 1);
                    CHECK  (adjacency->get_adjacent_cell(1, 0) == cell<0>);
        };
        WHEN("No incidecne output is set") {
            auto adjacency = Compute_incidence_for_simplices::compute_adjacency_for_simplices(
                    *tessellation,
                    dim<3>,
                    {}
            );

            check(adjacency);
        }
        WHEN("Incidecne output is set") {
            Multi_vector<2, Cell_id> incidence_2_3, incidence_3_2, incidence_3_0;
            auto adjacency = Compute_incidence_for_simplices::compute_adjacency_for_simplices(
                    *tessellation,
                    dim<3>,
                    {
                        {2, 3, incidence_2_3},
                        {3, 2, incidence_3_2},
                        {3, 0, incidence_3_0},
                     }
            );

            check(adjacency);
        }
    }
}
