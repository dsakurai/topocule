//
// Created by Daisuke Sakurai on 2020-05-20.
//

#ifndef TOPOCULE_ND_CELL_COMPLEX_INTERFACE_H
#define TOPOCULE_ND_CELL_COMPLEX_INTERFACE_H

#include <K_cells.h>

namespace tpcl {

#if TPCL_cplusplus_20
    template <class T>
    concept Nd_cell_complex_concept = requires (T t) {
        {t.getCellsOfDim(int())};
    };

    template<Nd_cell_complex_concept c>
    constexpr bool Nd_cell_complex_concept_valid = true;
#endif // TPCL_cplusplus_20
}

#endif //TOPOCULE_ND_CELL_COMPLEX_INTERFACE_H
