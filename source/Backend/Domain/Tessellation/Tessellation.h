//
// Created by Daisuke Sakurai on 2020-07-19.
//

#ifndef TOPOCULE_TESSELLATION_H
#define TOPOCULE_TESSELLATION_H

#include <Cell_adjacency_in_manifold.h>
#include <K_cells.h>
#include <Nd_cells.h>
#include <Nd_cell_complex_interface.h>
#include <May_check_multi_fields_sanity.h>
#include <May_handle_simplicial_complex.h>
#include <Safe_vector.h>

#include <memory>

namespace tpcl {

    /**
     * This class does not validate the information it stores.
     * Such validations should be done in some other parts of your program.
     *
     * Currently this class inherits from Nd_cells.
     * A better approach is to have a shared pointer as a member.
     * In that case, polymorphism shall be imitated by casting.
     */
    class Tessellation:
            public Nd_cells,
            public May_handle_simplicial_complex,
            public Has_another_simplicial_complex_handler,
            public May_check_multi_fields_sanity
    {
    public:

        /**
         * We ignore the point coordinates.
         * The third argument, doublePrecision, is used in the TTK's ExplicitTriangulation class.
         * We don't use it.
         */
        int set_input_points(const Cell_id &pointNumber
//                       const void *pointSet = nullptr
                /*, const bool &doublePrecision = false*/
        );

        void set_cell_incidence(const std::shared_ptr<Cell_incidence>& cell_incidence) {
            this->cell_incidence = cell_incidence;
        }

        std::shared_ptr<Cell_incidence>& get_cell_incidence() {
            return cell_incidence;
        }
        std::shared_ptr<const Cell_incidence> get_cell_incidence() const {
            return cell_incidence;
        }


        K_cells create_k_cells(const int dim) const;

        Cell_id get_number_of_vertices() const {
            return get_number_of_cells_for_dim(0);
        }

        /**
         * Legacy from TTK's API. May be removed in the future.
         */
        template<class Container>
        void set_vertex_neighbors(const Cell_id vertex, const Container &neighbors, bool both_directions = false);

        template<class Container>
        int set_vertex_neighbors(const Container &vertexNeighbors) {
            size_t vertex = 0;
            if (get_number_of_vertices() != vertexNeighbors.size()) return -1;

            for (const auto &ns : vertexNeighbors) {
                set_vertex_neighbors(vertex, ns);
                ++vertex;
            }

            return 0;
        }

        [[nodiscard]]
        Cell_id get_vertex_neighbor(const Cell_id &vertexId, const int localNeighborId) const {
            return get_cell_adjacency(/*dim*/0)->get_adjacent_cell(vertexId, Cell_id(localNeighborId));
        }

        [[nodiscard]]
        Cell_id get_num_vertex_neighbors(const Cell_id &vertexId) const {
            return (Cell_id) get_cell_adjacency(0)->get_number_of_adjacent_cells(vertexId);
        }

        /**
         * Note that creating `Cell_adjacency_in_manifold` requires
         * the number of cells for the dimension to be known *a priori*
         * because that class creates an adjacency matrix.
         *
         * @param dim
         * @param adjacent_cells
         */
        void set_cell_adjacency(int dim, const std::shared_ptr<tpcl::Cell_adjacency_in_manifold>& adjacent_cells);

        [[nodiscard]]
        std::shared_ptr<const Cell_adjacency_in_manifold> get_cell_adjacency(int dim) const;
        [[nodiscard]]
        const std::shared_ptr<Cell_adjacency_in_manifold> &get_cell_adjacency(int dim);

        void clear() override;

        // Sanity checker

        void check_multi_fields_sanity() const override;

    private:
        bool is_for_simplicial_complex() const override;

        std::shared_ptr<Cell_incidence> cell_incidence;

        tpcl::Safe_vector<std::shared_ptr<Cell_adjacency_in_manifold>> cell_adjacencies;
    };

    //
    // Definitions
    //

    template<class Container>
    void Tessellation::set_vertex_neighbors(const Cell_id vertex, const Container &neighbors, bool both_directions) {
        const int dim_0 = 0;
        if (cell_adjacencies.size() == dim_0) [[TPCL_unlikely]] cell_adjacencies.resize(dim_0 + 1);
        if (!cell_adjacencies[dim_0]) [[TPCL_unlikely]]  {
            const Cell_id num_cells = get_number_of_cells_for_dim(dim_0);
            cell_adjacencies[dim_0] = std::make_shared<Cell_adjacency_in_manifold>(
                    dim_0,
                    num_cells
            );
        }

        auto& adjacent_cells = get_cell_adjacency(dim_0);

        for (auto& neighbor: neighbors) {
            if (!both_directions && (vertex > neighbor)) continue;
            adjacent_cells->insert_adjacency(vertex, neighbor);
        }
    }

}

#endif //TOPOCULE_TESSELLATION_H
