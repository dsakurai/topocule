//
// Created by Daisuke Sakurai on 2019/11/10.
//

#include <test.h>
#include <Ttk_nd_triangulation.h>
#include <Cell_incidence.h>
#include <FTMTree.h>

using namespace tpcl;

TEST_CASE ("Test Ttk_nd_triangulation: vertex neighbors") {
    Ttk_nd_triangulation tri;

    using namespace std;
    const vector<set<int>> vertexNeighbors = {
        {1},
        {0,2},
        {1}
    };

    tri.get_tpcl_triangulation()->set_cell_incidence(std::make_shared<tpcl::Cell_incidence>(2, /*triangles*/ true));

    tri.setInputPoints(vertexNeighbors.size());
    tri.setVertexNeighbors(vertexNeighbors);

    { // check
        for (int i = 0; i < vertexNeighbors.size(); ++i) {
            INFO("i: " << i);
            const Cell_id numNeighbors = tri.getVertexNeighborNumber(i);
            CHECK(vertexNeighbors[i].size() == numNeighbors);

            int n = 0;
            for (const auto& neighbor:  vertexNeighbors[i]) {
                ttk::SimplexId neighbor_stored;
                tri.getVertexNeighbor(i, n, neighbor_stored);
                CHECK(neighbor_stored == neighbor);
                ++n;
            }
        }
    }
};
