//
// Created by Daisuke Sakurai on 2019/11/10.
//

#ifndef TOPO_MOLECULE_STATES_NDTRIANGULATION_H
#define TOPO_MOLECULE_STATES_NDTRIANGULATION_H

#include <AbstractTriangulation.h>
#include <Tessellation.h>
#include <memory>

namespace tpcl {

    /**
     * Wrapper for TTK Ttk_nd_triangulation.
     */
    class Ttk_nd_triangulation : public ::ttk::AbstractTriangulation {
    public:
        Ttk_nd_triangulation (std::shared_ptr<Tessellation> tpcl_triangulation) :
                tpcl_triangulation {tpcl_triangulation}
        {
        }

        Ttk_nd_triangulation () :
                tpcl_triangulation {std::make_shared<Tessellation>()}
        {
        }

        static constexpr int UNSET = Nd_cells::UNSET;

        inline int setInputPoints(const Cell_id &pointNumber
                //                       const void *pointSet = nullptr
                /*, const bool &doublePrecision = false*/
        ) {
            return tpcl_triangulation->set_input_points(pointNumber);
        }

        ttk::SimplexId getNumberOfVertices() const override {
            return (ttk::SimplexId) tpcl_triangulation->get_number_of_vertices();
        }

        template<class Container>
        int setVertexNeighbors(const Cell_id vertex, const Container &neighbors) {
            tpcl_triangulation->set_vertex_neighbors(vertex, neighbors);
            this->hasPreconditionedVertexNeighbors_ = true;
            return 0;
        }

        template<class Container>
        int setVertexNeighbors(const Container &vertexNeighbors) {
            tpcl_triangulation->set_vertex_neighbors<Container>(vertexNeighbors);
            this->hasPreconditionedVertexNeighbors_ = true;
            return 0;
        }

        int getVertexNeighbor(const ttk::SimplexId &vertexId, const int &localNeighborId,
                              ttk::SimplexId &neighborId) const override {
            neighborId = tpcl_triangulation->get_vertex_neighbor(
                    vertexId,
                    localNeighborId
            );
            return 0;
        }

        ttk::SimplexId getVertexNeighborNumber(const ttk::SimplexId &vertexId) const override {
            return (ttk::SimplexId) tpcl_triangulation->get_num_vertex_neighbors(vertexId);
        }

        void set_tpcl_triangulation(std::shared_ptr<Tessellation> tpcl_triangulation) {
            this->tpcl_triangulation = tpcl_triangulation;
        }

        std::shared_ptr<Tessellation> get_tpcl_triangulation() {
            return tpcl_triangulation;
        }

        int clear() override {
            tpcl_triangulation->clear();
            return ::ttk::AbstractTriangulation::clear();
        }

    private:
        std::shared_ptr<Tessellation> tpcl_triangulation;

    // Unimplemented pure-virtual methods
    private:
        int getCellEdge(const ttk::SimplexId &cellId, const int &localEdgeId, ttk::SimplexId &edgeId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getCellEdgeNumber(const ttk::SimplexId &cellId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getCellEdges() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getCellNeighbor(const ttk::SimplexId &cellId, const int &localNeighborId,
                            ttk::SimplexId &neighborId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getCellNeighborNumber(const ttk::SimplexId &cellId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getCellNeighbors() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getCellTriangle(const ttk::SimplexId &cellId, const int &localTriangleId,
                            ttk::SimplexId &triangleId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getCellTriangleNumber(const ttk::SimplexId &cellId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getCellTriangles() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getCellVertex(const ttk::SimplexId &cellId, const int &localVertexId, ttk::SimplexId &vertexId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getCellVertexNumber(const ttk::SimplexId &cellId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        int getDimensionality() const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::pair<ttk::SimplexId, ttk::SimplexId>> *getEdges() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getEdgeLink(const ttk::SimplexId &edgeId, const int &localLinkId, ttk::SimplexId &linkId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getEdgeLinkNumber(const ttk::SimplexId &edgeId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getEdgeLinks() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getEdgeStar(const ttk::SimplexId &edgeId, const int &localStarId, ttk::SimplexId &starId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getEdgeStarNumber(const ttk::SimplexId &edgeId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getEdgeStars() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getEdgeTriangle(const ttk::SimplexId &edgeId, const int &localTriangleId,
                            ttk::SimplexId &triangleId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getEdgeTriangleNumber(const ttk::SimplexId &edgeId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getEdgeTriangles() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getEdgeVertex(const ttk::SimplexId &edgeId, const int &localVertexId, ttk::SimplexId &vertexId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getNumberOfCells() const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getNumberOfEdges() const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getNumberOfTriangles() const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getTriangles() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int
        getTriangleEdge(const ttk::SimplexId &triangleId, const int &localEdgeId, ttk::SimplexId &edgeId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getTriangleEdgeNumber(const ttk::SimplexId &triangleId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getTriangleEdges() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int
        getTriangleLink(const ttk::SimplexId &triangleId, const int &localLinkId, ttk::SimplexId &linkId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getTriangleLinkNumber(const ttk::SimplexId &triangleId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getTriangleLinks() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int
        getTriangleStar(const ttk::SimplexId &triangleId, const int &localStarId, ttk::SimplexId &starId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getTriangleStarNumber(const ttk::SimplexId &triangleId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getTriangleStars() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getTriangleVertex(const ttk::SimplexId &triangleId, const int &localVertexId,
                              ttk::SimplexId &vertexId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        int getVertexEdge(const ttk::SimplexId &vertexId, const int &localEdgeId, ttk::SimplexId &edgeId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getVertexEdgeNumber(const ttk::SimplexId &vertexId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getVertexEdges() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getVertexLink(const ttk::SimplexId &vertexId, const int &localLinkId, ttk::SimplexId &linkId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getVertexLinkNumber(const ttk::SimplexId &vertexId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getVertexLinks() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getVertexNeighbors() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getVertexPoint(const ttk::SimplexId &vertexId, float &x, float &y, float &z) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        int getVertexStar(const ttk::SimplexId &vertexId, const int &localStarId, ttk::SimplexId &starId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getVertexStarNumber(const ttk::SimplexId &vertexId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getVertexStars() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        int getVertexTriangle(const ttk::SimplexId &vertexId, const int &localTriangleId,
                              ttk::SimplexId &triangleId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        ttk::SimplexId getVertexTriangleNumber(const ttk::SimplexId &vertexId) const override {
            throw std::runtime_error("Not implemented yet");
            return -1;
        }

        const std::vector<std::vector<ttk::SimplexId>> *getVertexTriangles() override {
            throw std::runtime_error("Not implemented yet");
            return nullptr;
        }

        bool isEdgeOnBoundary(const ttk::SimplexId &edgeId) const override {
            throw std::runtime_error("Not implemented yet");
            return false;
        }

        bool isEmpty() const override {
            throw std::runtime_error("Not implemented yet");
            return false;
        }

        bool isTriangleOnBoundary(const ttk::SimplexId &triangleId) const override {
            throw std::runtime_error("Not implemented yet");
            return false;
        }

        bool isVertexOnBoundary(const ttk::SimplexId &vertexId) const override {
            throw std::runtime_error("Not implemented yet");
            return false;
        }
    };

#if TPCL_cplusplus_20
    static_assert(Nd_cell_complex_concept_valid<Ttk_nd_triangulation>);
#endif
}

#endif //TOPO_MOLECULE_STATES_NDTRIANGULATION_H
