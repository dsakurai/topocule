
add_library(Tessellation
        Ttk_nd_triangulation.cpp
        Ttk_nd_triangulation.h
        Tessellation.cpp
        Tessellation.h
        )

target_include_directories(Tessellation
    PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}"
)

target_link_libraries(Tessellation
    PUBLIC
        K_cells
        Cell_incidence
        Nd_cell_complex_interface
        May_check_multi_fields_sanity
        ttk::base::abstractTriangulation
)

if(TPCL_ENABLE_TESTS)
    target_sources_test_topocule(
        PUBLIC
            test_Nd_triangulation.cpp
    )

    target_link_libraries_test_topocule(
        PUBLIC
        Tessellation
        two_tets_vtk
    )
endif(TPCL_ENABLE_TESTS)
