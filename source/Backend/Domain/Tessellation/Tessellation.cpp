//
// Created by Daisuke Sakurai on 2020-07-19.
//

#include "Tessellation.h"

#include <string>

using namespace tpcl;

int Tessellation::set_input_points(const Cell_id &pointNumber) {

    set_number_of_cells_for_dim(0, pointNumber);

    return 0;
}

void Tessellation::clear() {
    Nd_cells::clear();
    cell_incidence = nullptr;
    cell_adjacencies.clear();
}

K_cells Tessellation::create_k_cells(const int dim) const {
    Cell_id num_cells = -2;
    if (get_maximal_dimension() < dim) {
        throw std::runtime_error {
                "Failed to get the number of cells for dimension " + std::to_string(dim)
                + ". The storage has the capacity for dimensions up to " + std::to_string(
                        get_maximal_dimension() - 1)
                + "."
        };
    }

    num_cells = get_number_of_cells_for_dim(dim);

    // We could access the array but it was unset anyway...
    if (num_cells == UNSET) TPCL_unlikely {
        throw std::runtime_error {"number of cells unset for dimension " + std::to_string(dim)};
    }

    return K_cells {dim, num_cells, cell_incidence};
}

bool Tessellation::is_for_simplicial_complex() const {
    return delegate_simplicial_complex_verification(cell_incidence.get());
}

std::shared_ptr<const Cell_adjacency_in_manifold> Tessellation::get_cell_adjacency(int dim) const {
    return cell_adjacencies[dim];
}

const std::shared_ptr<Cell_adjacency_in_manifold> &Tessellation::get_cell_adjacency(int dim) {
    return cell_adjacencies[dim];
}

void Tessellation::set_cell_adjacency(int dim, const std::shared_ptr<tpcl::Cell_adjacency_in_manifold>& adjacent_cells) {
    this->cell_adjacencies.safe_ref(dim) = adjacent_cells;
}

void Tessellation::check_multi_fields_sanity () const {

    auto incidence = this->get_cell_incidence();

    const auto num_incidence = incidence->get_number_of_incidence();
    {
        const auto max_dim_source = num_incidence - 1;
        if (this->get_maximal_dimension() < max_dim_source ) throw std::runtime_error {
                    "Maximum source dimension in the incident cells data structure exceeds the promise in the tessellation."};

        // dim of source
        for (int dim_s = 0; dim_s < incidence->get_number_of_incidence(); ++dim_s) {
            // dim of target
            if (incidence->get_number_of_incidence(dim_s) - 1 > this->get_maximal_dimension()) throw std::runtime_error {"Maximum target dimension in the incident cells data structure exceeds the promise in the tessellation."};
            for (int dim_t = 0; dim_t < incidence->get_number_of_incidence(dim_s); ++dim_t) {
                if (incidence->is_incidence_set(dim_s, dim_t)){
                    if (incidence->get_number_of_incidence(dim_s, dim_t) - 1 > this->get_number_of_cells_for_dim(dim_s)) throw std::runtime_error {"Maximum source cell in the incident cells data structure exceeds the promise in the tessellation."};
                    for (Cell_id cell_a = 0; cell_a < incidence->get_number_of_incidence(dim_s, dim_t); ++cell_a) {
                        for (Cell_id cell_b_raw = 0; cell_b_raw < incidence->get_number_of_incidence(dim_s, dim_t, cell_a); ++cell_b_raw) {
                            auto promised_cell_b_max = this->get_number_of_cells_for_dim(dim_t) - 1;
                            auto cell_b_id = incidence->get_incidence(dim_s, dim_t, cell_a, cell_b_raw);
                            if ( cell_b_id > promised_cell_b_max) throw std::runtime_error {"Maximum target cell ID "+ std::to_string(cell_b_id) +" in the incident cells data structure from dimension " + std::to_string(dim_s) + " to " + std::to_string(dim_t) + " from source cell " + std::to_string(cell_a) + " exceeds the promised maximum " + std::to_string(promised_cell_b_max)};
                        }
                    }
                }
            }
        }
    }

    {
        const auto max_dim_adjacency = cell_adjacencies.size() - 1;
        if (this->get_maximal_dimension() < max_dim_adjacency ) throw std::runtime_error {
                    "Maximum dimension in the cell adjacency data structure exceeds the promise in the tessellation."};
        for (int d = 0; d < max_dim_adjacency; ++d) {
            if (!cell_adjacencies[d]) continue;

            auto num_source_cells = cell_adjacencies[d]->get_number_of_cells();
            if (num_source_cells > this->get_number_of_cells_for_dim(d)) throw std::runtime_error {"Source cell ID in the cell adjacency data structure exceeds the promise in the tessellation"};
            for (Cell_id cell_a = 0; cell_a < cell_adjacencies[d]->get_number_of_cells(); ++cell_a) {
                auto num_target_cells = cell_adjacencies[d]->get_number_of_adjacent_cells(cell_a);
                for (Cell_id cell_b_raw = 0; cell_b_raw < num_target_cells; ++cell_b_raw) {
                    auto cell_b_id = cell_adjacencies[d]->get_adjacent_cell(cell_a, cell_b_raw);
                    if (cell_b_id > this->get_number_of_cells_for_dim(d)) throw std::runtime_error {"Target cell ID in the cell adjacency data structure exceeds the promise in the tessellation"};
                }
            }
        }
    }
}
