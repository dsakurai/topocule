//
// Created by Daisuke Sakurai on 2019/11/03.
//

#ifndef TOPO_MOLECULE_STATES_COMPUTEDELAUNAY_H
#define TOPO_MOLECULE_STATES_COMPUTEDELAUNAY_H

#include <Printer.h>

#include <CGAL/Epick_d.h>
#include <CGAL/Delaunay_triangulation.h>

#include <Ttk_nd_triangulation.h>

namespace tpcl {

    /**
     * This class holds the point coordinates, which is unnecessary for topological analysis.
     * This is a limitation due to the design of CGAL's triangulation class.
     * Given that the cominatorials become far larger than the data size of the point coordinates,
     * This is probably not a problem.
     *
     * @param PntArray: the type of the point array, such as std::vector<std::array<double,3>>
     *
     * @todo if the memory overhead becomes a problem, destroy this class and hold the adjacent points.
     */
    template<class PntArray>
    class ComputeDelaunay {
    public:
        using PointArray = PntArray;

        int doIt() {
            auto &pointsIn = *this->pointsIn;
            point2cells.clear();

            if (pointsIn.empty()) {
                ERROR_MESSAGE("points are empty");
                return 0;
            }

            int dim = getDim();
            dt = std::make_shared<DelaunayTriangulation>(dim);

            triangulate(pointsIn, *dt);

            finite_points_2_finite_full_cells(*dt, point2cells);

            return 1;
        }

        int getDim() {
            if (pointsIn->empty()) {
                return 0;
            }
            int dim = pointsIn->at(0).size();
            return dim;
        }

        void adjacentPoints(
                const int point,
                std::set<int> &points
        ) const {
            adjacent_points(point, point2cells[point], points);
        }

        struct Vertex_data {
            int id = -1; // the ID is identical to the point index in the input.
        };

// d-dimensional geometry kernel
        using GeometryKernel =
        CGAL::Epick_d<
                CGAL::Dynamic_dimension_tag // dynamically deterrmine the dimension of the full cell
        >;

        using Triangulation_vertex = CGAL::Triangulation_vertex<
                GeometryKernel,
                Vertex_data>;

        using TriangulationDataStructure = CGAL::Triangulation_data_structure<
                GeometryKernel::Dimension,
                Triangulation_vertex
        >;

// CGAL's Delaunay Triangulation is optimized for minimum storage usage.
// This makes it somewhat slow (and complex) to access the edges from a vertex.
        typedef CGAL::Delaunay_triangulation<
                GeometryKernel,
                TriangulationDataStructure
        > DelaunayTriangulation;

// get points neighboring a point
// This should be exposed in the header due to performance reasons
        inline void adjacent_points(
                const int point,
                const std::vector<typename DelaunayTriangulation::Full_cell_handle> &cells,
                std::set<int> &points
        ) const {
            points.clear();
            for (const auto &cell : cells) {
                for (auto v = cell->vertices_begin();
                     v != cell->vertices_end();
                     ++v
                        ) {
                    int id = (*v)->data().id;
                    if (point != id) points.insert(id);
                }
            }
        }

        static void triangulate(const PointArray &pointsIn, DelaunayTriangulation &dt) {

            using point_type = typename std::remove_reference_t<decltype(pointsIn)>::value_type;
            using value_type = typename point_type::value_type;
            const size_t dim = dt.maximal_dimension();

            // do the triangulation
            typename DelaunayTriangulation::Vertex_handle hint;
            int i = 0;
            for (int j = 0; j < pointsIn.size(); ++j) {
                const value_type *p_point = &pointsIn[j][0];
                typename DelaunayTriangulation::Point p(p_point, p_point + dim);
                if (typename DelaunayTriangulation::Vertex_handle() != hint) {
                    hint = dt.insert(p, hint);
                } else {
                    hint = dt.insert(p);
                }
                hint->data().id = j;
            }
        }

        // This is the "neighbor" in TTK FTM tree
        // We do not insert the infinite (i.e. dummy) cell in the output
        static void finite_points_2_finite_full_cells(
                DelaunayTriangulation &dt,
                std::vector<std::vector<typename DelaunayTriangulation::Full_cell_handle>> &cells) {

            // this is the number of *fininte* vertices!
            int num_vertices = dt.number_of_vertices();
            cells.resize(num_vertices);

            std::vector<int> cell_vertices;

            for (auto c = dt.full_cells_begin();
                 c != dt.full_cells_end();
                 ++c
                    ) {

                // a dummy cell shall not be stored
                if (dt.is_infinite(c)) continue;

                for (auto v = c->vertices_begin();
                     v != c->vertices_end();
                     ++v
                        ) {
                    int point = (*v)->data().id;
                    cells[point].push_back(c);
                }
            }
        }

        void setPoints(std::shared_ptr<PointArray> pointsIn) {
            this->pointsIn = pointsIn;
        }

        /**
         * The number of points. This excludes the virtual point (the infinite point) used
         * for the computation of the Delaunay triangulation.
         */
        size_t getNumberOfFinitePoints() const {
            return pointsIn->size();
        }

        /**
         * The dimensionality of the resulting triangulation is set to the
         * maximal dimension allowed in the Delaunay triangulation.
         */
        void to_ttk_nd_triangulation(Ttk_nd_triangulation &triangulation ) const {

            const auto max_dimension = dt->maximal_dimension();

            const auto& cell_incidence = triangulation.get_tpcl_triangulation()->get_cell_incidence();
            { // Intialize connectivity
                if(cell_incidence) {
                    throw std::runtime_error {"Cell connectivity is not empty."};
                }
                triangulation.get_tpcl_triangulation()->set_cell_incidence(
                        std::make_shared<Cell_incidence>(max_dimension, /*triangles*/ true));
            }

            size_t num_vertices = getNumberOfFinitePoints();

            triangulation.setInputPoints(num_vertices);

            for (size_t p = 0; p < num_vertices; ++p) {
                std::set<int> points;
                adjacentPoints(p, points);
                triangulation.setVertexNeighbors(p, points);
            }

            triangulation.get_tpcl_triangulation()->set_maximal_dimension(max_dimension);
            cell_incidence->set_for_simplicial_complex(true);
        }

    private:
        // algorithm implementation & storage of the triangulation
        std::shared_ptr<DelaunayTriangulation> dt = nullptr;

        std::vector<std::vector<typename DelaunayTriangulation::Full_cell_handle>> point2cells;
        std::shared_ptr<PointArray> pointsIn;
    };

}

#endif //TOPO_MOLECULE_STATES_COMPUTEDELAUNAY_H

