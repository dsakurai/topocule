//
// Created by Daisuke Sakurai on 2019/11/03.
//

#include <test.h>
#include <ComputeDelaunay.h>
#include <four_points.h>

#include <vector>

using namespace tpcl;

TEST_CASE ("Test ComputeDelaunay") {
    // Easy-to-test triangulation

    ComputeDelaunay<std::vector<std::array<double,2>>> adj;
    auto pointsIn = std::make_shared<std::vector<std::array<double,2>>>();
    *pointsIn = four_points::vertexes();

    adj.setPoints(pointsIn);
    CHECK(pointsIn->size() == adj.getNumberOfFinitePoints());

    adj.doIt();

    {// check the point-to-cell mapping

        std::vector<std::set<int>> adjacents;
        for (int p = 0; p < 4; ++p) {
            std::set<int> points;
            // @todo pass this to TTK
            adj.adjacentPoints(p, points);
            adjacents.emplace_back(std::move(points));
        }
        const decltype(adjacents) expect = four_points::vertex_adjaceny();
        CHECK(adjacents == expect);
    }
}


