
Tessellation
===========

Each dimension has its own cell IDs that start from 0.

Adapting TTK
---------
Topocule's tessellation is often fed to
Topology ToolKit (TTK).
This is established in a few scenarios:
1. Wrapping a topocule tessellation as a TTK's triangulation class.
2. Copying topocule tessellation to a TTK triangulation data structure

1. is often preferred but not quick to implement. 
Therefore, topocule constructs a tessellation and
copy the contents to TTK's triangulation data structure. 
