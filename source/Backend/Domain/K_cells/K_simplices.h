//
// Created by Daisuke Sakurai on 2020-06-07.
//

#ifndef TOPOCULE_K_SIMPLICES_H
#define TOPOCULE_K_SIMPLICES_H

#include <Cells_container_concepts.h>
#include <Cell_incidence.h>
#include <K_cells.h>

#include <vector>

namespace tpcl {
    /**
     * K_simplices is supposedly a specialized version of K_cells for
     * simplices that has beter performance, but currently it's identical to K_cells.
     */
    using K_simplices = K_cells;
}

#endif //TOPOCULE_K_SIMPLICES_H
