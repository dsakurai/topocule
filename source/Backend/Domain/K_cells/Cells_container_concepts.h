//
// Created by Daisuke Sakurai on 2020-06-07.
//

#ifndef TOPOCULE_CELLS_CONTAINER_CONCEPTS_H
#define TOPOCULE_CELLS_CONTAINER_CONCEPTS_H

#include <Basics.h>

#if TPCL_cplusplus_20
#include <concepts>
#endif

#include <memory>
#include <vector>
#include "K_cells.h"

namespace tpcl {
    class Cell_as_fullfaces_base {
    public:
        virtual Cell_id get_num_fullfaces() const = 0;

        virtual Cell_id get_id_of_fullface(Cell_id face) const = 0;
    };

#if TPCL_cplusplus_20
    template<class C>
    concept Cell_fullface_accessor_concept = requires(C c) {
        typename C::Faces_container;
    };

    template<Cell_fullface_accessor_concept a>
    constexpr bool Cell_fullface_accessor_concept_valid = true;
#endif // TPCL_cplusplus_20

    /**
     * Store k-cells in a simplicial complex.
     * This wraps a more basic `Cell_incidence` object,
     * which stores general connectivity between cells.
     *
     * @tparam Cell_fullface_access
     */
    template<
        class Cell_vertex_access,
        TPCL_concept(Cell_fullface_accessor_concept) Cell_fullface_access
        >
    class K_cells_accessor {
    public:
        // The internal container class is chosen by the accessor.
        using Container = typename Cell_fullface_access::Faces_container;

        K_cells_accessor(
                int dim,
                Cell_id num_cells,
                const std::shared_ptr<Container>& faces
        ) : dim{dim}, num_cells {num_cells}, faces{faces}
        {}

        Cell_id get_num_fullfaces(Cell_id cell) const {
            return get_cell_as_fullfaces(cell).get_num_fullfaces();
        }

        Cell_id get_id_of_fullface(Cell_id cell, Cell_id fullface) const {
            return get_cell_as_fullfaces(cell).get_id_of_fullface(fullface);
        }

        Cell_id get_id_of_vertex(Cell_id cell, Cell_id vertex) const {
            return get_cell_as_vertices(cell).get_id_of_vertex(vertex);
        }

        Cell_id get_num_cells() const {
            return num_cells;
        }

        Cell_fullface_access get_cell_as_fullfaces(Cell_id id) const {
            return {dim, id, *faces};
        }

        Cell_vertex_access get_cell_as_vertices(Cell_id id) const {
            return {dim, id, *faces};
        }

        /// resizing invalidates the iterator.
        class iterator {
        public:
            iterator (Cell_id cell) {};

            using value_type = Cell_id;
            using pointer = value_type*;
            using reference = value_type&;
            using iterator_category = std::forward_iterator_tag;

            // This should rather return the actual cell ID,
            // not the location of the cell ID.
//            value_type& operator*() {
//                return cell;
//            }

            bool operator ==(const iterator& that) {
                return this->cell == that.cell;
            }

            bool operator !=(const iterator& that) {
                return !(*this == that);
            }

            iterator& operator ++() {
                ++cell;
                return *this;
            }
            iterator operator ++(int) {
                iterator out = *this;
                return ++out;
            }

        private:
            Cell_id cell;
        };
        iterator begin() const {return iterator{0};}
        /// resizing invalidates the iterator.
        iterator end() const {return iterator{get_num_cells()};}

    private:
        const int dim;
        Cell_id num_cells = 0;
        const std::shared_ptr<Container>& faces;
    };
}
#endif //TOPOCULE_CELLS_CONTAINER_CONCEPTS_H
