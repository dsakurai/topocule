//
// Created by Daisuke Sakurai on 2020-12-16.
//

#include <test.h>

#include <Cell_adjacency_in_manifold.h>

using namespace tpcl;
TEST_CASE ("Test Cell_adjacency_in_manifold") {

    {
        INFO ("No cells");
        tpcl::Cell_adjacency_in_manifold adjacency{0, 0};
        CHECK(adjacency.begin() == adjacency.end());
    }

    {
        INFO ("Cells without adjacency");
        tpcl::Cell_adjacency_in_manifold adjacency{0, 2};
        CHECK(adjacency.begin() == adjacency.end());
    }
}
