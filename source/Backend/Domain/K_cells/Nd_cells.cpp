//
// Created by Daisuke Sakurai on 2020-09-19.
//

#include "Nd_cells.h"
#include <stdexcept>

void tpcl::Nd_cells::set_maximal_dimension(const int dim) {
    const int old_size = get_maximal_dimension() + 1;
    const int size_required = dim + 1;
    if (size_required < old_size) {
        throw std::runtime_error {
                "The dimensionalities handled "
                "exceeds the maximal dimensionality requested."
        };
    }
    for (int d = 0; d < size_required; ++d) {
        if (d >= old_size) set_number_of_cells_for_dim(d, UNSET);
    }
}

void tpcl::Nd_cells::set_number_of_cells_for_dim(int dim, Cell_id numCells) {

    const int size_required = dim + 1;
    if (num_cells_of_dim.size() < size_required) {
        num_cells_of_dim.resize(size_required, UNSET);
    }

    num_cells_of_dim[dim] = numCells;
}

tpcl::Cell_id tpcl::Nd_cells::get_number_of_cells_for_dim(int dim) const {
    // If this returns UNSET (== -1), you forgot to set the number of cells.
    return num_cells_of_dim.at(dim);
}

int tpcl::Nd_cells::get_maximal_dimension() const {
    return num_cells_of_dim.size() - 1;
}
