
add_library(K_cells)

target_sources(K_cells
        PUBLIC
            K_cells.h
            K_simplices.h
            Cells_container_concepts.h
            Cell_adjacency_in_manifold.cpp
            Cell_adjacency_in_manifold.h
            Nd_cells.cpp
            Nd_cells.h
        )

target_include_directories(K_cells
        PUBLIC
            "${CMAKE_CURRENT_SOURCE_DIR}"
        )

target_link_libraries(K_cells
        PUBLIC
        Basics
        May_handle_simplicial_complex
)

if(TPCL_ENABLE_TESTS)
    target_sources_test_topocule(
            PUBLIC
            test_Cell_adjacency_in_manifold.cpp
    )

    target_link_libraries_test_topocule(
            PUBLIC
            K_cells
    )
endif(TPCL_ENABLE_TESTS)
