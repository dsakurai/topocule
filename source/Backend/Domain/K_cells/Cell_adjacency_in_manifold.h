//
// Created by Daisuke Sakurai on 2020-09-13.
//

#ifndef TOPOCULE_CELL_ADJACENCY_IN_MANIFOLD_H
#define TOPOCULE_CELL_ADJACENCY_IN_MANIFOLD_H

#include <Basics.h>
#include "Nd_cells.h"

#include <memory>
#include <vector>

namespace tpcl {
    class Adjacency {
    public:
    };

    /**
     * Keep the adjacency in (i) a triangulation of a d-manifold or
     * (ii) a simplicial complex only if it is of interest.
     *
     * On (i)
     * ------
     *
     * Due to the definition,
     * the simplicial complex of the triangulation is homeomorphic to the manifold (with a boundary).
     * This means, among other things, that if two d-cells in the complex are adjacent, they face at
     * one (or more) (d-1)-cell.
     *
     * This is an important assumption for the Jacobi set algorithm to to correctly
     * identify the folds of the Reeb space.
     *
     * For example, a 3-complex violating this rule may contain two tetrahedra facing at one edge only
     * and not at a 2-face.
     * In this case, IF the adjacency reports these two tetrahedra to be
     * adjacent, a Jacobi set algorithm, as implemented in this library,
     * assumes the tetrahedra faces each other at a 2-face.
     * Therefore it misses the fact that two fiber components are meeting at the Jacobi set
     * (it thinks that one fiber component do not merge at the Jacobi set, for example).
     *
     * On (ii)
     * -------
     * Due to the problem in (i), simplicial complexes which are not homeomorphic to a d-manifold
     * (except at boundaries) can fail to get Jacobi set out of them.
     * This kind of simplicial complex can be generated, e.g., as a Rips complex.
     * If we compute the Jacobi set for k functions, we only need adjacency through k+1-faces in
     * the domain. (The Reeb graph computation using the level set graph does exactly this.)
     * This is one example of an *interesting* adjacency.
     */
    class Cell_adjacency_in_manifold: public Adjacency {
    public:
        /**
         *
         * @param num_cells: Total number of cells being hinged for all adjacency.
         */
        Cell_adjacency_in_manifold(int dimension, Cell_id num_cells) :
                dimension {dimension},
                adjacent_cells(/*size*/Cell_id{num_cells})
        { }

        Cell_adjacency_in_manifold(int dimension, const Nd_cells &nd_cells) :
                Cell_adjacency_in_manifold{
                        dimension,
                        Cell_id{nd_cells.get_number_of_cells_for_dim(dimension)}
                }
        { }

        friend class iterator;

        class iterator {
        public:
            iterator () {}
            iterator (const Cell_adjacency_in_manifold* adjacency):
                    adjacency {adjacency}
            {
                // If there's no adjacency relationship, there's nothing to iterate over.
                const auto num_source_cells = adjacency->get_number_of_cells();
                for (Cell_id s = 0; s < num_source_cells; ++s) {
                    if (adjacency->get_number_of_adjacent_cells(s)){
                        at_end = false;
                        break;
                    }
                }
            }

            std::pair<Cell_id, Cell_id> operator* () const {
                auto incident_cell_id = adjacency->get_adjacent_cell(
                        this_cell_id, adjacent_cell
                );
                return std::make_pair(this_cell_id, incident_cell_id);
            }

            iterator& operator ++() {
                while (!at_end) {
                    // increment as long as we can
                    // until an adjacent cell is found
                    try_increment();
                    if (!at_end && has_adjacent_cell()) break;
                }
                return *this;
            }

            bool operator==(const iterator& that) const {
                // both iterators are end
                if (this->at_end && that.at_end) return true;
                // exactly one iterator is end
                if (this->at_end != that.at_end) return false;

                return (this->this_cell_id == that.this_cell_id) && (this->adjacent_cell == that.adjacent_cell);
            }

            bool operator!=(const iterator& that) {
                return !(*this == that);
            }

            static iterator end() {
                return {};
            }

            static bool begin_equals_end(const Cell_adjacency_in_manifold* adjacency) {
                return adjacency->get_number_of_cells() == 0;
            }
        private:

            void try_increment() {
                if (is_incremented_adjacent_cell_valid() ) {
                    ++adjacent_cell;
                    return;
                }
                if (is_incremented_cell_valid()) {
                    adjacent_cell = 0;
                    ++this_cell_id;
                    return;
                }

                // reached the end of the iteration
                at_end = true;
            }

            bool has_adjacent_cell() const {
                return is_in_range(
                        adjacent_cell,
                        adjacency->get_number_of_adjacent_cells(
                                this_cell_id
                        )
                );
            }

            static bool is_in_range(int iter, int max) {
                return iter < max;
            }

            static bool is_increment_valid(int iter, int max) {
                return is_in_range(iter + 1, max);
            }
            bool is_incremented_adjacent_cell_valid() const {
                return is_increment_valid(
                        adjacent_cell,
                        adjacency->get_number_of_adjacent_cells(
                                this_cell_id
                                )
                );
            }

            bool is_incremented_cell_valid() const {
                return is_increment_valid(
                        this_cell_id,
                        adjacency->get_number_of_cells()
                );
            }

            const Cell_adjacency_in_manifold* adjacency = nullptr;
            Cell_id this_cell_id = Cell_id{0};
            int adjacent_cell = 0;
            // Default: begin == end.
            // We check if there's any cell when each instance is initialized.
            bool at_end = true;
        };

        iterator begin() const {
            // Avoid ill-formed iterators
            if (iterator::begin_equals_end(this)) return iterator::end();

            return iterator {this};
        }

        iterator end() const {
            return iterator::end();
        }

        /**
         * Total number of cells kept in the adjacency data structure
         */
        inline
        Cell_id get_number_of_cells() const {
            return (Cell_id) adjacent_cells.size();
        }

        inline
        Cell_id get_number_of_adjacent_cells(Cell_id cell_id) const {
            return (Cell_id) adjacent_cells[cell_id].size();
        }

        inline
        Cell_id get_adjacent_cell (Cell_id cell_id, Cell_id adjacent ) const {
            return adjacent_cells[cell_id][adjacent];
        }

        /**
         * This is done for both directions.
         *
         * @param cell_id
         * @param adjacent_cell_id
         */
        void insert_adjacency(Cell_id cell_id, Cell_id adjacent_cell_id) {
            adjacent_cells[cell_id].emplace_back(adjacent_cell_id);
            adjacent_cells[adjacent_cell_id].emplace_back(cell_id);
        }

        [[nodiscard]]
        int get_dimension() const {
            return dimension;
        }

        [[nodiscard]]
        bool get_is_sorted() const {
            return is_sorted;
        }

        void set_is_sorted(bool is_sorted) {
            this->is_sorted = is_sorted;
        }

    private:
        int dimension;
        bool is_sorted = false;
        std::vector<std::vector<Cell_id>> adjacent_cells;
    };
}

#endif //TOPOCULE_CELL_ADJACENCY_IN_MANIFOLD_H
