//
// Created by Daisuke Sakurai on 2020-09-19.
//

#ifndef TOPOCULE_ND_CELLS_H
#define TOPOCULE_ND_CELLS_H

#include <Basics.h>

#include <vector>

namespace tpcl {
    /**
     * The number of cells must be set manually.
     *
     * In many practices this may be computed
     * from various arrays s.a. that for the vertex coordinates /
     * face cells.
     * However, we do not auto-compute the number of cells because
     * some cells may only need an ID and no explicit coordinates nor face cells.
     * This is, for example, true for implicitly triangulated regular grids.
     *
     * The maximal dimension can be revised.
     */
    class Nd_cells {
    public:

        void set_maximal_dimension(const int dim);

        void set_number_of_cells_for_dim(int dim, Cell_id numCells);

        /**
         * @todo make the output of `get_number_of_cells_for_dim` optional
         */
        Cell_id get_number_of_cells_for_dim(int dim) const;

        static constexpr int UNSET = -1;

        virtual void clear() {
            num_cells_of_dim.clear();
        }

        int get_maximal_dimension() const;

    private:
        /*
         *  Caution: The size of this container does not reflect the full dimension.
         */
        std::vector<int> num_cells_of_dim;
    };

}



#endif //TOPOCULE_ND_CELLS_H
