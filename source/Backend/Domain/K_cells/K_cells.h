//
// Created by Daisuke Sakurai on 2019/11/27.
//

#ifndef TOPOCULE_K_CELLS_H
#define TOPOCULE_K_CELLS_H

#include <Basics.h>
#include <Cells_container_concepts.h>
#include <Cell_incidence.h>

#include <memory>

namespace tpcl {

    /**
     * This is an accessor and is thus not meant to be held in a container.
     * Due to this we allow auxiliary information like the dimension.
     * (If you store 1 million k-cells of dimension 1, that is a waste of RAM for sizeof(int) x 1 million x 2!
     * because we store the dimension and the ID in this cell!
     * )
     *
     * For storing purpose, use just store the indexes of the cells or a tailor-made container like the K_cells class.
     */
    class Cell_as_faces {
    public:
        using Faces_container = Cell_incidence;

        Cell_as_faces (int cell_dimension, Cell_id id, const Faces_container& faces):
                cell_dimension {cell_dimension},
                id {id},
                faces_in_container {faces}
        { }

        Cell_id get_num_faces(int face_dim) const {
            return faces_in_container.get_number_of_incidence(
                    get_cell_dimension(),
                    id,
                    face_dim
            );
        }

        int get_cell_dimension() const {
            return cell_dimension;
        }

        Cell_id get_id_of_face(int face_dim, Cell_id face) const {
            return faces_in_container.get_incident_cell_of_dim(
                    get_cell_dimension(),
                    face_dim,
                    id,
                    face
            );
        }

        Cell_id get_cell_id () const {
            return id;
        }

    private:
        const int cell_dimension;
        const Cell_id id;
        const Faces_container& faces_in_container;
    };

    class Cell_as_vertices: private Cell_as_faces {
    public:
        using typename Cell_as_faces::Faces_container;
        using Cell_as_faces::Cell_as_faces;

        constexpr int get_vertex_dimension() const {return 0;}

        Cell_id get_num_vertexes() const {
            return get_num_faces(get_vertex_dimension());
        }

        Cell_id get_id_of_vertex(Cell_id vertex) const {
            return get_id_of_face(get_vertex_dimension(), vertex);
        }
    };

    class Cell_as_fullfaces: private Cell_as_faces, public Cell_as_fullfaces_base {
    public:
        using typename Cell_as_faces::Faces_container;

        using Cell_as_faces::Cell_as_faces;

        int get_fullface_dimension() const {
            return get_cell_dimension() - 1;
        }

        Cell_id get_num_fullfaces() const override {
            return get_num_faces(get_fullface_dimension());
        }

        Cell_id get_id_of_fullface(Cell_id face) const override {
            return get_id_of_face(get_fullface_dimension(), face);
        }
    };

#if TPCL_cplusplus_20
    static_assert(Cell_fullface_accessor_concept_valid<Cell_as_fullfaces>);
#endif

    /**
     * This is a tailor-made container for k-cells.
     */
    using K_cells = K_cells_accessor<
            Cell_as_vertices,
            Cell_as_fullfaces>;
}

#endif //TOPOCULE_K_CELLS_H
