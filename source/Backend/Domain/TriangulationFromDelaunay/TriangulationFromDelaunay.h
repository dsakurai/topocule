//
// Created by Daisuke Sakurai on 2019/11/14.
//

#ifndef TOPOCULE_TRIANGULATIONFROMDELAUNAY_H
#define TOPOCULE_TRIANGULATIONFROMDELAUNAY_H

#include <Ttk_nd_triangulation.h>

namespace tpcl {

    /**
     *
     * @todo Move TriangulationFromDelaunay to the Geometry module
     *
     * This glues the Ttk_nd_triangulation data storage and the triangulation algorithm.
     * This function itself is isolated from the implementation of the triangulation.
     */
    template<class ComputeDelaunay>
    void triangulationFromDelaunay(const ComputeDelaunay &computeDelaunay,
                                   Ttk_nd_triangulation &triangulation//,
//                                   const& vector<bool>
                                   ) {
        computeDelaunay.to_ttk_nd_triangulation(triangulation);
    }

}

#endif //TOPOCULE_TRIANGULATIONFROMDELAUNAY_H
