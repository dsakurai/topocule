//
// Created by Daisuke Sakurai on 2019/11/14.
//

#include <array>
#include <test.h>

#include <ComputeDelaunay.h>
#include <TriangulationFromDelaunay.h>

using namespace tpcl;

TEST_CASE ("Test TriangulationFromDelaunay") {
    // Easy-to-test triangulation
    auto pointsIn = std::make_shared<std::vector<std::array<double,2>>>();
    *pointsIn = {
            {0.0, 0.0},
            {1.0, 0.0},
            {0.0, 1.0},
            {1.0, 1.0}
    };

    ComputeDelaunay<std::vector<std::array<double,2>>> adj;
    adj.setPoints(pointsIn);
    CHECK(pointsIn->size() == adj.getNumberOfFinitePoints());

    adj.doIt();

    Ttk_nd_triangulation tri;

    triangulationFromDelaunay(adj, tri);

    std::vector<std::vector<int>> adjacents;
    for (int p = 0; p < 4; ++p) {
        std::vector<int> points;
        // @todo pass this to TTK
        size_t numNeigh = tri.getVertexNeighborNumber(p);
        for (size_t n = 0; n < numNeigh; ++n) {
            ttk::SimplexId neigh;
            tri.getVertexNeighbor(p, n, neigh);
            points.emplace_back(neigh);
        }
        adjacents.emplace_back(std::move(points));
    }
    decltype(adjacents) expect = {
            {1,2},
            {0,2,3},
            {0,1,3},
            {1,2}
    };
    CHECK(adjacents == expect);
}
