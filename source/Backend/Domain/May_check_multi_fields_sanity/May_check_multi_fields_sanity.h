//
// Created by Daisuke Sakurai on 2020-12-07.
//

#ifndef TOPOCULE_MAY_CHECK_MULTI_FIELDS_SANITY_H
#define TOPOCULE_MAY_CHECK_MULTI_FIELDS_SANITY_H


namespace tpcl {

    class May_check_multi_fields_sanity {
    public:
        virtual void check_multi_fields_sanity() const =0;
    };

}


#endif //TOPOCULE_MAY_CHECK_MULTI_FIELDS_SANITY_H
