//
// Created by Daisuke Sakurai on 2020-07-18.
//

#include <test.h>

#include <Cell_incidence.h>

#include <array>
#include <vector>

namespace {
    using namespace tpcl;

    struct Cell {
        int dimension;
        Cell_id cell_id;
    };

    /**
     * Cells can be either of the same dimension or a different one.
     */
    struct Connectivity {
        enum order {
            lower_dimension,
            higher_dimension
        };
        std::array<Cell,2> cells;
    };
}

TEST_CASE ("Test Cell_incidence") {
    using namespace tpcl;
    using namespace std;

    int fullcell_dimension = 3;

    /*
     * two tetrahedra (0 and 1) facing at triangle 0
     */

    vector<Connectivity> incidence = {
            Connectivity {
                    Cell {.dimension= 2, .cell_id= 0},
                    Cell {.dimension= 3, .cell_id= 0}
            },
            Connectivity {
                    Cell {.dimension= 2, .cell_id= 0},
                    Cell {.dimension= 3, .cell_id= 1}
            }
    };

    auto max_of = [&](auto cell_info) {
        Cell_id mx = -1;
        for (auto& c: incidence)
            for (auto& cl: c.cells)
                mx = std::max(mx, (decltype(mx)) cell_info(cl));
        return mx;
    };

    auto cell_dim = [&](Cell& cell) {
        return cell.dimension;
    };
    auto cell_id = [&](Cell& cell) {
        return cell.cell_id;
    };

    // Max of cell dimension
    int max_dim = max_of(cell_dim);

    // return the inverted binary for the argument
    // 1 => 0
    // 0 => 1
    auto opposite = [](int b) {return (b) ? 0 : 1;};

    vector<vector<vector<vector<Cell_id>>>> incidence_expected;
    incidence_expected.resize(max_dim + 1);

    Cell_incidence connectivity {fullcell_dimension, /*simplex*/ true };

    CHECK(connectivity.get_maximal_cell_dimension() == fullcell_dimension);

    WHEN ("Inserting bidirectional and unidirectional links") {
        const bool both_directions = true;
        const int dim_0 = 0;
        const int dim_1 = 1;
        const Cell_id vertex_0 = 0;
        const std::vector<Cell_id> incident_edges {0, 1, 2};

        auto next = incident_edges.begin();

        connectivity.insert_incidence(dim_0, dim_1, vertex_0, *(next++), both_directions);
        {
            INFO("No unidirectional link: should be bidirectional");
            CHECK(connectivity.is_bidirectional(dim_0, dim_1));
        }

        connectivity.insert_incidence(dim_0, dim_1, vertex_0, *(next++), false /*uni-direction*/);
        {
            INFO("Uni-directional link is inserted: should be declared unidirectional");
            CHECK(connectivity.is_bidirectional(dim_0, dim_1) == false);
        }

        connectivity.insert_incidence(dim_0, dim_1, vertex_0, *(next++), both_directions);
        {
            INFO("Bi-directional link is inserted: should not change the state");
            CHECK(connectivity.is_bidirectional(dim_0, dim_1) == false);
        }

        {
            INFO("Checking iterator access");
            Cell_incidence::Cell_connectivity_of_dimensions access {connectivity, dim_0, dim_1};
            auto dim_0_dim_1_edge_0_connections = access.begin();
                    CHECK_MESSAGE(dim_0_dim_1_edge_0_connections != access.end(), "Vertex-to-edge connection");
                    CHECK_MESSAGE((*dim_0_dim_1_edge_0_connections) == incident_edges, "No vertex-to-edge connection");
                    CHECK_MESSAGE(++dim_0_dim_1_edge_0_connections == access.end(), "No further vertex-to-edge connection");
        }

        {
            INFO("Checking iterator access");
            Cell_incidence::Const_cell_incidence_accessor access {connectivity, dim_0};
            auto connections = access.begin(); // dim 0 -> dim 0
            CHECK_MESSAGE(!connections.is_incidence_set(), "No connection from vertex to vertex");
            CHECK_MESSAGE((*(++connections)).size() == 1, "one edge keeps a connection to an edge");
            CHECK_MESSAGE((*connections)[0] == incident_edges, "Vertex to edge");
        }
        {
            INFO("Checking iterator access");
            Cell_incidence::Const_cell_incidence_accessor access {connectivity, dim_0, dim_1, vertex_0};
            auto vertex_0_dim_1_connections = access.begin();
            auto expected = incident_edges.begin();
            CHECK_MESSAGE(vertex_0_dim_1_connections != access.end(),     "Still has item to access");
            CHECK_MESSAGE(*vertex_0_dim_1_connections == *expected,       "0-th edge");
            CHECK_MESSAGE(++vertex_0_dim_1_connections != access.end(),   "Still has item to access");
            CHECK_MESSAGE(*(vertex_0_dim_1_connections) == *(++expected), "1st edge");
            CHECK_MESSAGE(++vertex_0_dim_1_connections != access.end(),   "Still has item to access");
            CHECK_MESSAGE(*(vertex_0_dim_1_connections) == *(++expected), "2nd edge");
            CHECK_MESSAGE(++vertex_0_dim_1_connections == access.end(),   "Has no item to access");
        }
    }

    WHEN ("Inserting everything") {
        for (auto& incident: incidence) {
            for(int c = 0; c < 2; ++c) {
                Cell_id this_id  = incident.cells[c].cell_id;
                int this_dim     = incident.cells[c].dimension;
                Cell_id that_id  = incident.cells[opposite(c)].cell_id;
                int     that_dim = incident.cells[opposite(c)].dimension;
                if (auto& cells = incidence_expected[this_dim]; cells.size() < this_id + 1) {
                    cells.resize(this_id + 1);
                    cells[this_id].resize(max_dim + 1);
                }
                incidence_expected[this_dim][this_id][that_dim].emplace_back(that_id);
            }
        }

        for (auto& c: incidence) {
            connectivity.insert_incidence(
                    c.cells[Connectivity::lower_dimension].dimension,
                    c.cells[Connectivity::higher_dimension].dimension,
                    c.cells[Connectivity::lower_dimension].cell_id,
                    c.cells[Connectivity::higher_dimension].cell_id,
                    /*both_directions*/ true
            );
            auto& vect = connectivity.get_incidence(
                    c.cells[Connectivity::lower_dimension].dimension,
                    c.cells[Connectivity::higher_dimension].dimension,
                    c.cells[Connectivity::lower_dimension].cell_id
            );
            CHECK_MESSAGE(vect.end() != std::find(vect.begin(), vect.end(), c.cells[Connectivity::higher_dimension].cell_id), "cell connection inserted");
            auto& vect_backwards = connectivity.get_incidence(
                    c.cells[Connectivity::higher_dimension].dimension,
                    c.cells[Connectivity::lower_dimension].dimension,
                    c.cells[Connectivity::higher_dimension].cell_id
            );
            CHECK_MESSAGE(vect_backwards.end() != std::find(vect_backwards.begin(), vect_backwards.end(), c.cells[Connectivity::lower_dimension].cell_id), "cell connection inserted");
        }
    }
}
