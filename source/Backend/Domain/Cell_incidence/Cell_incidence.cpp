//
// Created by Daisuke Sakurai on 2020-08-22.
//

#include <Cell_incidence.h>

#include <string>

namespace tpcl {

    void details::Cell_incidence_container_raw::allocate_incidence() {
        const auto required_size = get_maximal_cell_dimension() + 1;
        incidence->resize(required_size);
        for (auto& i: *incidence) {
            i.resize(required_size);
        }
    }

    void details::Cell_incidence_container_raw::allocate_incidence(int dim_a, int dim_b) {
        incidence->at(dim_a).at(dim_b) = std::make_shared<details::Cell_incidence_container_raw::Cell_to_cell>();
    }

    void details::Cell_incidence_container_raw::allocate_incidence(int dim_a, int dim_b, Cell_id cell) {
        if (dim_a == dim_b) [[TPCL_unlikely]] throw std::runtime_error {
                    "Cells of same dimensions supplied as forming 'incidence'."
                    "You probably want to record them as adjacencies instead."
            };

        // No reason to do this every time, but just to be sure.
        allocate_incidence();

        if (incidence->at(dim_a).at(dim_b) == nullptr) {
            allocate_incidence(dim_a, dim_b);
        }

        int old_size = incidence->at(dim_a).at(dim_b)->size();
        Cell_id new_size = cell + Cell_id{1};
        if (old_size < new_size) {
            incidence->at(dim_a).at(dim_b)->resize(new_size);
        }
    }

    void Cell_incidence::insert_incidence(int a_dim, int b_dim, Cell_id a_id, Cell_id b_id, bool both_directions) {
        insert_incidence_(a_dim, b_dim, a_id, b_id);
        if (both_directions) {
            insert_incidence_(b_dim, a_dim, b_id, a_id);
        } else {
            is_bidirectional(a_dim, b_dim) = false;
        }
    }

    bool details::Cell_incidence_container_raw::is_incidence_set(int higher_dim, int lower_dim) const {
        return (*incidence)[higher_dim][lower_dim] != nullptr;
    }
}
