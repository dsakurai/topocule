//
// Created by Daisuke Sakurai on 2020-09-29.
//


#include "Incidence_outputs.h"

#include <test.h>
#include <vector>

namespace {
    using namespace tpcl;

    void call(Incidence_outputs&& ios) {}
}

TEST_CASE ("Test Incident_outputs") {
    using namespace tpcl;

    Multi_vector<2, Cell_id> output_storage = {{1}};

    {
        INFO("Use the Incidence_outputs as rvalue.");
        call ( {{0, 1, output_storage}});
    }

    Incidence_outputs ios = {{0, 1, output_storage}};

    {
        INFO("Registered storage.");
        CHECK(ios.get_out_or_temporary(0, 1).size() == 1);
    }
    {
        INFO("Unregistered storage.");
        CHECK(ios.get_out_or_temporary(0, 2).size() == 0);
    }
}