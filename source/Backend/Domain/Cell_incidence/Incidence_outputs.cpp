//
// Created by Daisuke Sakurai on 2020-09-26.
//

#include "Incidence_outputs.h"

tpcl::Incidence_output::Datastrucure &tpcl::Incidence_outputs::get_out_or_temporary(int source_dim, int target_dim) {
    if (auto* found = get(source_dim, target_dim)) {
        return *found;
    } else {
        temporary.emplace_back(source_dim, target_dim);
        return temporary.rbegin()->links;
    }
}

tpcl::Incidence_output::Datastrucure *tpcl::Incidence_outputs::get(int source_dim, int target_dim) {
    if (auto got = details::Incidence_outputs_base::get(source_dim, target_dim)) return got;
    if (auto got = find_in_vector(source_dim, target_dim, temporary)) return got;
    return nullptr;
}
