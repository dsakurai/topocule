//
// Created by Daisuke Sakurai on 2020-05-10.
//

#ifndef TOPOCULE_CELL_INCIDENCE_H
#define TOPOCULE_CELL_INCIDENCE_H

#include <Basics.h>
#include <May_handle_simplicial_complex.h>
#include <Multi_vector.h>

#include <memory>
#include <tuple>
#include <utility>
#include <vector>

namespace tpcl {

    template<class Container_s, class Container_t>
    void copy_from_beginning(const Container_s& source, Container_t& target) {
        target.resize(source.size());
        std::copy(source.begin(), source.end(), target.begin());
    }

    namespace details {
        class Directed: private std::vector<char>{
            using container = std::vector<char>;
        public:
            Directed (int maximal_dimension):
                    maximal_dimension {maximal_dimension} {
                resize(maximal_dimension*maximal_dimension, true);
            }
            Directed () = delete;

            char& is_bidirectional(int a_dim, int b_dim) {
                return container::operator[](position_in_matrix(a_dim, b_dim));
            }

            char is_bidirectional(int a_dim, int b_dim) const {
                return container::operator[](position_in_matrix(a_dim, b_dim));
            }
        private:

            int position_in_matrix(int a_dim, int b_dim) const {
                if (a_dim > b_dim) TPCL_unlikely { std::swap(a_dim, b_dim); }
                return a_dim + maximal_dimension * b_dim;
            }

            int maximal_dimension;
        };
    }


    namespace details {
        class Incidence_may_be_directed {
        public:
            explicit
            Incidence_may_be_directed (int maximal_dimension):
                    maximal_dimension{maximal_dimension},
                    directed{maximal_dimension}
            {}

            int get_maximal_cell_dimension() const {
                return maximal_dimension;
            }

            char &is_bidirectional(int a_dim, int b_dim) {
                return directed.is_bidirectional(a_dim, b_dim);
            }

            char is_bidirectional(int a_dim, int b_dim) const {
                return directed.is_bidirectional(a_dim, b_dim);
            }

        private:
            const int maximal_dimension;
            details::Directed directed;
        };

        class Incidence_may_handle_simplicial_complex:
                public May_handle_simplicial_complex,
                public Simplicial_complex_validator_quick
        {
        public:
            Incidence_may_handle_simplicial_complex(bool for_simplicial_complex):
                    Simplicial_complex_validator_quick{for_simplicial_complex}
            {}

        private:
            bool is_for_simplicial_complex() const override {
                return Simplicial_complex_validator_quick::is_for_simplicial_complex();
            }
        };

        /**
         * This is a low-level API for maintaining cell incidence.
         */
        class Cell_incidence_container_raw:
                public Incidence_may_be_directed,
                public Incidence_may_handle_simplicial_complex {
        public:
            explicit
            Cell_incidence_container_raw(int maximal_dimension, bool for_simplicial_complex):
                    Incidence_may_be_directed {maximal_dimension},
                    Incidence_may_handle_simplicial_complex {for_simplicial_complex}
            {
                allocate_incidence();
            }

            // Allocate incidence for maximal_dimension x maximal_dimension
            void allocate_incidence();

            void allocate_incidence(int dim_a, int dim_b);

            // Allocate incidence for this cell
            void allocate_incidence(int dim_a, int dim_b, Cell_id cell);

            /**
             *
             * @tparam Params: int dim_a, int dim_b, Cell_id cell_a; later arguments can be omitted.
             * @param params
             * @return
             *
             * The parameters follow the convention of Cell_incidence::get_incident_cell_of_dim
             *
             * @sa get_incidence_mutable
             */
            const auto &get_incidence() const {
                return (*incidence);
            }
            const auto &get_incidence(int dim_a) const {
                return get_incidence()[dim_a];
            };
            const auto &get_incidence(int dim_a, int dim_b) const {
                return *get_incidence(dim_a)[dim_b];
            };
            const auto &get_incidence(int dim_a, int dim_b, Cell_id cell_a) const {
                // Crash at this position usually means that the pointer to the vector is null.
                // Allocate the vector like `incidence[dim_a][dim_b] = make_shared<XXX>()`.
                return get_incidence(dim_a, dim_b)[cell_a];
            };
            const auto &get_incidence(int dim_a, int dim_b, Cell_id cell_a, Cell_id cell_b) const {
                return get_incidence(dim_a, dim_b, cell_a)[cell_b];
            };

            /**
             * This is the non-const version of `get_incidence`
             *
             * @tparam Params
             * @param params
             * @return
             *
             * @sa get_incidence
             */
            template<class... Params>
            auto &get_incidence_mutable(Params... params) {

                // invoke the const version
                const auto &const_out = get_incidence(params...);

                // get the non-const type
                using no_ref = typename std::remove_reference<decltype(const_out)>::type;
                using non_const = typename std::remove_const<no_ref>::type;

                return const_cast<non_const &>(const_out);
            };

            /**
             * Return the number of incident cells for some cell
             *
             * @param params: int cell_dim, int dim_incident, Cell_id cell
             *
             * The parameters follow the convention of Cell_incidence::get_incident_cell_of_dim
             */
            template<class... Params>
            inline int get_number_of_incidence(Params... params) const {
                // Crash at this position usually means that the pointer to the vector is null.
                // Allocate the vector like `incidence[dim_a][dim_b] = make_shared<XXX>()`.
                return get_incidence(params...).size();
            }

            bool is_incidence_set(int higher_dim, int lower_dim) const;

        private:

            using Cell_to_cell = tpcl::Multi_vector<2,Cell_id>;

            // auto& links  = incidence[dim_cell][dim_incident_cell]
            // Cell_id incident_cell_id = (*links)[cell_id][incident_cell]
            using Incidence = tpcl::Multi_vector<2, std::shared_ptr<Cell_to_cell> >;

            std::shared_ptr<Incidence> incidence = std::make_shared<Incidence>();
        };
    }


    /** Stores the incidence between cells.
     *
     * The storage is rather low-level.
     * Higher-level APIs on accessing faces, a containing cell,
     * or deciding the local dimension of a non-manifold,
     * are to be provided by more specialized classes.
     * Such specialized classes typically uses this class as inputs to
     * provide proper polymorphisms, semantics and efficiency.
     *
     * @sa K_cells, Cell_adjacency_in_manifold
     *
     */
    class Cell_incidence: public details::Cell_incidence_container_raw {
    public:
        explicit
        Cell_incidence (int maximal_dimension, bool for_simplicial_complex) :
                details::Cell_incidence_container_raw {maximal_dimension, for_simplicial_complex}
        {}

        /**
         * Return a cell incident to another cell
         *
         * @param cell: the cell on which the returned cell is incident to
         * @param dim_incident: dimension of incident cell
         * @param incident: `incident`-th cell that is incdent to `cell`, starting from 0
         */
        Cell_id get_incident_cell_of_dim(int cell_dim, int dim_incident, Cell_id cell, Cell_id incident) const {
            return get_incidence(cell_dim, dim_incident, cell, incident);
        }

        void insert_incidence(int a_dim, int b_dim, Cell_id a_id, Cell_id b_id, bool both_directions = false);

        /**
         * Copies a container of elements to the data structure.
         *
         * The definition should be in the header because this copy operation is very quick.
         */
        template<class... Params, class Elements>
        void copy_incidence(Elements &to_copy, Cell_id new_id, Params... params);;

        //
        // Details
        //

        template<class... Parameters>
        class Const_cell_incidence_accessor;
        using Cell_connectivity_of_dimensions = Const_cell_incidence_accessor</*dim*/int,/*dim*/int>;

    private:
        void insert_incidence_(int a_dim, int b_dim, Cell_id a_id, Cell_id b_id) {
            allocate_incidence(a_dim, b_dim, a_id);
            get_incidence_mutable(a_dim, b_dim, a_id).emplace_back(b_id);
        }
    };

    template<class... Params, class Elements>
    void Cell_incidence::copy_incidence(Elements &to_copy, Cell_id new_id, Params... params) {
        std::vector<Elements> &incidence = get_incidence_mutable(params...);

        if constexpr (TPCL_DEBUG)
            if (incidence.size()) TPCL_unlikely throw std::runtime_error{"Copy target not empty."};

        auto pos = incidence.begin() + new_id;
        incidence.insert(pos, to_copy);
    }

    /**
     * This class should not be used directly as it assumes
     * internal memory layout of the Cell_incidence class
     *
     * Use it like...
     * ~~~
     * auto iterators = Cell_connectivity_of_dimensions{cell_incidence, dim_a};
     * // or
     * auto iterators = Cell_connectivity_of_dimensions{cell_incidence, dim_a, cell_a};
     * // or
     * auto iterators = Cell_connectivity_of_dimensions{cell_incidence, dim_a, cell_a, dim_b};
     *
     * for( auto i = iterators.begin(); i != iterators.end(), ++i ) {
     *     // ...
     * }
     * ~~~
     *
     */
    template <class... Parameters>
    class Cell_incidence::Const_cell_incidence_accessor {
    public:
        Const_cell_incidence_accessor() = delete;
        Const_cell_incidence_accessor (const Cell_incidence& cell_incidence, const Parameters&... parameters):
                cell_incidence {cell_incidence},
                parameters {std::make_tuple(parameters...)}
        { }

        size_t size() const {
            return std::apply(
                    [&](Parameters... p) {
                        return cell_incidence.get_number_of_incidence(p...);
                    }, parameters
            );
        }

        auto begin() {
            return Iterator {cell_incidence, parameters};
        }

        auto begin() const {
            return Iterator {cell_incidence, parameters};
        }

        auto end() {
            return std::apply(
                    // We may have to get rid of this `&`.
                    [&](Parameters&... p){
                        return Iterator::end(cell_incidence, p...);},
                    parameters
            );
        }

        auto end() const {
            return std::apply(
                    [&](Parameters... p){
                        return Iterator::end(cell_incidence, p...);},
                    parameters
            );
        }

        class Iterator {
        public:
            Iterator() = delete;

            Iterator(const Cell_incidence& cell_connectivity,
                     std::tuple<Parameters...> parameters
            ):
                    cell_connectivity {cell_connectivity},
                    parameters {parameters}
            {}

            static Iterator end(
                    const Cell_incidence& cell_connectivity, Parameters&... parameters) {
                auto iter = Iterator {cell_connectivity, {}};
                iter.id = cell_connectivity.get_number_of_incidence(parameters...);
                return iter;
            }

            /**
             * @return: the actual return type may change in the future, so always use auto&.
             */
            const auto& operator*() const {
                auto get_incidence = [&](Parameters... parameters) -> /* return by reference */ auto& {
                    return (cell_connectivity.get_incidence(parameters..., id));
                };

                // apply tuple as parameters
                return std::apply(get_incidence, parameters);
            }

            auto generate_accessor() {
                auto tup = std::tuple_cat(parameters, std::make_tuple(id));
                return std::apply(
                        [&](auto... args){
                            return Const_cell_incidence_accessor<Parameters..., Cell_id>(cell_connectivity, args...);
                        },
                        tup);
            }

            /**
             * Behavior undefined for the iterator `end`.
             * @return
             */
            bool is_incidence_set() {
                auto is_incidence_set = [&](Parameters... parameters) {
                    return (cell_connectivity.is_incidence_set(parameters..., id));
                };

                return std::apply(is_incidence_set, parameters);
            }

            /**
             * Do not increment the end() iterator.
             * @return
             */
            Iterator& operator++() {
                ++id;
                return *this;
            }

            bool operator==(const Iterator& iter) const {
                return id == iter.id;
            }
            bool operator!=(const Iterator& iter) const {
                return !(id == iter.id);
            }

            size_t get_id () const {
                return id;
            }

        private:
            size_t id = 0;
            const Cell_incidence& cell_connectivity;
            std::tuple<Parameters...> parameters;
        };

    private:
        const Cell_incidence& cell_incidence;
        std::tuple<Parameters...> parameters;
    };

}


#endif //TOPOCULE_CELL_INCIDENCE_H
