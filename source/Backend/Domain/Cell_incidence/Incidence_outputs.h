//
// Created by Daisuke Sakurai on 2020-09-26.
//

#ifndef TOPOCULE_INCIDENCE_OUTPUTS_H
#define TOPOCULE_INCIDENCE_OUTPUTS_H

#include <Basics.h>

#include <iosfwd>
#include <memory>
#include <Multi_vector.h>
#include <vector>
#include <boost/core/noncopyable.hpp>

namespace std {
    template <class T>
    class initializer_list;
}

namespace tpcl {

    /**
     * This struct is copiable
     */
    struct Incidence_output{
        using Datastrucure = Multi_vector<2, Cell_id>;

        int source_dim, target_dim;
        Datastrucure & links;
    };

    namespace details {
        /**
         * This is the core structure, which is Incidence_outputs without its temporary storage
         */
        class Incidence_outputs_base {
        public:
            Incidence_outputs_base(std::initializer_list<Incidence_output>& incidence_io) :
                    incidence_io{incidence_io} {}

            /**
             * Get the data structure. If not set by the user, return null.
             * @param source_dim
             * @param target_dim
             * @return
             */
            virtual Incidence_output::Datastrucure *get(int source_dim, int target_dim) {
                return find_in_vector(source_dim, target_dim, incidence_io);
            }

        protected:
            template<class T>
            Incidence_output::Datastrucure *find_in_vector(int source_dim, int target_dim, std::vector<T> &data);

        private:
            std::vector<Incidence_output> incidence_io;
        };

    }

    namespace details {

        template<class T>
        Incidence_output::Datastrucure *
        Incidence_outputs_base::find_in_vector(int source_dim, int target_dim, std::vector<T> &data) {
            for (auto& io: data) {
                if (io.source_dim == source_dim && io.target_dim == target_dim) {
                    return &io.links;
                }
            }
            return nullptr;
        }
    }

    /**
     * This sets the I/O of an incidence calculation.
     * If an algorithm uses an incidence I/O not set by the user,
     * it is provided with a temporary storage.
     *
     * This simplifies the code in the algorithm.
     *
     * Use it like:
     *
     * ~~~
     * Cell_incidence ci;
     *
     * call_function({
     *   {source_dim_0, target_dim_0, ci.get_incidence(source_dim_0, target_dim_0)},
     *   {source_dim_1, target_dim_1, ci.get_incidence(source_dim_1, target_dim_1)}
     * });
     * ~~~
     *
     * Copy nor assignment is disabled because we need a more clever mechanism to share the temporary storage.
     */
    class Incidence_outputs: public details::Incidence_outputs_base, private boost::noncopyable {
    public:

        Incidence_outputs (std::initializer_list<Incidence_output>&& incidence_io):
                details::Incidence_outputs_base {
                        incidence_io
                }
        {}

        /**
         * Get the data structure.
         * If not set by the user, create a temporary data structure.
         * This can be used to create a temporary output storage to request
         * creating a particular incidence.
         *
         * The output may be discarded.
         *
         * @param source_dim
         * @param target_dim
         * @return
         */
        Incidence_output::Datastrucure& get_out_or_temporary(int source_dim, int target_dim);

        Incidence_output::Datastrucure* get(int source_dim, int target_dim) override;

    protected:
        /**
         * This is copiable
         */
        struct Storaged {
            // not to be set by the user
            std::shared_ptr<Incidence_output::Datastrucure> storage
                    = std::make_shared<Incidence_output::Datastrucure>();
        };
        /**
         * This is copiable
         */
        struct Incidence_inout_with_storage: private Storaged, public Incidence_output {
            Incidence_inout_with_storage(int source_dim, int target_dim):
                    Incidence_output {source_dim, target_dim, *storage} {}
        };

    private:
        std::vector<Incidence_inout_with_storage> temporary;
    };
}


#endif //TOPOCULE_INCIDENCE_OUTPUTS_H
