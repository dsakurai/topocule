//
// Created by Daisuke Sakurai on 2020-09-07.
//

#include <test.h>

#include <Cell_incidence.h>
#include <Cell_connectivity_as_ttk.h>

TEST_CASE("Test Cell_connectivity_as_ttk") {
    using namespace tpcl;
    Cell_incidence connectivity_tpcl {3, /*triangluation*/true};

    WHEN("No cells are set") {
        CHECK_THROWS_AS(Cell_connectivity_as_ttk {connectivity_tpcl}, Cell_connectivity_as_ttk::Exception_no_cell);
    }
    WHEN("Setting dummy incidence.") {
        connectivity_tpcl.allocate_incidence(dim<3>, dim<0>);
        WHEN ("Not a triangulation input") {
            connectivity_tpcl.set_for_simplicial_complex(false);
            CHECK_THROWS_AS(Cell_connectivity_as_ttk {connectivity_tpcl}, May_handle_simplicial_complex::Not_simplex_error);
        }
        WHEN("No cell dimension set") {
            Cell_connectivity_as_ttk connectivity_ttk {connectivity_tpcl};
            CHECK(connectivity_ttk.get_cell_dimension() == connectivity_tpcl.get_maximal_cell_dimension());
        }
    }
    WHEN ("A triangulation input") {
        WHEN("Cell dimension set") {
            const int dim_two = 2, dim_zero = 0;
            enum id {
                id_0,
                id_1,
                id_2,
                id_3,
            };
            const int dim_to_pass = dim_two;

            // first triangle
            connectivity_tpcl.insert_incidence(dim_two, dim_zero, id_0, id_0);
            connectivity_tpcl.insert_incidence(dim_two, dim_zero, id_0, id_1);
            connectivity_tpcl.insert_incidence(dim_two, dim_zero, id_0, id_2);

            // second triangle
            connectivity_tpcl.insert_incidence(dim_two, dim_zero, id_1, id_0);
            connectivity_tpcl.insert_incidence(dim_two, dim_zero, id_1, id_1);
            connectivity_tpcl.insert_incidence(dim_two, dim_zero, id_1, id_3);

            Cell_connectivity_as_ttk connectivity_ttk {connectivity_tpcl, dim_to_pass};

            CHECK(connectivity_ttk.get_cell_dimension() == dim_to_pass);

            auto cells_connectivity = connectivity_ttk.get_ttk_cells_connectivity();
            auto offset = connectivity_ttk.get_ttk_cells_offset();


            std::vector<Cell_id_long> expected_cells_connectivity = {id_0, id_1, id_2, id_0, id_1, id_3};
            std::vector<Cell_id_long> expected_offset;
            for (int c = 0; c < 2; ++c) {
                const int num_vertices = dim_two + 1;
                expected_offset.push_back(num_vertices * c);
            }
            expected_offset.push_back(expected_cells_connectivity.size());

            CHECK(expected_cells_connectivity == *cells_connectivity);
        }
    }
}
