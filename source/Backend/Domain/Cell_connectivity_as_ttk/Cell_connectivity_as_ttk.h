//
// Created by Daisuke Sakurai on 2020-09-04.
//

#ifndef TOPOCULE_CELL_CONNECTIVITY_AS_TTK_H
#define TOPOCULE_CELL_CONNECTIVITY_AS_TTK_H

//#include <Cell_incidence.h>

#include <Basics.h>

#include <memory>
#include <optional>
#include <stdexcept>
#include <vector>

namespace tpcl {
    class Cell_incidence;

    /**
     * Generate cell connectivity arrays to be passed to TTK.
     */
    class Cell_connectivity_as_ttk {
    public:
        /**
         * @param cell_connectivity
         * @param cell_dimension_optional: The dimension of the cells to be passed to TTK (if not provided, the maximum
         * cell dimension in cell_incidence is used.)
         */
        Cell_connectivity_as_ttk (const Cell_incidence& cell_connectivity, const std::optional<int> cell_dimension_optional = std::nullopt);

        const std::shared_ptr<const std::vector<Cell_id_long>> get_ttk_cells_connectivity() const {
            return ttk_cells_connectivity;
        }

        const std::shared_ptr<const std::vector<Cell_id_long>> get_ttk_cells_offset() const {
            return ttk_cells_offset;
        }

        const int get_cell_dimension() const {
            return cell_dimension;
        }

        class Exception_no_cell : public std::runtime_error {
        public:
            using std::runtime_error::runtime_error;
        };

    private:
        const int cell_dimension;

        // In theory this does not have to be copied from tpcl::Cell_incidence,
        // but the data type is unnecessarily as big as long long int
        // and compared to tpcl::Cell_incidence it even has an extra element that stores the number of cells.
        // It's simpler to stupidly copy everything we have...
        std::shared_ptr<std::vector<Cell_id_long>> ttk_cells_connectivity;

        std::shared_ptr<std::vector<Cell_id_long>> ttk_cells_offset;
    };
}

#endif //TOPOCULE_CELL_CONNECTIVITY_AS_TTK_H
