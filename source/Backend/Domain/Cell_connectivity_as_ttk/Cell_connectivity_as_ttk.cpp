//
// Created by Daisuke Sakurai on 2020-09-04.
//

#include "Cell_connectivity_as_ttk.h"

#include <Cell_incidence.h>

tpcl::Cell_connectivity_as_ttk::Cell_connectivity_as_ttk(const Cell_incidence &cell_connectivity, const std::optional<int> cell_dimension_optional):
        cell_dimension {(cell_dimension_optional)? *cell_dimension_optional : cell_connectivity.get_maximal_cell_dimension()},
        ttk_cells_connectivity {std::make_shared<typename decltype(ttk_cells_connectivity)::element_type>()},
        ttk_cells_offset {std::make_shared<typename decltype(ttk_cells_offset)::element_type>()}
{
    cell_connectivity.verify_simplicial_complex_quickly();
    if (!cell_connectivity.is_incidence_set(cell_dimension, dim<0>)) throw Exception_no_cell {"Cells not set"};

    const auto num_cells = cell_connectivity.get_number_of_incidence(cell_dimension, dim<0>);

    { // memory allocation for a simplicial complex
        const auto num_vertices_in_cell = cell_dimension + 1;
        const auto num_vertices = num_cells * num_vertices_in_cell;
        ttk_cells_connectivity->reserve(num_vertices);
        ttk_cells_offset->reserve(num_cells + /*record length of connectivity array*/ 1);
        ttk_cells_offset->resize(num_cells);
        for (int i = 0; i < num_cells; ++i) {
            (*ttk_cells_offset)[i] = num_vertices_in_cell * i;
        }

        ttk_cells_offset->emplace_back(num_vertices);
    }

    auto cell_to_vertex = Cell_incidence::Const_cell_incidence_accessor<int, int> {
            cell_connectivity, cell_dimension, dim<0>};

    for (auto & cell_as_vertices: cell_to_vertex) {
        ttk_cells_connectivity->insert(
                ttk_cells_connectivity->end(),
                cell_as_vertices.begin(),
                cell_as_vertices.end()
                );
    }
}

