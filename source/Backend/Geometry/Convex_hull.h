//
// Created by Daisuke Sakurai on 2020-12-31.
//

#ifndef TOPOCULE_CONVEX_HULL_H
#define TOPOCULE_CONVEX_HULL_H

#include <CGAL/ch_graham_andrew.h>

namespace tpcl::geometry {
    /**
     * Redirects to CGAL's Graham-Andrew algorithm
     * @tparam Args
     * @param args
     * @return
     */
    template <class... Args>
    auto convex_hull_graham_andrew (Args&&... args) {
        return CGAL::ch_graham_andrew(std::forward<Args>(args)...);
    }

}

#endif //TOPOCULE_CONVEX_HULL_H
