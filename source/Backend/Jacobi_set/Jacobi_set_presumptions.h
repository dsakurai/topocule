//
// Created by Daisuke Sakurai on 2020-12-19.
//

#ifndef TOPOCULE_JACOBI_SET_PRESUMPTIONS_H
#define TOPOCULE_JACOBI_SET_PRESUMPTIONS_H

#include <Basics.h>

#include <array>
#include <vector>

namespace tpcl {

    namespace jacobi_set {

        // [component][cell]
        using Fiber_component = std::vector<Cell_id>;

        // [negative_side or positive_side][component]
        using Fiber_components_on_both_sides = std::array<std::vector<Fiber_component>,2>;

        static constexpr int dim_range = 2;
        static constexpr int dim_edge = dim_range - 1;

        using Point = int;
        using Cell = Cell_id;

        struct Cell_fragment {
            Cell_id cell_id;
            Cell_id fragment_id;
        };
        using Cell_fragments = std::vector<Cell_fragment>;

        // position of a component relative to the edge
        enum Side { degenerate = -1, negative_side = 0, positive_side = 1 };

        struct Jacobi_edge {
            Jacobi_edge (
                    Cell_id edge_id,
                    std::array<std::vector<Fiber_component>,2>& components_on_both_sides
            ):
                    edge_id {edge_id},
                    fiber_components_on_both_sides {std::move(components_on_both_sides)}
            {}
            Jacobi_edge (
                    Cell_id edge_id,
                    std::array<std::vector<Fiber_component>,2>&& components_on_both_sides
            ):
                    edge_id {edge_id},
                    fiber_components_on_both_sides {std::move(components_on_both_sides)}
            {}

            Cell_id edge_id;
            // the components on the negative sides are the 0th element; those on the positive side the 1st
            std::array<std::vector<Fiber_component>,2> fiber_components_on_both_sides;
        };

        using Jacobi_edges = std::vector<jacobi_set::Jacobi_edge>;
    }

}

#endif //TOPOCULE_JACOBI_SET_PRESUMPTIONS_H
