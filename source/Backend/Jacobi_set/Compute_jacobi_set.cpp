//
// Created by Daisuke Sakurai on 2020-05-10.
//

#include <Compute_jacobi_set.h>

#include <Cell_incidence.h>
#include <Disjoint_sets.h>

#include <array>
#include <memory>

namespace tpcl::jacobi_set::details {

    std::array<std::vector<Fiber_component>, 2> Reconnect_components::do_it() {
        decltype(do_it()) fiber_components_on_both_sides;

        std::vector<Cell_id> cell_id_to_fragment_id_tmp;

        // for the negative and positive sides
        for (int np = 0; np < 2; ++np) {
            const auto& fragmented_cells = fragments.fragments_on_both_sides[np];
            const auto& cell_id_2_fragment_id = fragments.cell_id_2_fragment_id_on_both_sides[np];
            auto& fiber_components = fiber_components_on_both_sides[np];
            treat_each_side(
                    fragmented_cells,
                    cell_id_2_fragment_id,
                    fiber_components
            );
        }

        return std::move(fiber_components_on_both_sides);
    }

    void Reconnect_components::treat_each_side(const Cell_fragments &fragmented_cells,
                                               const Reconnect_components::Cell_id_2_fragment_id &cell_id_2_fragment_id,
                                               std::vector<Fiber_component> &fiber_components) {

        std::vector<Cell_id> cells_tmp;

        Disjoint_sets disjoint_sets (fragmented_cells.size());

        // @todo: adjacency around the checked edge

        for (auto[cell_a, cell_b]: *get_adjacency()) {
            auto fragment_a = cell_id_2_fragment_id[cell_a];
            auto fragment_b = cell_id_2_fragment_id[cell_b];

            // If fragments exist
            if ((fragment_a != UNUSED) && (fragment_b != UNUSED) ) disjoint_sets.union_set(fragment_a, fragment_b);
        }

        const auto num_fragments = disjoint_sets.get_num_elements();
        // Keep the fiber components as cells touching the components.
        // Each fiber component touches a cells.
        // `cell_ids_in_fiber_component[fragment]` gives an array of such cells' IDs
        std::vector<Fiber_component> cell_ids_in_fiber_component (num_fragments);

        // Keep the cells that represent a fiber component
        std::vector<Cell_id> reprensentative_fragments;

        using Fragment_id = Cell_id;

        // Reconnect the fragments into a component
        for (Fragment_id fragment = 0; fragment < num_fragments; ++fragment) {
            auto fiber_component_fragment = disjoint_sets.find_set(fragment);

            // Get the cell ID of this cell fragment cut out by the input edge
            const Cell_fragment& cell_fragment = fragmented_cells[fragment];
            cell_ids_in_fiber_component[fiber_component_fragment].emplace_back(cell_fragment.cell_id);

            // keep if representative
            if (fiber_component_fragment == fragment) reprensentative_fragments.emplace_back(fiber_component_fragment);
        }

        for (auto& fiber_component: reprensentative_fragments) {
            fiber_components.emplace_back(std::move(cell_ids_in_fiber_component[fiber_component]));
        }
    }

}
