//
// Created by Daisuke Sakurai on 2019/11/25.
//

#include <test.h>

#include <Cell_incidence.h>
#include <Compute_jacobi_set.h>
#include <Multi_fields.h>
#include <Tessellation.h>

#include <memory>
#include <nlohmann/json.hpp>

using namespace tpcl;
using json  = nlohmann::json;

TEST_CASE ("Test Jacobi_set") {

    using Fiber_component = tpcl::jacobi_set::Fiber_component;

    auto multi_fields = std::make_shared<tpcl::Multi_fields>();

    auto tessellation = std::make_shared<tpcl::Tessellation>();

    const int negative_side  = 0; const int positive_side = 1;

    // prepare multi-fields

    // allocate objects
    std::shared_ptr<tpcl::Cell_adjacency_in_manifold> adjacency;
    std::shared_ptr<tpcl::Cell_incidence> incidence;
    using Values = std::vector<double>;
    auto field_values_0 = std::make_shared<tpcl::Field_values_of_type<typename Values::value_type>>("field_values_0");
    auto field_values_1 = std::make_shared<tpcl::Field_values_of_type<typename Values::value_type>>("field_values_1");

    INFO("Edge 0 with endpoint vertices 0 and 1 is to be tested if it is Jacobi");
    auto tessellation_common = [
            &adjacency,
            &tessellation,
            &incidence
    ](int dim_fullcells) { // parameters

        // As links of cells we need:
        // - Incidence from each edge to be tested to its full cofaces
        // - Incidence from each edge to be tested to their endpoints
        // - Incidence from each such a coface to its vertices
        // - Adjacency between all these cofaces

        adjacency = std::make_shared<tpcl::Cell_adjacency_in_manifold>(dim_fullcells, /*num_cells*/ 3);
        incidence = std::make_shared<tpcl::Cell_incidence>(dim_fullcells, /*simplicial*/ true);
        tessellation->set_maximal_dimension(dim_fullcells);
    };

    auto compose_multi_fields = [&adjacency, &incidence, &tessellation, &field_values_0, &field_values_1, &multi_fields]() {
        multi_fields->set_tessellation(tessellation);

        multi_fields->add_field_values(field_values_0);
        multi_fields->add_field_values(field_values_1);

        {// prepare tessellation
            tessellation->set_cell_adjacency(tessellation->get_maximal_dimension(), adjacency);
            tessellation->set_cell_incidence(incidence);
        }
        // Better implement and call multi_fields->check_multi_fields_sanity()
        tessellation->check_multi_fields_sanity();
    };

    int dim_fullcells = -1; // unset

    using with_degeneracy = geometry::Orientation_with_degeneracy<double>;
    using with_simulation_of_simplicity = geometry::Orientation_with_simulation_of_simplicity<double>;

    auto check_jacobi_set = [&multi_fields, &field_values_0, &field_values_1](
            auto orientation_kernel
            ) {
        return jacobi_set::Compute_jacobi_set<decltype(orientation_kernel)> {
                multi_fields,
                std::array<std::string, 2>{
                        field_values_0->get_name(),
                        field_values_1->get_name()
                }
        };
    };


    WHEN ("Degenerate triangle") {
        dim_fullcells = tpcl::dim<2>;

        tessellation_common(dim_fullcells);

        // One degenerate triangle
        //
        // 0,1--2

        // edge 0 -> vertex 0 & 1
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 0, 0);
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 0, 1);
        // edge 1 -> vertex 1 & 2
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 1, 1);
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 1, 2);

        // triangle 0 -> vertex
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 0);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 1);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 2);

        // edge 0 & 1 -> triangle
        incidence->insert_incidence(tpcl::dim<1>, dim_fullcells, 0, 0);
        incidence->insert_incidence(tpcl::dim<1>, dim_fullcells, 1, 0);

        // triangle -> triangle adjacency:
        // none

        tessellation->set_number_of_cells_for_dim(dim_fullcells, 1);
        tessellation->set_number_of_cells_for_dim(tpcl::dim<0>, 3);
        tessellation->set_number_of_cells_for_dim(tpcl::dim<1>, 2);

        // x-coords in the range
        field_values_0->set_values(std::make_shared<Values>(Values{
                /*    0    1    2  */
                /**/ 0.0, 0.0, 1.0
        }));
        // y-coords in the range
        field_values_1->set_values(std::make_shared<Values>(Values{
                /*    0    1    2  */
                /**/ 0.0, 0.0, 0.0
        }));

        compose_multi_fields();

        auto compute_jacobi_set = check_jacobi_set(with_degeneracy{});

        {
            auto check = [&](tpcl::Cell_id edge_id) {
                auto jacobi = compute_jacobi_set.is_jacobi_edge(/*edge*/ edge_id);
                CHECK(jacobi);

                // No component on one side of the Jacobi edge
                // (May be on the positive_side, actually...)
                CHECK((*jacobi)[negative_side].empty());
                // tets 0 and 1 are on the other side of the Jacobi edge
                CHECK((*jacobi)[positive_side].empty());
            };
            WHEN("Edge mapped to a point") {
                check(/*edge*/0);
            }
            WHEN("Cell mapped onto a line") {
                check(/*edge*/1);
            }
        }
    }
    WHEN ("Two  Tets") {
        dim_fullcells = tpcl::dim<3>;

        tessellation_common(dim_fullcells);

        // Two tets 0123 and 0124
        //
        // 2,3--1
        // |  X |
        // 0----4

        // edge -> vertex
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 0, 0);
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 0, 1);

        // tet 0 -> vertex
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 0);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 1);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 2);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 3);

        // tet 1 -> vertex
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 1, 0);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 1, 1);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 1, 2);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 1, 4);

        // edge -> tets
        incidence->insert_incidence(tpcl::dim<1>, dim_fullcells, 0, 0);
        incidence->insert_incidence(tpcl::dim<1>, dim_fullcells, 0, 1);

        // tet -> tet adjacency
        adjacency->insert_adjacency(0, 1);

        tessellation->set_number_of_cells_for_dim(dim_fullcells, 2);
        tessellation->set_number_of_cells_for_dim(tpcl::dim<1>, 1);
        tessellation->set_number_of_cells_for_dim(tpcl::dim<0>, 5);

        WHEN("Not a Jacobi edge") {

            // Consider these tets
            // 2,3--1
            // |  X |
            // 0----4

            // x-coords
            field_values_0->set_values(std::make_shared<Values>(Values{
                 // 0   1   2   3   4
                    0., 1., 0., 0., 1.
            }));
            // y-coords
            field_values_1->set_values(std::make_shared<Values>(Values{
                 // 0   1   2   3   4
                    0., 1., 1., 1., 0.
            }));

            compose_multi_fields();

            auto checker = [](const auto& compute_jacobi_set) {
                auto jacobi_output = compute_jacobi_set.is_jacobi_edge(/*edge*/ 0);
                CHECK(!jacobi_output);
            };
            {
                auto compute_jacobi_set = check_jacobi_set(with_degeneracy{});
                checker(compute_jacobi_set);
            }
            {
                auto compute_jacobi_set = check_jacobi_set(with_simulation_of_simplicity {});
                checker(compute_jacobi_set);
            }
        }
        WHEN ("A Jacobi edge of a definite (i.e. vanishing) fold") {

            // Consider these tets
            // 2,3,4--1
            // |  /
            // 0

            // x-coords in the range
            field_values_0->set_values(std::make_shared<Values>(Values{
                 // 0   1   2   3   4
                    0., 1., 0., 0., 0.
            }));
            // y-coords in the range
            field_values_1->set_values(std::make_shared<Values>(Values{
                 // 0   1   2   3   4
                    0., 1., 1., 1., 1.
            }));

            compose_multi_fields();

            auto checker = [](auto compute_jacobi_set) {
                auto jacobi_output = compute_jacobi_set.is_jacobi_edge(/*edge*/ 0);
                CHECK(jacobi_output);

                INFO("Check the contents of Jacobi");
                // No component on one side of the Jacobi edge
                // (May be on the positive_side, actually...)
                CHECK((*jacobi_output)[negative_side].empty());
                // tets 0 and 1 are on the other side of the Jacobi edge
                CHECK((*jacobi_output)[positive_side] == std::vector<Fiber_component>{{0, 1}});
            };

            checker(check_jacobi_set(with_degeneracy{}));
            checker(check_jacobi_set(with_simulation_of_simplicity{}));
        }
    }
    WHEN ("Three cells (which are triangles)") {
        dim_fullcells = tpcl::dim<2>;
        tessellation_common(dim_fullcells);

        // Consider these triangles
        // 2 -- 1
        //   X  /\
        // 3 - 0 - 4

        // edge -> vertex
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 0, 0);
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 0, 1);

        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 1, 0);
        incidence->insert_incidence(tpcl::dim<1>, tpcl::dim<0>, 1, 2);

        // triangle 0 -> vertex
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 0);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 1);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 0, 2);

        // triangle 1 -> vertex
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 1, 0);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 1, 1);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 1, 3);

        // triangle 2 -> vertex
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 2, 0);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 2, 1);
        incidence->insert_incidence(dim_fullcells, tpcl::dim<0>, 2, 4);

        // edge -> tets
        incidence->insert_incidence(tpcl::dim<1>, dim_fullcells, 0, 0);
        incidence->insert_incidence(tpcl::dim<1>, dim_fullcells, 0, 1);
        incidence->insert_incidence(tpcl::dim<1>, dim_fullcells, 0, 2);
        incidence->insert_incidence(tpcl::dim<1>, dim_fullcells, 1, 0);

        tessellation->set_number_of_cells_for_dim(dim_fullcells, 3);
        tessellation->set_number_of_cells_for_dim(tpcl::dim<1>, 2);
        tessellation->set_number_of_cells_for_dim(tpcl::dim<0>, 5);

        WHEN ("A Jacobi edge of a merging fold") {
            // x-coords and y-coords in the range
            field_values_0->set_values(std::make_shared<Values>(Values{
                 // 0   1   2   3   4
                    0., 1., 0., 0., 1.
            }));
            field_values_1->set_values(std::make_shared<Values>(Values{
                    0., 1., 1., 1., 0.
            }));

            using Fiber_components_on_both_sides = jacobi_set::Fiber_components_on_both_sides;
            auto check_edge = [](const Fiber_components_on_both_sides& jacobi, std::array<std::vector<Fiber_component>,2>& expected) {
                CHECK(jacobi == expected);
            };

            auto check_edge_0 = [&check_edge](const Fiber_components_on_both_sides& jacobi) {
                using Fiber_component = jacobi_set::Fiber_component;
                std::array<std::vector<Fiber_component>,2> expected = {
                        // components on the negative side of the Jacobi edge
                        std::vector<Fiber_component>{
                                Fiber_component{2} // component with one triangle
                        },
                        // components on the positive side of the Jacobi edge
                        std::vector<Fiber_component>{
                                Fiber_component{0}, // component with one triangle
                                Fiber_component{1}, // component with one triangle
                        }
                };
                check_edge(jacobi, expected);
            };

            compose_multi_fields();

            auto checker = [&check_jacobi_set, &check_edge_0, &check_edge](auto orientation_kernel) {
                auto compute_jacobi_set = check_jacobi_set(orientation_kernel);

                WHEN ("Testing if edge 0 is Jacobi") {
                    auto jacobi = compute_jacobi_set.is_jacobi_edge(/*edge*/ 0);
                            REQUIRE(jacobi);
                    check_edge_0(*jacobi);
                }

                WHEN ("Testing everything in the class") {
                    compute_jacobi_set.do_it();
                    auto& jacobi_set = *compute_jacobi_set.get_jacobi_set();
                            REQUIRE(jacobi_set.size() == 2);

                    {
                        INFO("Edge 0");
                        auto& jacobi = jacobi_set[0];
                        check_edge_0(jacobi.fiber_components_on_both_sides);
                    }
                    {
                        INFO("Edge 1");
                        auto& jacobi = jacobi_set[1];

                        std::array<std::vector<Fiber_component>,2> expected = {
                                // components on the negative side of the Jacobi edge
                                std::vector<Fiber_component>{
                                        Fiber_component{0} // component with one triangle
                                },
                                // components on the positive side of the Jacobi edge
                                std::vector<Fiber_component>{} // none
                        };

                        check_edge(jacobi.fiber_components_on_both_sides, expected);
                    }
                }
            };

            WHEN("With degeneracy") {
                checker(with_degeneracy{});
            }
            WHEN("With simulation of simplicity") {
                checker(with_simulation_of_simplicity{});
            }
        }
    }
}

