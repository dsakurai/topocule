//
// Created by Daisuke Sakurai on 2020-06-06.
//

#ifndef TOPOCULE_DISJOINT_SETS_H
#define TOPOCULE_DISJOINT_SETS_H

#include <array>
#include <vector>
#include <boost/pending/disjoint_sets.hpp>
#include <optional>

namespace tpcl {
    class Disjoint_sets {
    public:
        using id_type = int;

        Disjoint_sets () {}

        Disjoint_sets (size_t n) {
            initialize_elements(n);
        }

        void clear() {
            initialize_elements(0);
        }

        void union_set(size_t a, size_t b) {
            return ds->union_set(a, b);
        }

        size_t find_set(size_t a) {
            return ds->find_set(a);
        }

        id_type get_num_elements() {
            return parents.size();
        }

    private:
        void initialize_elements(size_t n) {
            ranks.resize(n);
            parents.resize(n);

            if (n) {
                ds.reset(new boost_disjoint_sets(&ranks[0], &parents[0]));
            } else {
                ds.reset(nullptr);
            }

            for (int i = 0; i < n; ++i) {
                ds->make_set(i);
            }
        }

    private:
        using boost_disjoint_sets = boost::disjoint_sets<id_type *, id_type *>;

        std::vector<id_type> ranks;
        std::vector<id_type> parents;
        std::unique_ptr<boost_disjoint_sets> ds;
    };
}

#endif //TOPOCULE_DISJOINT_SETS_H
