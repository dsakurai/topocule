Terminology
-----------

The terminology used in the source code of 
Jacobi set computation and its relatives
generally follow that found in CGAL.

A *fullcell* in a n-D space is a cell having n dimension.
We ignore whether it actually has non-zero volume.
