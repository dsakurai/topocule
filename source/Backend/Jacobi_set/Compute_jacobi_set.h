//
// Created by Daisuke Sakurai on 2020-05-10.
//

#ifndef TOPOCULE_COMPUTE_JACOBI_SET_H
#define TOPOCULE_COMPUTE_JACOBI_SET_H

#include <Basics.h>
#include <Copy.h>
#include <Cell_incidence.h>
#include <Cell_adjacency_in_manifold.h>
#include <Disjoint_sets.h>
#include <Jacobi_set_presumptions.h>
#include <Multi_fields.h>
#include <Orientation.h>
#include <Tessellation.h>

#include <array>
#include <boost/pending/disjoint_sets.hpp>
#include <optional>
#include <span>
#include <vector>

namespace tpcl {

    namespace jacobi_set {
        using Adjacency = Cell_adjacency_in_manifold;

        class Jacobi_set_algorithm_component {
        public:

            /**
             * @todo TODO We should create a copy constructor
             * to avoid unnecessary verifications and pre-computation
             * done in is_jacobi_edge etc.
             */
            Jacobi_set_algorithm_component(
                    const std::shared_ptr<Multi_fields> &multi_fields,
                    const std::array<std::string, 2> field_names
            ) :
                    multi_fields{multi_fields},
                    adjacency{multi_fields->get_tessellation()->get_cell_adjacency(
                            multi_fields->get_tessellation()->get_maximal_dimension())} {
                // Verification code
#warning /// @performance this is somewhat too heavy to be used by Reconnect_components for every iteration

                multi_fields->verify_simplicial_complex_quickly();

                if (!multi_fields->get_tessellation()->get_cell_adjacency(dim_fullcell()))
                    throw std::runtime_error{"Adjacency not set."};

                const auto &incidence = multi_fields->get_tessellation()->get_cell_incidence();

                if (!incidence)
                    throw std::runtime_error{"Incidence data structure is unset."};

                for (auto &dims: {
                        std::make_pair(dim_fullcell(), dim<0>),
                        std::make_pair(dim_edge, dim<0>),
                        std::make_pair(dim_edge, dim_fullcell()),
                }) {
                    if (!incidence->is_incidence_set(dims.first, dims.second))
                        throw std::runtime_error{TPCL_ERROR(
                                                         "Incidence of " + std::to_string(dims.first) + "-cells to " +
                                                         std::to_string(dims.second) + "-cells unset.")};
                }

                for (int i = 0; i < field_names.size(); ++i) {
                    auto &name = field_names[i];
                    auto &field = multi_fields->get_field_values(name);
                    using wanted = typename decltype(fields)::value_type::element_type;
                    if (std::shared_ptr<wanted> field_as_double = std::dynamic_pointer_cast<wanted>(field)) {
                        fields[i] = field_as_double;
                    }
                }
            }

            inline int dim_fullcell() const {
                if (int dim = get_triangulation()->get_maximal_dimension();
                        dim != get_triangulation()->UNSET) {
                    return dim;
                }
                // else
                throw std::runtime_error{"the dimensionality of fullcells is unset."};
                return get_triangulation()->UNSET;
            }

            const std::shared_ptr<Tessellation> &get_triangulation() const {
                return multi_fields->get_tessellation();
            }

            inline Cell_id get_number_of_fullcells() const {
                return get_triangulation()->get_number_of_cells_for_dim(dim_fullcell());
            }

            double get_field_value(int field, Cell_id vertex_id) const {
                return (*fields[field])[vertex_id];
            }

            const std::shared_ptr<Adjacency> &get_adjacency() const {
                return adjacency;
            }

        protected:
            // Used for ID mapping during Jacobi set computation.
            static constexpr Cell_id UNUSED = -1;
            struct Fragments {
                std::array<Cell_fragments,2> fragments_on_both_sides;
                std::array<std::vector<Cell_id>,2> cell_id_2_fragment_id_on_both_sides;
            };

        private:
            Jacobi_set_algorithm_component() = delete;

            // input
            const std::shared_ptr<Multi_fields> multi_fields;
            const std::shared_ptr<Adjacency> adjacency;
            std::array<std::shared_ptr<Field_values_of_type<double>>, 2> fields;
        };

        /**
         * Reports if edges are included in the Jacobi set.
         * To check one specific edge, call `is_jacobi_edge`.
         *
         * If an edge is included in the Jacobi set, it is called a *Jacobi edge*.
         * This class returns the fiber components around the Jacobi edges.
         * A component is expressed in the output as the cells that overlap with the component.
         * The components are either on the left-hand-side or right-hand-side of the edge.
         * ``Left" or ``right" to an edge is that of the edge when rotated so that
         * the 0th vertex is at the origin and the other vertex is on the positive side of the y-axis.
         * In this sense, the input edge is oriented according to the vertex IDs.
         */
        template <class Orientation_kernel> // @todo use concepts for Orientation_kernel
        class Compute_jacobi_set : public Jacobi_set_algorithm_component {
            Compute_jacobi_set() = delete;

        public:
            // constructor
            using Jacobi_set_algorithm_component::Jacobi_set_algorithm_component;

            void do_it();

            [[nodiscard]]
            inline const std::shared_ptr<Jacobi_edges> &get_jacobi_set() const;

            /**
             * Check if a given edge is in the Jacobi set.
             * If so, returns the fiber components.
             *
             * Note that returning by value does not trigger expensive copy operations thanks to modern C++ features.
             *
             * @param edge_id
             * @return
             */
             std::optional<Fiber_components_on_both_sides> is_jacobi_edge(Cell_id edge_id) const;

        private:
            static constexpr size_t num_endpoints_coords = /*2D*/dim_range * /*2 points*/ dim_range;

            template<class C>
            auto num(const C& c) const {
                return c.size();
            }

            struct Edge_info {
                Edge_info (const Jacobi_set_algorithm_component* presumption, Cell_id edge_id) :
                        edge_id {edge_id}
                {
                    // Find cells on the negative and positive side of the edge
                    // Note: we convert the edge ID to edge endpoints
                    auto edges = presumption->get_triangulation()->create_k_cells(dim_edge);

                    auto edge_cell = edges.get_cell_as_vertices(edge_id);

                    for (int v = 0; v < dim_edge + 1 /* == 2 */; ++v) {
                        auto vertex_id = edge_cell.get_id_of_vertex(v);
                        edge_endpoints_id[v] = vertex_id;
                        for ( int x = 0; x < dim_range; ++x) {
                            edge_endpoint_coords[x + (dim_edge + 1) * v] = presumption->get_field_value(x, vertex_id);
                        }
                    }
                }
                Cell_id edge_id;
                std::array<Cell_id,dim_range> edge_endpoints_id;
                std::array<double,num_endpoints_coords> edge_endpoint_coords;
            };

            std::shared_ptr<Jacobi_edges> jacobi_set = std::make_shared<Jacobi_edges>();

            Fragments check_sides(const Edge_info& edge_info) const;
        };

        namespace details {

            /**
             * When projected on to the range, an edge cuts cells into fragments.
             * We have one array of fragments on the negative and positive sides of the edge.
             * The array index of the fragment is called the fragment ID in this class.
             */
            class Reconnect_components: private Jacobi_set_algorithm_component {
            public:
                using Cell_id_2_fragment_id = std::vector<Cell_id>;

                Reconnect_components (
                        const Jacobi_set_algorithm_component& presumptions,
                        const Jacobi_set_algorithm_component::Fragments& fragments
                ):
                        Jacobi_set_algorithm_component {presumptions},
                        fragments {fragments}
                { }

                /**
                 * the negative side is the 0th element; the positive one the 1st
                 */
                std::array<std::vector<Fiber_component>,2> do_it();

            private:
                void treat_each_side (
                        const Cell_fragments& fragmented_cells,
                        const Cell_id_2_fragment_id& cell_id_2_fragment_id,
                        std::vector<Fiber_component>& fiber_components
                );
                const Jacobi_set_algorithm_component::Fragments& fragments;
            };
        }

        template <class Orientation_kernel>
        void Compute_jacobi_set<Orientation_kernel>::do_it() {

            const int num_edges = get_triangulation()->get_number_of_cells_for_dim(dim_edge);
            if (num_edges == get_triangulation()->UNSET) throw std::runtime_error{"edges are not set."};

            // @todo openmp
            //#pragma omp parallel
            //#pragma omp for
            for (int edge_id = 0; edge_id < num_edges; ++edge_id) {
                if (auto fiber_components = is_jacobi_edge(edge_id)) {
                    jacobi_set->emplace_back(
                            edge_id,
                            std::move(*fiber_components)
                    );
                }
            }
        }


        // Note that returning by value does not trigger expensive copy operations thanks to modern C++ features.
        template<class Orientation_kernel>
        std::optional<jacobi_set::Fiber_components_on_both_sides> Compute_jacobi_set<Orientation_kernel>::is_jacobi_edge(Cell_id edge_id) const {
            // To check if an edge is in the Jacobi set, we take a ball around it.
            //
            // Imagine an edge projected onto the range.
            // If a edge is in the Jacobi set, i.e. singular,
            // the inverse image of the link contains singularity.
            // That is, the number of connected component in the inverse image changes
            // as we cross the edge in the range.
            //
            // A vertex in a full cell around an edge in the domain
            // can be classified to be projected onto the negative or positive side
            // of the edge in the range (or it lies on the edge).
            //
            // We cut each cell into fragments so that the cell can be classified into
            // either negative or positive according to that in the range relative to the edge.
            //
            // Notice that the singularity of the edge can be checked through the connectivity of the negative and positive
            // label. A ball around a regular edge has exactly one connected component of volume consisting of points
            // classified to be on the negative side.
            // And the positive side, too. A ball around a singular edge will not follow this rule.
            //
            // In terms of code, we implement this check as the following steps:
            //
            // - project domain cells onto the range
            // - cut each projected cell into fragments:
            //        those on the negative side and the positive side of the edge
            // - re-connect the cut fragments if they're connected in the domain:
            //        use union-find
            // - decide if the edge is Jacobi

            constexpr auto with_degeneracy = Orientation_kernel::with_degeneracy;

            using Is_jacobi_optional = decltype(is_jacobi_edge(Cell_id{}));

            Is_jacobi_optional out = std::nullopt;  // evaluates to false when returned as is
            Fiber_components_on_both_sides components; // evaluates to true when returned

            Edge_info edge_info (this, edge_id);

            if constexpr (with_degeneracy) {
                // Edge maps to a single point?
                if (
                        (edge_info.edge_endpoint_coords[0] == edge_info.edge_endpoint_coords[2])
                        &&
                        (edge_info.edge_endpoint_coords[1] == edge_info.edge_endpoint_coords[3])
                        ) {
                    // Yes => the input edge is degenerate
                    // Therefore it is a Jacobi edge
                    out = std::make_optional(components);
                    return out;
                }

                // This block is particularly dependent on a 2D range
                static_assert(dim_range == 2);
            }

            // For cells around this edge
            auto fragments = check_sides(edge_info);

            if constexpr (with_degeneracy) {
                if (fragments.fragments_on_both_sides[negative_side].empty() &&
                    fragments.fragments_on_both_sides[positive_side].empty()
                        ) // degenerate case
                    out = std::make_optional(decltype(components){});
            }

            // Re-join the fragments into continua (actually fiber surfaces)
            components  = details::Reconnect_components{*this,
                                                        fragments
            }.do_it();

            if ((num(components[negative_side]) != 1) ||
                (num(components[positive_side]) != 1)
                    ) {

                out = // is Jacobi
                        std::make_optional<Fiber_components_on_both_sides>(
                                std::move(components)
                        );
            }

            return std::move(out);
        }

        template<class Orientation_kernel>
        typename Compute_jacobi_set<Orientation_kernel>::Fragments Compute_jacobi_set<Orientation_kernel>::check_sides(const Edge_info& edge_info) const {

            /// @performance use Reusable_containers
            decltype(check_sides(edge_info)) fragments;

            fragments.cell_id_2_fragment_id_on_both_sides[negative_side].resize(get_number_of_fullcells(), UNUSED);
            fragments.cell_id_2_fragment_id_on_both_sides[positive_side].resize(get_number_of_fullcells(), UNUSED);

            const auto& cell_connectivity = get_triangulation()->get_cell_incidence();
            auto num_incident_fullcells = cell_connectivity->get_number_of_incidence(
                    dim_edge,
                    dim_fullcell(),
                    edge_info.edge_id
            );

            auto fullcell_dim = dim_fullcell();
            auto fullcells = get_triangulation()->create_k_cells(fullcell_dim);

            for (int c = 0; c < num_incident_fullcells; ++c) {
                const auto fullcell_id = cell_connectivity->get_incident_cell_of_dim(dim_edge, fullcell_dim, edge_info.edge_id, c);
                // To check the position of a vertex relative to this edge, we compute the orientation
                // of the triangle spanned by the edge and the vertex
                std::array<Cell_id, (/*3 points*/dim_range + 1)>                  triangle_vertex_ids;   // NOLINT no need of initialization
                std::array<double,/*2D*/ dim_range * (/*3 points*/dim_range + 1)> triangle_vertices; // NOLINT no need of initialization

                basics::copy(edge_info.edge_endpoints_id, triangle_vertex_ids);
                basics::copy(edge_info.edge_endpoint_coords, triangle_vertices);

                const int num_vertices = fullcell_dim + 1;
                // flag that records whether we found a fragment on the negative or positive side
                std::array<bool, 2> found_fragment = {false, false};

                for (int v = 0; v < num_vertices; ++v) {
                    { // Prepare the triangle consisting of the edge and this vertex
                        // @todo dependency on range dimension == 2

                        Cell_id vertex_id = fullcells.get_id_of_vertex(fullcell_id, v);
                        { // if the vertex of this cell is shared with the edge, the vertex shall not be used for cutting the cell
                            auto vertex_shared_with_edge = std::find(edge_info.edge_endpoints_id.begin(), edge_info.edge_endpoints_id.end(),
                                                                     vertex_id);
                            if (vertex_shared_with_edge != edge_info.edge_endpoints_id.end()) continue;
                        }

                        *triangle_vertex_ids.rbegin() = vertex_id;
                        // @todo dependency on range dimension == 2
                        auto last_coord = triangle_vertices.rbegin();
                        // use the reverse iterator
                        // y-coordinate of this vertex
                        *(last_coord) = get_field_value(1, vertex_id);
                        // x-coordinate
                        *(++last_coord) = get_field_value(0, vertex_id);
                    }

                    // check if the vertex lies on the negative / positive side of the edge
                    Side side_of_fragment = [&triangle_vertices, &triangle_vertex_ids]() {
                        const auto sign = Orientation_kernel::template orientation<dim_range>(triangle_vertices, triangle_vertex_ids);

                        switch(sign) {
                            case geometry::NEGATIVE: return negative_side;
                            case geometry::POSITIVE: return positive_side;
                            default: return degenerate;
                        }
                    }();

                    if (side_of_fragment == degenerate) continue;

                    auto opposite_side = (side_of_fragment == negative_side) ? positive_side : negative_side;

                    // Already in the output? => Not interested.
                    if (found_fragment[side_of_fragment]) continue;
                    found_fragment[side_of_fragment] = true;

                    Cell_fragments *fragments_on_this_side = &fragments.fragments_on_both_sides[side_of_fragment];
                    std::vector<Cell_id> *cell_id_2_fragment_id = &fragments.cell_id_2_fragment_id_on_both_sides[side_of_fragment];

                    auto fragment_id = fragments_on_this_side->size();
                    fragments_on_this_side->emplace_back(Cell_fragment{
                            .cell_id = fullcell_id,
                            .fragment_id = Cell_id(fragment_id)
                    });
                    (*cell_id_2_fragment_id)[fullcell_id] = Cell_id(fragment_id);

                    // Found both negative and positive? => Mission complete for this cell.
                    if (found_fragment[opposite_side]) break;
                }
            }

            return std::move(fragments);
        }

        template<class Orientation_kernel>
        const std::shared_ptr<Jacobi_edges> &Compute_jacobi_set<Orientation_kernel>::get_jacobi_set() const {
            return jacobi_set;
        }

    }

} // namespace


#endif //TOPOCULE_COMPUTE_JACOBI_SET_H
