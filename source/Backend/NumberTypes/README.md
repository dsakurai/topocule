
Number Types
------------

Daisuke Sakurai

We experiment with multi-precision floating numbers here.
We follow the API of CGAL as much as our time allows.

First, get an idea on the algebraic foundations of CGAL in its manual (Fogel, E., Halperin, D., & Wein, R. (2012). CGAL arrangements and their applications: A step-by-step guide (Vol. 7). Springer Science & Business Media. Section 1.4.4 The Geometry Kernel of Cgal):

> The Cartesian<NumberType> class template models the Kernel concept. It uses a Cartesian representation of coordinates. The template parameter NumberType determines the type of the coordinates of the kernel objects.
>
>...
>
>The Cartesian<NumberType> class template uses reference counting of the geometric objects. Cgal also provides the class template Simple_cartesian<NumberType> a kernel that uses Cartesian representation but no reference counting.
>
>...
>
> Using homogeneous coordinates obviates the need for division operations in numerical computations, since the additional coordinate serves as a common denominator. Avoiding divisions can be useful for exact geometric computation.
>
> ...
>
> However, some operations provided by this kernel involve division, for example computing Cartesian coordinates. To keep the requirements on the number type parameter of Homogeneous low, the number type Quotient<RingNumberType> is used for operations that require division.
>
>...
>
>Cgal kernels apply ﬁltering techniques based on interval arithmetic [32,153] to achieve exact and eﬃcient predicate evaluation. Some kernels also apply lazy evaluations [65], where the exact computations are delayed at runtime until they are actually needed, if at all, to achieve exact and eﬃcient geometric-object constructions as well as to further expedite predicate evaluation.
>
>...
>
>For your convenience, Cgal provides some predeﬁned types of useful kernels. All predeﬁned kernels are Cartesian. They all support constructions of points from double-precision ﬂoating-point (double) Cartesian coordinates. They all provide exact geometric predicates, but they handle geometric constructions diﬀerently:
> 
> - `Exact_predicates_exact_constructions_kernel` provides exact geometric constructions, in addition to exact geometric predicates.
> 
> - `Exact_predicates_inexact_constructions_kernel` provides exact geometric predicates, but geometric constructions may be inexact due to roundoﬀ errors. Naturally, it is faster than the Exact_predicates_exact_constructions_kernel kernel; thus, it is preferable for geometric algorithms that just issue predicates involving their input entities and do not perform construction of new geometric objects (e.g., computing the convex hull of a set of points). In the context of arrangements, we usually have to construct points of intersection between input curves, so this kernel should be used with discretion.
>

The most interesting operation for us is to compute
the orientation predicate.
Mathematically, this is equivalent to the computation of
a matrix determinant. (See e.g. (Magalhães et al.~2017).)

### Notes on Machine-Epsilon Arithmetic

It is the stance of CGAL (as of version 5.1) to allow degenerate geometry.
Topocule, instead, avoids degeneracy by employing *Simulation of Simplicity* (SOS).
To do so, topocule lets CGAL's number type compute predicates, and if it is zero,
i.e. degeneracy is detected, it uses the SoS to resolve it.

Practically, the concept of machine epsilon in topological analysis for visualization had been simple:
it is sufficiently smaller than the smallest value in the used numbering system (such as the IEEE float).
The machine epsilon was used so that comparing two data values are never the same.
However, topocule's algorithms for multi-field topology, Jacobi set and Reeb space,
requires actual arithmetic like multiplication and subtraction.
And therefore we need a more sophisticated implementation.

In the original article by H. Edelsbrunner and E.P. Mücke, the arithmetic was treated to be compatible to,
$e^{i \epsilon}$, where $i$ is the ID of a vertex.
(He also treated the dimensionality as we do later in this text.)
Now, we need to implement the SoS, but, of course, we are not going to compute the
exponential in the normal sense.

Consider we compute the orientation of a triangle in 2D.
Let three points \f$(p,q,r)\f$ in the plane be the vertices of the triangle.
Mathematically, the orientation is defined as the determinant of the directed edges: 
\f[
\vec{pq}_x \vec{pr}_y - \vec{pq}_y \vec{pr}_x,
\f]
which is
\f[
(q_x - p_x) (r_y - p_y) - (q_y - p_y) (r_x - p_x) \qquad...(*)
\f]
where \f$v_x\f$ is the \f$x\f$-coordinate of the point \f$v\f$.

We denote
\f[
a_x = \hat{a}_x + \epsilon_x^{i(p)}, \qquad...(\dagger)
\f]
where \f$ \hat{a}_x \f$ is the original data value and \f$ \epsilon_x^{i(p)} \f$
is the machine epsilon. \f$i(p)\f$ is the index of \f$p\f$.

In scalar field topology it was necessary only to guarantee an order between any two
\{epsilon}s, such that \f$ \epsilon_x^{i} < \epsilon_x^{j}\f$ *iff* \f$i < j \f$.
This by itself, however, does not let us determine the sign of some numbers, such as
\f$\epsilon_x^{3} \epsilon_y^{4} - \epsilon_x^{2} \epsilon_y^{6} \f$ and 
\f$\epsilon_x^{3} \epsilon_y^{4} - \epsilon_x^{4} \epsilon_y^{3} \f$.

In the original version of the SoS this problem does not happen because there were
further assumptions on the machine epsilon.
While the reader can check the details in the article, we as users
only need to remember a few rules to decide the signs of determinants.

**TODO** We need to keep the number of swap operations while the sorting the
vertex indices.

### Notes on determinant computation in CGAL

CGAL uses `orientationC2` to determine the exact orientation
(after a probably optional heuristic speed-up fails).
I found an implementation of `orientationC2`, which internally
calls `sign_of_determinant()`.
This is (likely) implemented in `sign_of_deteminant.h`.
~~~
template <class RT>
inline
typename Sgn<RT>::result_type
sign_of_determinant( const RT& a00,  const RT& a01,
                        const RT& a10,  const RT& a11)
{
  return enum_cast<Sign>(CGAL_NTS compare( a00*a11, a10*a01));
}
~~~
(The code snippet also includes a call to `compare()`, which is likely implemented in `number_utils.h`.)
The problem with the code snipppet above is that it implies the full computations of the multiplication
of two big numbers, which is theoretically unnecessary.

Hence, one wonders there's a speed-up.
The following articles seem to be a good starting point:
> Burnikel, C., Funke, S., & Seel, M. (2001). Exact geometric computation using cascading. International Journal of Computational Geometry & Applications, 11(03), 245-266.
> http://graphics.stanford.edu/~sfunke/Papers/SoCG98/EXPCOMP.pdf
>
> Li, C., Pion, S., & Yap, C. K. (2005). Recent progress in exact geometric computation. The Journal of Logic and Algebraic Programming, 64(1), 85-111.
>
> Kaltofen, E., & Villard, G. (2004). Computing the sign or the value of the determinant of an integer matrix, a complexity survey. Journal of Computational and Applied Mathematics, 162(1), 133-146.

Putting these potential speed-ups aside, it seems that the fastest implementation available in CGAL, when using CGAL and
GMP only, is the Mpzf class that is implemented in CGAL. The predicate of big numbers that come from GMP (apparently)
requires the full computation of the determinant even when only the sign is of interest.
The GMP classes are located in `CGAL/Gmpzf.h`, which actually wraps the more low-level `Gmpzf_type.h`.
This appears to be the file in which the GMP API calls of multi-precision floats happen.
There's also the `Gmpq_type.h`, which implements the quotient representation.

Here is a quote from the CGAL's webpage (https://www.cgal.org/FAQ.html#inexact_NT):

> The good news is that CGAL provides an easy way for programmers to get around the limitations of inexact number types like double and float, through its templated kernels. See also our page on The Exact Computation Paradigm.
>
> A solution that always works (although it might slow down the algorithm considerably) is to use an exact multi-precision number type such as CGAL::Quotient<CGAL::MP_Float> or CGAL::Gmpq, instead of double or float.
>
> If your program does not involve the construction of new objects from the input data (such as the intersection of two objects, or the center of a sphere defined by the input objects), the CGAL::Exact_predicates_inexact_constructions_kernel kernel provides a nice and fast way to use exact predicates together with an inexact number type such as double for constructions.
>
> When your program uses constructions of new objects, you can still speed things up by using the number type CGAL::Lazy_exact_nt on top of an exact number type.
>
> Good references that discuss the robustness problem in geometric computing are:
>
> S. Schirra. Robustness and precision issues in geometric computation. Research Report MPI-I-98-004, Max Planck Institute for Computer Science, 1998. [ps-file] Revised version of: Robustness and precision issues in geometric computations. Ch 9, in M. van Kreveld et al. (eds.), Algorithmic Foundations of Geographic Information Systems, LNCS 1340, Springer, 1997.
>
> S. Schirra. Robustness and precision issues in geometric computation. in J.-R. Sack and J. Urrutia (eds.), Handbook of Computational Geometry Chapter 14, Elsevier Science, 1999
>
> Lutz Kettner, Kurt Mehlhorn, Sylvain Pion, Stefan Schirra, and Chee Yap. Classroom Examples of Robustness Problems in Geometric Computations, Computational Geometry: Theory and Algorithms, 40(1):61-78, 2008. [html]

References:
Magalhães, S. V., Franklin, W. R., & Andrade, M. V. (2017, November). Fast exact parallel 3D mesh intersection algorithm using only orientation predicates. In Proceedings of the 25th ACM SIGSPATIAL International Conference on Advances in Geographic Information Systems (p. 44). ACM.
