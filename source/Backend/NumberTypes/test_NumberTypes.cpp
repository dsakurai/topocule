//
// Created by Daisuke Sakurai on 2019/11/25.
//

#include <CGAL/Arr_non_caching_segment_traits_2.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Cartesian.h>
#include <CGAL/Gmpq.h>
#include <CGAL/Kernel/global_functions_2.h>

#include <test.h>

template <
        class Kernel
>
void check_arrangement_with_kernel() {
    // Caching is disabled for simplicity
    using ArrangementTraits      = CGAL::Arr_non_caching_segment_traits_2<Kernel>;

    using Point       = typename ArrangementTraits::Point_2;
    // Segment can be a line segment in reality.
    using Segment     = typename ArrangementTraits::X_monotone_curve_2;
    using Arrangement = CGAL::Arrangement_2<ArrangementTraits>;

    // 1 triangle in a plane
    Point p1(1, 1), p2(1, 2), p3(2, 1);
    Segment cv[] = {Segment(p1, p2), Segment(p2, p3), Segment(p3, p1)};

    Arrangement arr;
    insert(arr, &cv[0], &cv[sizeof(cv)/sizeof(Segment)]);

    // 2 faces
    CHECK(arr.number_of_faces() == 2);
}

template <
    class Number_type
    >
void check_arrangement_with_number_type() {
    using Kernel      = CGAL::Cartesian<Number_type>;
    check_arrangement_with_kernel<Kernel>();
}

TEST_CASE ("Test NumberTypes") {

    // Check that our number types run with CGAL's arrangement class.
    // This test, however, does not guarantee the correctness of the results.

    // Use the int type (inexact computation)
    check_arrangement_with_number_type<int>();
    // Use the quotient type of GMP
    check_arrangement_with_number_type<CGAL::Gmpq>();
    // Use CGAL's default exact geometry kernel
    check_arrangement_with_kernel<CGAL::Exact_predicates_exact_constructions_kernel>();
};
