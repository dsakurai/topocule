//
// Created by Daisuke Sakurai on 2020-08-12.
//

#include <test.h>
#include <Dat_file_reader.h>

using namespace tpcl;

template <class value_type>
void check(int dim, int num_points, Dat_file_reader& reader) {

    std::vector<double> points(dim * num_points);
    reader.set_output(points.data());
    reader.doIt();


    decltype(points) points_head, points_tail;
    std::copy(points.begin(), points.begin() + 6, std::back_inserter(points_head));
    std::copy(points.begin() + (dim * num_points - 6), points.end(), std::back_inserter(points_tail));

    const decltype(points) expected_head = {
            3.0722980e+000, 2.6771369e+000, 2.4513512e+000,
            2.9689485e+000, 2.8651284e+000, 3.0423970e+000,
    };
    const decltype(points) expected_end = {
            1.0604761e+000, 2.8117743e+000, 1.3601921e+000,
            9.0627878e-001, 3.1307771e+000, 1.6782627e+000,
    };

            CHECK(expected_head == points_head);
            CHECK(expected_end == points_tail);
}

TEST_CASE("Test Dat_file_reader") {

    using namespace std;

    //
    // Test the File reader
    //

    string coords_file = PENTANE_DATA_DIR "-reduced-dim/R_3D.txt";
    string field_file = PENTANE_DATA_DIR "-reduced-dim/chi.txt";

    {
        Dat_file_reader reader;
        reader.set_file_name(coords_file);

        size_t dim, num_points;
        reader.get_shape(dim, num_points);
                CHECK(dim == 3);
                CHECK(num_points == 10001);

        check<double>(dim, num_points, reader);
        check<float>(dim, num_points, reader);
    }
}
