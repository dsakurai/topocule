//
// Created by Daisuke Sakurai on 2019-09-15.
//

#include "Dat_file_reader.h"

#include <Printer.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

using namespace tpcl;

std::pair<std::string, std::string> strip_comments(const std::string& line) {

    std::pair<std::string, std::string> out;
    auto& out_line    = out.first;
    auto& out_comment = out.second;

    if (line.empty()) return out;

    // Hackish implementation
    if (line[0] == '#') {
        out_line = "";
        out_comment = line;
    } else {
        out_line = line;
        out_comment = "";
    }

    return out;
}

template<class value_type = float>
int read_contents(std::ifstream &infile,
                  size_t& dimX,
                  size_t& num_points,
                  const std::string& file_name,
                  Dat_file_reader::Line_filter* filter = nullptr,
                  value_type* points = nullptr
                  ) {

    dimX = 0;
    num_points = 0;

    int num_columns = 0;
    int chunk_count = 0;
    int line_count = 0;
    std::string row;
    std::string comment;

    // read a row
    bool first_time = true;
    while (std::getline(infile, row)) {

        std::tie(row, comment) = strip_comments(row);

        if (filter) {(*filter)(row, line_count++);}
        if (row.empty()) continue;

        std::istringstream iss(row);

        int nc; // num columns

        value_type value;
        nc = 0;
        while (iss >> value) {
            // found a column
            if (points) {
                points[chunk_count] = value;
                ++chunk_count;
            }
            ++nc;
        }
        ++num_points;

        // update the dimension
        if (first_time) {
            num_columns = nc;
            first_time = false;
        }

        if (nc == 0) break;

        // verify
        if (nc != num_columns) {
            ERROR_MESSAGE("input file " + file_name + " contains rows with variable number of items.")
            return 0;
        }
    }

    dimX = num_columns;

    return 1;
}


int Dat_file_reader::doIt() {
    using namespace std;
    if (file_name.size() == 0) {
        ERROR_MESSAGE("file name not set");
        return 0;
    }
    if (out == nullptr) {
        ERROR_MESSAGE("pointer to output not set.");
        return 0;
    }

    ifstream infile(file_name);

    size_t dimX, num_points;
    if (infile.is_open()) {
        if (std::type_index(typeid(float)) == data_type_info) {
            read_contents(infile, dimX, num_points, file_name, filter.get(), (float *) out);
        } else if (std::type_index(typeid(double)) == data_type_info) {
            read_contents(infile, dimX, num_points, file_name, filter.get(), (double *) out);
        } else {
            ERROR_MESSAGE("Unknown data type " + std::string(data_type_info.name()));
            return 0;
        }
        infile.close();
    } else {
        ERROR_MESSAGE("failed to open file " + file_name);
        return 0;
    }

    return 1;
}

int Dat_file_reader::get_shape(size_t& dim, size_t& num_points) {
    using namespace std;

    if (file_name.size() == 0) {
        std::cerr <<"Error: file name not set. \t"<< __FILE__ << ":" << __LINE__ << std::endl;
        return 0;
    }

    ifstream infile(file_name);
    if (infile.is_open()) {
        read_contents(infile, dim, num_points, file_name, filter.get());
        infile.close();
    } else {
        ERROR_MESSAGE("failed to open file " + file_name);
        return 0;
    }

    return 1;
}

void Dat_file_reader::set_filter(const std::shared_ptr<Line_filter> &filter) {
    Dat_file_reader::filter = filter;
}
