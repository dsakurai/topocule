//
// Created by Daisuke Sakurai on 2019-09-15.
//

#ifndef TOPO_MOLECULE_STATES_FILE_READER_H
#define TOPO_MOLECULE_STATES_FILE_READER_H

#include <string>
#include <memory>
#include <typeindex>

namespace tpcl {

    class Dat_file_reader {
    public:
        void set_file_name(const std::string &fname) {
            file_name = fname;
        }

        int doIt();

        int get_shape(size_t &dim, size_t &num_points);

        template<class value_type>
        void set_output(value_type *out) {
            this->out = out;
            set_data_type_info(typeid(*out));
        }

        const std::type_index &get_data_type_info() {
            return data_type_info;
        }

        void set_data_type_info(const std::type_index &type_id) {
            data_type_info = type_id;
        }

        // dummy filter
        class Line_filter {
        public:
            virtual void operator()(
                    __attribute__((unused))/*not in use*/ std::string& row,
                    __attribute__((unused))/*not in use*/ size_t line_count) {
                    // Empty the string `row` to skip this line in the reader
            }
        };

    private:
        std::type_index data_type_info = std::type_index(typeid(double));
        std::shared_ptr<Line_filter> filter = nullptr;
    public:
        void set_filter(const std::shared_ptr<Line_filter> &filter);

    private:
        std::string file_name;
        void *out;
    };

}


#endif //TOPO_MOLECULE_STATES_FILE_READER_H
