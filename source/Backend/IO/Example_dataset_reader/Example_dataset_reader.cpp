//
// Created by Daisuke Sakurai on 2020-09-20.
//

#include "Example_dataset_reader.h"

#include <VTK_reader.h>

#include <nlohmann/json.hpp>
#include <fstream>

std::shared_ptr<tpcl::Multi_fields> tpcl::Example_dataset_reader::read_example_dataset(const std::string &config) {
    using namespace tpcl;

    nlohmann::json data_info;
    std::ifstream{config} >> data_info;

    // set up multi-fields
    VTK_reader reader {data_info["data_file"], data_info["fields"] };
    reader.do_it();
    auto multi_fields  = reader.get_multi_fields();
    return multi_fields;
}
