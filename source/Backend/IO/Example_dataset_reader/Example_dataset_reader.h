//
// Created by Daisuke Sakurai on 2020-09-20.
//

#ifndef TOPOCULE_EXAMPLE_DATASET_READER_H
#define TOPOCULE_EXAMPLE_DATASET_READER_H

#include <iosfwd>

#include <memory>

#include <Multi_fields.h>

namespace tpcl {
    namespace Example_dataset_reader {
        std::shared_ptr<Multi_fields> read_example_dataset(const std::string &config);
    }
}

#endif //TOPOCULE_EXAMPLE_DATASET_READER_H
