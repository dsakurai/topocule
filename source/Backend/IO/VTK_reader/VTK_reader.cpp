//
// Created by Daisuke Sakurai on 2020-08-11.
//

#include <VTK_reader.h>

#include <Field_values.h>
#include <Tessellation.h>

#include <array>
#include <string>
#include <vector>

#include <vtkCellIterator.h>
#include <vtkDataSet.h>
#include <vtkDataArray.h>
#include <vtkPointData.h>
#include <vtkNew.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLGenericDataObjectReader.h>


vtkSmartPointer<vtkDataSet> tpcl::read_vtk_file(const std::string& file_name) {
    // Load from a VTK file
    vtkNew<vtkXMLGenericDataObjectReader> reader;
    reader->SetFileName(file_name.c_str());
    reader->Update();

    return reader->GetOutputAsDataSet();
}

std::vector<std::shared_ptr<tpcl::Field_values>>
tpcl::copy_field_values(vtkDataSet *vtk_data_set, const std::vector<std::string> &field_names) {

    using out_type = std::vector<std::shared_ptr<tpcl::Field_values>>;

    auto get_vtk_array = [&](const std::string &name, vtkDataSet* vtk_data_set) {
        auto *array = vtk_data_set->GetPointData()->GetArray(name.c_str());
        if (!array) throw std::runtime_error{"Array not found in the VTK data"};
        return array;
    };

    out_type fields_out = [&] () {
        out_type out;
        // Set field values
        for (int i = 0; i < std::size(field_names); ++i) {
            auto *vtk_array = get_vtk_array(field_names[i], vtk_data_set);

            auto field = std::make_shared<Field_values_of_type<double>>(
                    vtk_array->GetName()
            );

            int num_values = vtk_array->GetNumberOfValues();
            auto arr = std::make_shared<std::vector<double>>(num_values);
            for (int i = 0; i < num_values; ++i) {
                (*arr)[i] = vtk_array->GetTuple1(i);
            };

            field->set_values(arr);

            out.emplace_back(field);
        }
        return out;
    }();

    return fields_out;
}

void tpcl::add_cell_connectivity(vtkSmartPointer<vtkCellIterator> iter, tpcl::Tessellation *tessel,
                                 const int cell_type_wanted) {
    int dim = -1;
    Cell_id cell_id = 0;
    auto cell_incidence = tessel->get_cell_incidence();
    if (!cell_incidence) throw std::runtime_error{"Cell incidence is not set"};
    for (;
            !iter->IsDoneWithTraversal();
            iter->GoToNextCell()) {
        auto current_type = iter->GetCellType();

        if (current_type == cell_type_wanted) {
            dim = iter->GetCellDimension();

            const int num_points = iter->GetNumberOfPoints();
            for (int p = 0; p < num_points; ++p) {
                const auto pid = iter->GetPointIds()->GetId(p);
                cell_incidence->insert_incidence(
                        dim,
                        /*dimension of a vertex*/int{0},
                        cell_id,
                        Cell_id(pid),
                        /*both directions*/ true
                );
            }
        }
        ++cell_id;
    }
    tessel->set_number_of_cells_for_dim(dim, cell_id);
}

std::shared_ptr<tpcl::Tessellation>
tpcl::create_tessellation(vtkSmartPointer<vtkCellIterator> iter,
                          const tpcl::Cell_type& cell_type
                          ) {
    int maximal_dimensionality = cell_type.get_dimension();
    auto [cell_type_wanted, simplex] = [&cell_type] () {
        switch (cell_type.get_type()) {
            case Cell_type::triangle:
                return std::make_pair(VTK_TRIANGLE, true);
            case Cell_type::tetrahedron:
                return std::make_pair(VTK_TETRA, true);
            default: throw std::runtime_error {"Unknown cell type requested"};
        }
    }();

    auto tessellation = std::make_shared<Tessellation>();
    tessellation->set_maximal_dimension(maximal_dimensionality);
    auto cell_connectivity = std::make_shared<Cell_incidence>(maximal_dimensionality, /*triangulated*/ simplex);
    tessellation->set_cell_incidence(cell_connectivity);

    add_cell_connectivity(
            iter,
            tessellation.get(),
            cell_type_wanted);

    return tessellation;
}

std::shared_ptr<tpcl::Tessellation>
tpcl::create_tessellation(vtkDataSet *vtk_data_set) {
    auto cell_type = [&vtk_data_set]() {
        if (vtkPolyData::SafeDownCast(vtk_data_set)) {
            // 2D data
            return Cell_type {Cell_type::triangle};
        }
        if (vtkUnstructuredGrid::SafeDownCast(vtk_data_set)) {
            // 3D data
            return Cell_type {Cell_type::tetrahedron};
        }
        throw std::runtime_error {"Unexpected data type"};
    }();

    return create_tessellation(
        vtk_data_set->NewCellIterator(),
        cell_type
    );
}

void tpcl::VTK_reader::do_it() {

    auto vtk_data_set = read_vtk_file(file_name);
    auto fields       = copy_field_values(vtk_data_set, field_names);
    auto tessellation = create_tessellation(vtk_data_set);
    multi_fields = std::make_shared<Multi_fields>();
    multi_fields->set_tessellation(tessellation);
    for (auto& field: fields) {
        multi_fields->add_field_values(field);
    }
}
