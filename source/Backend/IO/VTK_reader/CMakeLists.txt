
if (NOT TPCL_WITH_VTK)
    return()
endif()

add_library(VTK_reader)

target_include_directories(VTK_reader
    PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}"
)

target_sources(VTK_reader
    PUBLIC
    VTK_reader.cpp
    VTK_reader.h
)

target_include_directories(VTK_reader
    PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}"
)

target_link_libraries(VTK_reader
    PUBLIC
    Topocule_vtk
    Fields
)

if(TPCL_ENABLE_TESTS)
    target_sources_test_topocule(
        PUBLIC
        test_VTK_reader.cpp
    )

    target_link_libraries_test_topocule(
        PUBLIC
            triangle_vtk
            two_tets_vtk
            VTK_reader
    )

endif(TPCL_ENABLE_TESTS)
