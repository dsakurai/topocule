//
// Created by Daisuke Sakurai on 2020-08-12.
//

//
// Created by Daisuke Sakurai on 2020-08-11.
//

#include <VTK_reader.h>

#include <string>

#include <vtkCell.h>

#include <test.h>
#include <Multi_fields.h>

#include <nlohmann/json.hpp>
#include "Tessellation.h"

using namespace tpcl;

using json = nlohmann::json;

std::shared_ptr<Multi_fields>
load(
        const char* info,
        const std::string &file_name,
        const std::vector<std::string> &array_names,
        const int dim,
        const std::vector<Cell_id>& num_cells_expected
) {
    INFO(info);

    auto multi_fields = [&]() {
        VTK_reader reader {file_name, array_names};
        reader.do_it();
        return reader.get_multi_fields();
    }();

    auto fields       = multi_fields->get_field_values();
    auto tessellation = multi_fields->get_tessellation();

    REQUIRE(fields.size() == array_names.size());

    for (const auto&f: fields) REQUIRE(f);

    CHECK(tessellation->get_maximal_dimension() == dim);
    {
        INFO("Check expected number of cells are set for proper cell dimensionalities");
        CHECK(tessellation->get_maximal_dimension() == num_cells_expected.size() - 1);
    }

    int d = 0;
    for(auto& _num_cells_expected_: num_cells_expected) {
        CHECK(tessellation->get_number_of_cells_for_dim(d) == _num_cells_expected_);
        ++d;
    }

    return multi_fields;
}

TEST_CASE ("Test VTK_reader") {


    auto load_multi_fields = [] (
            const char* info,
            const char *config_file_name,
            const std::vector<Cell_id>& num_cells_expected
            ) {
        json j;
        std::ifstream {config_file_name} >> j;

        std::string file_name = j["data_file"];
        std::vector<std::string> array_names = j["fields"];
        return load(
                info,
                file_name,
                array_names,
                j["max_cell_dim"],
                num_cells_expected
        );
    };

    {
        auto info = "Load triangle_vtk example";
        INFO (info);
        CHECK(
                load_multi_fields( info,
                                   TRIANGLE_VTK_CONFIG,
                                   // number of cells
                                   {Nd_cells::UNSET, // 0D
                                    Nd_cells::UNSET, // 1D
                                    1                       // 2D
                                    }
                ));
    }
    {
        auto info = "Load two_tets_vtk example";
        INFO (info);
        CHECK(load_multi_fields( info,
                                 TWO_TETS_VTK_CONFIG,
                                 // number of cells
                                 {Nd_cells::UNSET, // 0D
                                  Nd_cells::UNSET, // 1D
                                  Nd_cells::UNSET, // 2D
                                  2                       // 3D
                                 }
        ));
    }
}
