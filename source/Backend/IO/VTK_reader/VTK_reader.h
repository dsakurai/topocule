//
// Created by Daisuke Sakurai on 2020-08-11.
//

#ifndef TOPOCULE_VTK_READER_H
#define TOPOCULE_VTK_READER_H

#include <Field_values.h>

#include <Tessellation.h>

#include <iosfwd>
#include <memory>


#include <vtkDataSet.h>
#include <vtkSmartPointer.h>
#include <vtkCellType.h>
#include <Multi_fields.h>

class vtkDataSet;

namespace tpcl {

    class Cell_type {
    public:
        enum types {
            triangle,
            tetrahedron,
        };

        constexpr Cell_type (types type):
                type {type}
        {}

        constexpr static int get_dimension(types type) {
            switch(type) {
                case triangle:
                    return 2;
                case tetrahedron:
                    return 3;
                default:
                    throw std::runtime_error {"Unknown cell type"};
            }
        }

        types& get_type() {
            return type;
        }

        const types& get_type() const {
            return type;
        }

        int get_dimension() const {
            return get_dimension(type);
        }

    private:
        types type;
    };

    [[nodiscard]]
    vtkSmartPointer<vtkDataSet> read_vtk_file (const std::string& file_name);

    [[nodiscard]]
    std::vector<std::shared_ptr<tpcl::Field_values>>
    copy_field_values(vtkDataSet *vtk_data_set, const std::vector<std::string> &field_names);;

    void add_cell_connectivity(vtkSmartPointer<vtkCellIterator> iter, tpcl::Tessellation *tessel,
                               const int cell_type_wanted);

    [[nodiscard]]
    std::shared_ptr<tpcl::Tessellation>
    create_tessellation(vtkSmartPointer<vtkCellIterator> iter,
                        const tpcl::Cell_type& cell_type
                        );

    /**
     *  The wanted cells are the largest dimensional cells.
     *  The dimension will be deduced automatically from the wanted cell.
     *
     * @param vtk_data_set
     * @param cell_type_wanted
     * @return
     */
    [[nodiscard]]
    std::shared_ptr<tpcl::Tessellation>
    create_tessellation(vtkDataSet *vtk_data_set);


    class VTK_reader {
    public:
        VTK_reader (
                const std::string& file_name,
                const std::vector<std::string> &field_names
        ) :
                file_name {file_name},
                field_names {field_names}
        { }

        void do_it();

        [[nodiscard]]
        std::shared_ptr<tpcl::Multi_fields> get_multi_fields() {
            return multi_fields;
        }

    private:
        std::string file_name;
        std::vector<std::string> field_names;
        std::shared_ptr<tpcl::Multi_fields> multi_fields;
    };
}

#endif //TOPOCULE_VTK_READER_H
