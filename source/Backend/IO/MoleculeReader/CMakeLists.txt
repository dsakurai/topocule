
add_library(MoleculeReader
    MoleculeReader.h
    MoleculeReader.cpp
    )

target_include_directories(MoleculeReader
    PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}"
)

target_link_libraries(MoleculeReader
    PUBLIC
        Dat_file_reader
)

if(TPCL_ENABLE_TESTS)
    target_sources_test_topocule(
        PUBLIC
            test_Molecule_reader.cpp
    )

    target_link_libraries_test_topocule(
        PUBLIC
            MoleculeReader
            data
            )
endif(TPCL_ENABLE_TESTS)
