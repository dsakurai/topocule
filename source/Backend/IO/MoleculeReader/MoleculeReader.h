//
// Created by Daisuke Sakurai on 2019-09-14.
//

#ifndef TOPO_MOLECULE_STATES_MOLECULEREADER_H
#define TOPO_MOLECULE_STATES_MOLECULEREADER_H

#include <array>
#include <string>
#include <vector>
#include <Dat_file_reader.h>

namespace tpcl {

    class MoleculeReader {
    public:
        enum file_enum {
            coords,
            field,
            num_files
        };

        MoleculeReader() {
            this->files.resize(num_files);
            files[coords] = "coordinates file";
            files[field] = "field file";
        }

        void check_reader(const MoleculeReader::file_enum f) {
            if (!reader[f]) throw std::runtime_error {"Reader not set"};
        }

        void set_file_name(const MoleculeReader::file_enum f, const std::string &fname) {
            check_reader(f);
            reader[f]->set_file_name(fname);
        }

        int get_array_shape(const MoleculeReader::file_enum f, size_t &dim, size_t &num_points) {
            check_reader(f);
            return reader[f]->get_shape(dim, num_points);
        }

        void set_reader(const MoleculeReader::file_enum f, const std::shared_ptr<Dat_file_reader>& reader) {
            this->reader[f] = reader;
        }

        template<class value_type>
        void set_pointer(const MoleculeReader::file_enum f, value_type *data) {
            check_reader(f);
            reader[f]->set_output((value_type *) data);
        }

        int doIt();

    private:
        std::vector<std::string> files;
        std::array<std::shared_ptr<Dat_file_reader>, 2> reader;
    };

}


#endif //TOPO_MOLECULE_STATES_MOLECULEREADER_H
