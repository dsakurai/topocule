//
// Created by Daisuke Sakurai on 2019/11/09.
//

#include <test.h>
#include <MoleculeReader.h>

using namespace tpcl;

TEST_CASE ("Test Molecule_reader") {
// Closed because the test depends on research data

    using namespace std;

    //
    // Test the File reader
    //

    string coords_file = PENTANE_DATA_DIR "-reduced-dim/R_3D.txt";
    string field_file = PENTANE_DATA_DIR "-reduced-dim/chi.txt";


    //
    // Test the molecule reader
    //
    {
        MoleculeReader reader;
        size_t coords_dim, coords_num_points;
        size_t field_dim, field_num_points;

        auto reader_coords = std::make_shared<Dat_file_reader>();
        auto reader_field  = std::make_shared<Dat_file_reader>();

        reader.set_reader(MoleculeReader::coords, reader_coords);
        reader.set_reader(MoleculeReader::field, reader_field);

        reader.set_file_name(MoleculeReader::coords, coords_file);
        reader.set_file_name(MoleculeReader::field,  field_file);

        reader.get_array_shape(MoleculeReader::coords, coords_dim, coords_num_points);
        reader.get_array_shape(MoleculeReader::field, field_dim, field_num_points);

        std::vector<float> coords (coords_dim*coords_num_points);
        std::vector<float> field (field_dim*field_num_points);

        reader.set_pointer(MoleculeReader::coords, coords.data());
        reader.set_pointer(MoleculeReader::field, field.data());
    }
}

