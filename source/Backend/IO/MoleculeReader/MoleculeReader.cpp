//
// Created by Daisuke Sakurai on 2019-09-14.
//

#include "MoleculeReader.h"
#include <iostream>

using namespace tpcl;

int MoleculeReader::doIt() {
    using namespace std;
    for (int f = 0; f < num_files; ++f) {
        if (!reader[f]->doIt()) {
            std::cerr << "Error: reading "<< files[f] <<" failed.\t" << __FILE__ << ":" << __LINE__ << std::endl;
            return 0;
        }
    }
    return 1;
}
