//
// Created by Daisuke Sakurai on 2020-12-19.
//

#include <Multi_fields.h>
#include "Compute_reeb_space.h"
#include <set>

namespace tpcl::reeb_space {

    Compute_reeb_space::Compute_reeb_space(
            int dimension_range,
            const std::shared_ptr<Multi_fields>& multi_fields,
            const std::shared_ptr<Jacobi_edges>& jacobi_edges):
            dimension_range {dimension_range},
            multi_fields {multi_fields},
            jacobi_edges {jacobi_edges}
    {
        multi_fields->verify_simplicial_complex_quickly();
    }

    void Compute_reeb_space::do_it() {
        Propagate {dimension_range, *multi_fields, *jacobi_edges}
                .do_it();
        // for arrangement in arrangements
        //    glue_Reeb_edges(arrangement)
        //    glue_Reeb_faces(arrangement)
        //        # - this Jaocbi edge in this tet is identical to that one in the representative tet
        //        # - this 2-face in this tet is identical to that one in the representative tet
        //    reeb_space = compactify(arrangement)
        //        # keep the representative edges and faces in the tessellation structure
    }

    Propagate::Propagate(int dimension_range, const Multi_fields &multi_fields, const Jacobi_edges &jacobi_edges):
            dimension_range{dimension_range},
            multi_fields{multi_fields},
            jacobi_edges{jacobi_edges},
            tessellation{*multi_fields.get_tessellation()},
            dim_tets{tessellation.get_maximal_dimension()},
            edge_to_tets{tessellation.get_cell_incidence()->get_incidence(dimension_range, dim_tets)},
            tet_adjacency{*tessellation.get_cell_adjacency(dim_tets)},
            tets_on_edges{jacobi_edges.size()}
    { }

    void Propagate::do_it() {
        // inputs

        tets_on_edges.resize(jacobi_edges.size());

        { // Parallelizable block
            // intermediate data structures
            for (auto& jacobi_edge: jacobi_edges) {
                const auto edge_id = jacobi_edge.edge_id;
                const auto& tets = edge_to_tets[edge_id];
                std::vector<Cell_id>& tets_on_edge = tets_on_edges[edge_id];

                for (Cell_id tet_id: tets) {
                    // propagate to adjacent tets
                    propagate_edge(edge_id, tet_id, tets_on_edge);
                }
            }
        }
    }

    void Propagate::propagate_edge(Cell_id edge_id, Cell_id tet_id, std::vector<Cell_id> &tets_on_edge) {

        // If the
        if (!is_cut(edge_id, tet_id) ) return;

        // Register this tet if its silhouette is cut by the edge
        tets_on_edge.emplace_back(tet_id);

        // for adjacent tets
        const Cell_id num_adjacent = tet_adjacency.get_number_of_adjacent_cells(tet_id);
        for (Cell_id a = 0; a < num_adjacent; ++a) {
            const auto tet_id_adjacent = tet_adjacency.get_adjacent_cell(tet_id, a);

            /// @performance This recursive call cannot be tail-call optimized.
            /// There's thus a danger of a stack overflow...
            propagate_edge(edge_id, tet_id_adjacent, tets_on_edge);
        }
    }
}
