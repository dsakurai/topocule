//
// Created by Daisuke Sakurai on 2020-12-19.
//

#ifndef TOPOCULE_COMPUTE_REEB_SPACE_H
#define TOPOCULE_COMPUTE_REEB_SPACE_H

#include <Multi_vector.h>
#include <Jacobi_set_presumptions.h>

namespace tpcl {

    namespace reeb_space {

        using Jacobi_edge = jacobi_set::Jacobi_edge;
        using Jacobi_edges = jacobi_set::Jacobi_edges;

        class Compute_reeb_space {
        public:
            Compute_reeb_space(
                    int dimension_range,
                    const std::shared_ptr<Multi_fields>& multi_fields,
                    const std::shared_ptr<jacobi_set::Jacobi_edges>& jacobi_edges);
            void do_it();

        private:
            // Inputs
            int dimension_range;
            std::shared_ptr<Multi_fields> multi_fields;
            std::shared_ptr<Jacobi_edges> jacobi_edges;
        };

        /**
         * Helper class for Reeb space computation.
         */
        class Propagate {
        public:
            Propagate (int dimension_range, const Multi_fields& multi_fields, const Jacobi_edges& jacobi_edges);

            void propagate_edge(Cell_id edge_id, Cell_id tet_id, std::vector<Cell_id> &tets_on_edge);

            void do_it();

        private:
            const int dimension_range;
            const int dim_tets;
            const Multi_fields& multi_fields;
            const Jacobi_edges& jacobi_edges;
            const Tessellation& tessellation;
            const Multi_vector<2, Cell_id>& edge_to_tets;
            const Cell_adjacency_in_manifold& tet_adjacency;

            // out
            Multi_vector<2, Cell_id> tets_on_edges;
        };
    }
}


#endif //TOPOCULE_COMPUTE_REEB_SPACE_H
