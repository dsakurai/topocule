
add_library(Contour_tree
        GetGraph.cpp
        GetGraph.h
        SimplifyContourTree.h
        LayoutGraph.cpp
        LayoutGraph.h
        )

target_include_directories(Contour_tree
    PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}"
        )

target_link_libraries(Contour_tree
    PUBLIC
        ttk::base::ftmTree
        Tessellation
        BGLGraph
        MoleculeReader # @todo move this out of this lib
        checker
    )

# Documentation
if (TPCL_ENABLE_DOCUMENTATION)
    add_readme("README.md")
endif (TPCL_ENABLE_DOCUMENTATION)

if(TPCL_ENABLE_TESTS)
    target_sources_test_topocule(
        PUBLIC
            test_GetGraph.cpp
            test_SimplifyContourTree.cpp
    )

    target_link_libraries_test_topocule(
            PUBLIC
            Contour_tree
            )
endif(TPCL_ENABLE_TESTS)

