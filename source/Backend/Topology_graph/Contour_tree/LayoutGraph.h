//
// Created by Daisuke Sakurai on 2020/01/26.
//

#ifndef TOPOCULE_LAYOUT_GRAPH_H
#define TOPOCULE_LAYOUT_GRAPH_H

#include <BGLGraph.h>
#include <SimplifyContourTree.h>

class exception_property_size_mismatch : public std::runtime_error {
public:
    exception_property_size_mismatch () : std::runtime_error {"Property has wrong size for the graph."}
    {}
};

void set_coords(
        tpcl::BGLGraph_list_based& g,
        const std::vector<double> &ys_vect,
        std::vector<std::array<double, 3>> &coords);

template<class Map, class Vector, class Graph>
void get_random_access_version(Map& map, Vector& vec, Graph& g) {

    for (auto& [/*vertex descriptor*/v, value]: map) {
        auto id = g[v].pedigree_id;
        // make sure the ID fits in
        if (vec.size() < id+1) vec.resize(id+1);

        vec[id] = value;
    }
}


template <class mapped_type>
void layout_graph(
        tpcl::BGLGraph_list_based& g,
        std::map<tpcl::BGLGraph_list_based::vertex_descriptor, mapped_type>& ys,
        std::vector<std::array<double,3>>& coords
) {

    std::vector<double> ys_vect;
    get_random_access_version(ys, ys_vect, g);

    set_coords(g, ys_vect, coords);
}


#endif //TOPOCULE_LAYOUT_GRAPH_H
