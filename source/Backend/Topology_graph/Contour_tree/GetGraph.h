//
// Created by Daisuke Sakurai on 2019/11/18.
//

#ifndef TOPOCULE_GETGRAPH_H
#define TOPOCULE_GETGRAPH_H

#include <BGLGraph.h>
#include <DataTypes.h>

#include <FTMTree.h>
#include <FTMTree_MT.h>
#include <boost/graph/connected_components.hpp>

namespace ttk {
    namespace ftm {
        class FTMTree_MT;
    }
}

namespace tpcl {

    template <class Graph>
    void getGraph(ttk::ftm::FTMTree_MT *tree, Graph* out, int augmented = 1) {
        using namespace ttk;
        using namespace ftm;
        const SimplexId numberOfSuperArcs = tree->getNumberOfSuperArcs();
        if (out == nullptr) throw std::runtime_error("error: output graph is null");

        const SimplexId num_vertices = tree->getNumberOfVertices();
        std::vector<typename Graph::vertex_descriptor> pedigree_id_2_vertex_descriptor(num_vertices);

        // caution: tree->getNumberOfNodes() does not give the maximum number of nodes!
        //
        {
            if(boost::num_vertices(*out)){
                throw std::runtime_error("error: output graph is given a vertex.");
            }
        }

        *out = Graph (num_vertices);
        int cnt = 0;
        for (auto [vi, ve] = boost::vertices(*out);
             vi != ve;
             ++vi, ++cnt) {
            (*out)[*vi].pedigree_id = cnt;
            pedigree_id_2_vertex_descriptor[cnt] = *vi;
        }

        if (numberOfSuperArcs == 0) return;

        auto &g = *out;

        // for each super arc
        // The implementation is based on TTK's addCompleteSkeletonArc of ttkFTMTree
        for (SimplexId arcId = 0; arcId < numberOfSuperArcs; ++arcId) {
            SuperArc *arc = tree->getSuperArc(arcId);

            // Get or create the lowest (?) point of the arc
            SimplexId previous_vertex_pedigreeId;
            { // get the ID
                SimplexId downNodeId = tree->getLowerNodeId(arc);
                SimplexId downVertex_pedigreeId
                        = tree->getNode(downNodeId)->getVertexId();
                previous_vertex_pedigreeId = downVertex_pedigreeId;
            }


            // for the points inside the arc
            if (augmented) {
                for (const SimplexId current_vertex_pedigreeId : *arc) {

                    add_edge(pedigree_id_2_vertex_descriptor[previous_vertex_pedigreeId],
                             pedigree_id_2_vertex_descriptor[current_vertex_pedigreeId], g);

                    previous_vertex_pedigreeId = current_vertex_pedigreeId;
                }
            }

            { // same for the upper node
                const SimplexId upNodeId = tree->getUpperNodeId(arc);
                const SimplexId upper_vertex_pedigreeId = tree->getNode(upNodeId)->getVertexId();
                add_edge(pedigree_id_2_vertex_descriptor[previous_vertex_pedigreeId],
                         pedigree_id_2_vertex_descriptor[upper_vertex_pedigreeId], g);
            }
        }
    }
    template <class Graph>
    void getGraph(ttk::ftm::FTMTree_MT *tree, std::shared_ptr<Graph>& out, int augmented = 1){
        if (!out) out = std::make_shared<Graph>();
        getGraph(tree, out.get(), augmented);
    }
}

#endif //TOPOCULE_GETGRAPH_H
