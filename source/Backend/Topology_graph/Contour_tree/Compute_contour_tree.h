//
// Created by Daisuke Sakurai on 2020/01/09.
//

#ifndef TOPOCULE_COMPUTECONTOURTREE_H
#define TOPOCULE_COMPUTECONTOURTREE_H

#include <FTMTree.h>
#include <GetGraph.h>
#include <Tessellation.h>
#include <Ttk_nd_triangulation.h>

namespace tpcl {

    template<class value_type>
    auto compute_contour_tree(
            const tpcl::Ttk_nd_triangulation& triangulation,
            const value_type* vertex_scalars,
            Cell_id num_vertices,
            ttk::ftm::FTMTree& tree,
            int num_threads
    ){

        tree.setVertexScalars(vertex_scalars);

        // Sort the vertices
        std::vector<Cell_id> offsets (num_vertices);
        ttk::preconditionOrderArray(num_vertices, vertex_scalars, offsets.data());
        tree.setVertexSoSoffsets(offsets.data());

        tree.setTreeType(ttk::ftm::TreeType::Contour);
//    tree.setSegmentation(GetWithSegmentation());
//    tree.setNormalizeIds(GetWithNormalize());
        // build the tree (specify the contour tree)
        tree.setThreadNumber(num_threads);
        tree.build<value_type, Ttk_nd_triangulation>(&triangulation);

    }

    template<class Graph, class value_type>
    inline auto compute_contour_tree(
            const std::shared_ptr<tpcl::Tessellation>& tessellation,
            std::shared_ptr<Graph> &out,
            std::vector<value_type> &chi,
            int num_threads
    ) {
        tessellation->verify_simplicial_complex_quickly();

        tpcl::Ttk_nd_triangulation triangulation {tessellation};

        ttk::ftm::FTMTree tree;

        compute_contour_tree(triangulation, chi.data(), chi.size(), tree, num_threads);

        // get the contour tree
        getGraph(tree.getTree(ttk::ftm::TreeType::Contour), out, /* augmented tree = */ 1);

        return out;
    }

}

#endif //TOPOCULE_COMPUTECONTOURTREE_H
