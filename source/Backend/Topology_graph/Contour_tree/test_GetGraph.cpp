//
// Created by Daisuke Sakurai on 2019/11/18.
//

#include <iostream>
#include <test.h>

#include <ComputeDelaunay.h>
#include <Compute_contour_tree.h>
#include <FTMTree.h>
#include <GetGraph.h>
#include <Ttk_nd_triangulation.h>
#include <TriangulationFromDelaunay.h>

#include <prettyprint.hpp>

#include <test_compare_graphs.h>

using namespace tpcl;

/** Contour tree computation together with expected graph structure
 *
 * @param tree
 * @param expect
 * @param num_threads
 */
void construct_graph(ttk::ftm::FTMTree& tree, BGLGraph& expect, int num_threads){
    auto points = std::make_shared<std::vector<std::vector<double>>>();
    std::vector<double> chi;

    *points = {
                        // scalar:
            {0.0, 0.0}, // 2
            {1.0, 0.0}, // 1
            {0.0, 1.0}, // 0
            {1.0, 1.0}  // 3
    };

    chi = {
            2,
            1,
            0,
            3
    };

    // 3     | 3
    // | 0   | 2
    // |/    |
    // 1     | 1
    // |     |
    // 2     | 0

    expect = BGLGraph(4);

    for(int i = 0; i < boost::num_vertices(expect); ++i) {
        expect[boost::vertex(i,expect)].pedigree_id = i;
    }

    boost::add_edge(
            boost::vertex(1, expect),
            boost::vertex(3, expect),
            expect
    );
    boost::add_edge(
            boost::vertex(1, expect),
            boost::vertex(0, expect),
            expect
    );
    boost::add_edge(
            boost::vertex(2, expect),
            boost::vertex(1, expect),
            expect
    );

    ComputeDelaunay<decltype(points)::element_type> adjacency;
    adjacency.setPoints(points);
    adjacency.doIt();

    Ttk_nd_triangulation triangulation;

    triangulationFromDelaunay(adjacency, triangulation);

    tpcl::compute_contour_tree(triangulation, chi.data(), chi.size(), tree, /*num_threads*/1);
}


void check(ttk::ftm::FTMTree &tree, BGLGraph &expect, BGLGraph &out,
           std::string filename,
           int line
           ) {// this should fail
    INFO(std::string{"Called from "} + filename + ":" + std::to_string(line));

    REQUIRE(boost::num_vertices(out) == 4);

    CHECK(same_string(out, expect));
}

TEST_CASE ("Test contour tree output in serial") {
    ttk::ftm::FTMTree tree;

    BGLGraph expect;
    int num_threads;
    num_threads = 1;

    construct_graph(tree, expect, num_threads);

    WHEN("Pointer to output is null") {
        CHECK_THROWS(getGraph(tree.getTree(ttk::ftm::TreeType::Contour), (BGLGraph *) nullptr));
    }
    WHEN("A simple example is set") {
        // get the contour tree
        BGLGraph out;
        getGraph(tree.getTree(ttk::ftm::TreeType::Contour), &out);

        check(tree, expect, out, __FILE__, __LINE__);
    }
}

TEST_CASE ("Test contour tree output in parallel") {
    ttk::ftm::FTMTree tree;

    BGLGraph expect;
    int num_threads;
    num_threads = 2;

    construct_graph(tree, expect, num_threads);

    // get the contour tree
    BGLGraph out;
    getGraph(tree.getTree(ttk::ftm::TreeType::Contour), &out);

    check(tree, expect, out, __FILE__, __LINE__);
}
