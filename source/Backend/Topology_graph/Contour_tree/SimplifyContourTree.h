//
// Created by Daisuke Sakurai on 2019/12/05.
//

#ifndef TOPOCULE_SIMPLIFYCONTOURTREE_H
#define TOPOCULE_SIMPLIFYCONTOURTREE_H

#include <BGLGraph.h>
#include <iostream>
#include <map>
#include <algorithm>
#include <vector>
#include <memory>

namespace tpcl {

    template<class Scalar_type_>
    class Contour_tree_traits {
    public:
        //
        using Scalar_type = Scalar_type_;
        using Graph = BGLGraph_list_based;
        using Value_map = std::map<
                boost::graph_traits<Graph>::vertex_descriptor,
                Scalar_type_>;
    };

    // Check if degree one
    // If so, return the vertex adjacent to the input
    template <class Graph>
    std::optional<typename Graph::vertex_descriptor>
    degree_one(
            const typename Graph::vertex_descriptor v,
            const Graph &g
    ) {
        using out = decltype(degree_one(v, g));

        auto[ai, ae] = boost::adjacent_vertices(v, g);
        if (ai == ae) {
            // no adjacent vertex
            return out{};
        }

        if (auto adjacent = *ai;++ai == ae) {
            // one adjacent vertex
            return out{adjacent};
        }

        // multiple adjacent vertexes
        return out{};
    }

    template <class edge_property_type>
    edge_property_type merge_properties(
            edge_property_type& e0,
            edge_property_type& e1
    ){
        return e0;
    }


    /**
     * Check if it has exactly two out edges.
     * If so, return the out edges adjacent to the input
     *
     * @tparam Graph
     * @param v
     * @param g
     * @return
     */
    template <class Graph>
    std::optional<std::array<typename Graph::edge_descriptor,2>>
    out_degree_two(
            const typename Graph::vertex_descriptor v,
            const Graph &g
    ) {
        using Out = decltype(out_degree_two(v, g));

        using edge_descriptor = typename Graph::edge_descriptor;

        Out out = typename Out::value_type {};

        const int degree = out->size();

        // debug
        int cnt = 0;
        for (
                auto[oi, oe] = boost::out_edges(v, g);
                oi != oe;
                ++cnt, ++oi) {
            if (cnt < out->size()) (*out)[cnt] = *oi;
            else return std::nullopt;
        }
        return (cnt == degree)? out : std::nullopt;
    }

    /**
     * This implementation does not work for BGL graphs that use integer index of some array as
     * vertex descriptor. This is because we erase a vertex from the graph, which generally
     * invalidates the integer index.
     */
    template<class SimplificationTraits>
    class Graph_util {
    public:

        using Simplification_traits = SimplificationTraits;
        using value_map_type = typename Simplification_traits::Value_map;
        using Graph = typename SimplificationTraits::Graph;
        using vertex_descriptor = typename boost::graph_traits<Graph>::vertex_descriptor;
        using edge_descriptor = typename boost::graph_traits<Graph>::edge_descriptor;

        Graph_util () {}

        Graph_util (
                Graph &g, value_map_type &value_map,
                const std::shared_ptr<std::vector<vertex_descriptor>>& id_2_vertex = nullptr ){
            set_graph(g, value_map, id_2_vertex);
        }

        // auxiliary classes
        class Leaf_superarc;

        /**
         * Simulaiton of simplicity
         * @tparam val_type
         * @tparam Graph
         */
        class SoS {
        public:
            using val_type = typename value_map_type::mapped_type;

            SoS(unsigned int id, val_type value) :
                    id_(id),
                    value_(value) {}

            bool operator<(const SoS &s) const {
                if (this->value() == s.value()) return this->id() < s.id();

                return this->value() < s.value();
            }

            val_type value() const {
                return value_;
            }

            unsigned int id() const {
                return id_;
            }

        private:
            // ID is listed first because it is used to get value
            unsigned int id_;
            val_type value_;
        };


        void set_graph(Graph &g, value_map_type &value_map,
                       const std::shared_ptr<std::vector<vertex_descriptor>>& id_2_vertex = nullptr) {
            this->g = &g;
            this->id_2_vertex = id_2_vertex;
            this->value_map = &value_map;

            if (!id_2_vertex) {
                set_id_2_vertex_map();
            }

            // keep the locations of the vertex descriptors
            vertex_property_locations.resize(value_map.size(), value_map.end());
            for (auto iter = value_map.begin(); iter != value_map.end(); ++iter) {
                auto v = iter->first;
                auto id = g[v].pedigree_id;
                if (vertex_property_locations.size() < id + 1) {vertex_property_locations.resize(id + 1, value_map.end());}
                vertex_property_locations[id] = iter;
            }
        }

        Graph *get_graph() const {
            return g;
        }

        auto vertex_property_location(size_t v) {
            return vertex_property_locations[v];
        }

        void clear_and_remove_vertex(vertex_descriptor v) {
            auto id = (*g)[v].pedigree_id;
            auto location = vertex_property_locations[id];

            boost::clear_vertex(v, *g);
            value_map->erase(location);
            boost::remove_vertex(v, *g);

            // invalidate the record
            vertex_property_locations[id] = this->value_map->end();
        }

        SoS value(vertex_descriptor v) const {
            return SoS{(*g)[v].pedigree_id, value_map->at(v)};
        }

        void set_id_2_vertex_map() {
            using id_2_vertex_type = typename decltype(id_2_vertex)::element_type;

            // can we map the id to vertex in O(1)?
            id_2_vertex = std::make_shared<id_2_vertex_type>();

            // no => construct it
            auto n_vertices = boost::num_vertices(*g);
            id_2_vertex->resize(n_vertices);

            for (auto[vi, ve] = boost::vertices(*g);
                 vi != ve;
                 ++vi
                    ) {
                unsigned int id = (*g)[*vi].pedigree_id;
                // allocate memory
                if (id_2_vertex->size() < id + 1) id_2_vertex->resize(id + 1);
                // record the vertex descriptor in the map
                id_2_vertex->at(id) = *vi;
            }
        }

        class not_a_leaf : std::runtime_error {
        public:
            not_a_leaf() : std::runtime_error{"vertex is not a leaf when it was supposed to be"} {}
        };

        std::optional<vertex_descriptor>
        degree_one(const vertex_descriptor v) const {
            return tpcl::degree_one(v, *g);
        }

        /**
         * The output of the function `follow_path`.
         * @sa follow_path
         */
        struct followed_path {
            /// The vertex that was visited right before the last one.
            std::optional<vertex_descriptor> one_before;
            /// The last vertex visited
            vertex_descriptor end_of_visit;

            followed_path(const std::pair<std::optional<vertex_descriptor>, vertex_descriptor> &pair) :
                    one_before{pair.first},
                    end_of_visit{pair.second} {
            }

            operator std::tuple<std::optional<vertex_descriptor> &, vertex_descriptor &>() {
                return std::tuple<std::optional<vertex_descriptor> &, vertex_descriptor &>(
                        one_before, end_of_visit);
            }

            followed_path() {}
        };

        class do_nothing {
        public:
            void operator()(vertex_descriptor v, Graph &g) {
            }
        };

        /**
         * Move along a path in a graph.
         *
         * @param go_up follow the path upwards or downwards
         * @param v_start the starting point of our movement
         * @return a followed_path object
         *
         * @sa followed_path
         */
        template<class action_inside_path>
        followed_path follow_path(
                const bool go_up,
                const vertex_descriptor v_start,
                action_inside_path &action
        ) const {
            std::optional<vertex_descriptor> v_prev{};

            auto v_start_id = (*g)[v_start].pedigree_id;
            auto v_start_value = value(v_start);

            // lambda for checking the direction
            auto right_direction = [&go_up, &v_start_id, &v_start_value, this](
                    vertex_descriptor adjacent) {
                bool is_going_up = v_start_value < this->value(adjacent);
                return (go_up == is_going_up);
            };

            vertex_descriptor v_next;

            // flag: found a node that follows the requested direction
            bool already_found = false;
            for (
                    auto[ai, ae] = boost::adjacent_vertices(v_start, *g);
                    ai != ae;
                    ++ai
                    ) {
                if (right_direction(*ai)) {
                    // fail?
                    if (already_found) {
                        // this path ends here
                        v_prev = std::nullopt;
                        return std::pair(
                                /*there's no previous vertex we walked on*/v_prev,
                                /*v_start is the end of the path*/v_start);
                    }
                    v_prev.emplace(v_start);
                    v_next = *ai;
                    already_found = true;
                }
            }

            if (!already_found) {
                // adjacent vertex on the desired direction not found: we can't follow the path further
                return std::pair(
                        /*there's no previous vertex we walked on*/v_prev,
                        /*v_start is the end of the path*/v_start);
            }

            // pass edge
            if constexpr (std::is_same_v<action_inside_path, accumulate_edges>) {
                action(boost::edge(v_start, v_next, *g).first, *g);
            }

            bool first_time = true;
            while (auto v_regular = regular_vertex(v_next)) {
                auto lower = v_regular->lower(*g);
                auto upper = v_regular->upper(*g);
                v_prev.emplace(v_next);

                if constexpr (std::is_same_v<action_inside_path, accumulate_edges>) {
                    // pass edge
                    if (go_up) action(v_regular->upper_out_edge,*g);
                    else       action(v_regular->lower_out_edge,*g);
                } else {
                    // pass vertex
                    action(v_next, *g);
                }

                v_next = (go_up) ? upper : lower;
            }

            return std::pair{/*one last to the end*/v_prev, /*the end*/ v_next};
        }

        followed_path follow_path(
                const bool go_up,
                const vertex_descriptor v_start
        ) const {
            do_nothing dummy;
            return follow_path<do_nothing>(go_up, v_start, dummy);
        }

        class clear_and_remove_vertex {
        public:
            using Graph_util_ = Graph_util<SimplificationTraits>;
            clear_and_remove_vertex(Graph_util_& graph_util):
                graph_util {graph_util}
                { }
            void operator()(vertex_descriptor v, Graph &g) {
                graph_util->clear_and_remove_vertex(v);
            }
            Graph_util_& graph_util;
        };

        class accumulate_vertexes {
        public:
            void operator()(vertex_descriptor v, Graph &g) {
                accumulated.emplace_back(v);
            }

            std::vector<vertex_descriptor> &get_accumulated() { return accumulated; }

        private:
            std::vector<vertex_descriptor> accumulated;
        };

        class accumulate_edges {
        public:
            void operator()(edge_descriptor e, Graph &g) {
                accumulated.emplace_back(e);
            }

            std::vector<edge_descriptor> &get_accumulated() { return accumulated; }

        private:
            std::vector<edge_descriptor> accumulated;
        };

        class not_degree_one : std::runtime_error {
        public:
            not_degree_one() : std::runtime_error{"vertex is not degree one"} {}
        };

        // if the leaf is not degree one, an exception will be thrown
        vertex_descriptor neighbor_of_leaf(
                vertex_descriptor leaf_vertex
        ) {
            auto deg_one = degree_one(leaf_vertex);
            try {
                return deg_one.value();
            } catch (const std::bad_optional_access &e) {
                throw not_degree_one{};
            }
        }

        // if the leaf is not degree one, an exception will be thrown
        bool goes_up_from_leaf(
                vertex_descriptor leaf_vertex
        ) {

            // if the leaf is not degree one, an exception will be thrown
            vertex_descriptor neighbor = neighbor_of_leaf(leaf_vertex);

            return this->value(leaf_vertex) < this->value(neighbor);
        }

        // if the leaf is not degree one, an exception will be thrown
        vertex_descriptor find_the_other_end(
                vertex_descriptor leaf_vertex,
                bool *return_goes_up = nullptr
        ) {
            vertex_descriptor neighbor = neighbor_of_leaf(leaf_vertex);
            bool go_up = goes_up_from_leaf(leaf_vertex);

            if (return_goes_up) *return_goes_up = go_up;

            return follow_path(go_up, leaf_vertex).end_of_visit;
        }

        /**
         * A superarc in a contour tree, aka a path in a graph.
         */
        class Superarc {
        public:
            Superarc () = default;
            Superarc (vertex_descriptor end0, vertex_descriptor end1) :
                ends {to_array(end0, end1)}
            { }
            virtual ~Superarc(){}

            static std::array<vertex_descriptor,2> to_array(
                    vertex_descriptor end0, vertex_descriptor end1
                    ) {
                return {end0, end1};
            }

            vertex_descriptor& end(size_t i) {
                return ends[i];
            }
            vertex_descriptor end(size_t i) const {
                return ends[i];
            }

        private:
            /// The leaf vertex
            std::array<vertex_descriptor,2> ends;
        };

        /**
         * This is an auxiliary class for aiding the simplification.
         *
         * The class stores the *leaf superarc*, which is a superarc (of a contour tree)
         * that contains a leaf.
         *
         * Presumption: the_other_end is always set.
         * @todo throw exception if not.
         */
        class Leaf_superarc: public Superarc {
        public:
            using Graph_util_ = Graph_util<SimplificationTraits>;

            Leaf_superarc() {}

            Leaf_superarc(vertex_descriptor leaf_vertex, Graph_util_ *algorithm) :
                    Superarc{
                            throw_if_not_leaf(leaf_vertex, *algorithm->get_graph()),
                            algorithm->find_the_other_end(leaf_vertex)
                    }
            { }

            Leaf_superarc(vertex_descriptor leaf_vertex, vertex_descriptor the_other_end, const Graph &g) :
                    Superarc{
                            throw_if_not_leaf(leaf_vertex, g),
                            the_other_end
                    }
            {}

            static vertex_descriptor
            throw_if_not_leaf(vertex_descriptor leaf_vertex, const Graph &g) {
                if (!tpcl::degree_one(leaf_vertex, g)) { throw not_a_leaf{}; }
                return leaf_vertex;
            }

            vertex_descriptor get_leaf_vertex() const {
                return this->end(0);
            }

            /**
             *
             * Presumption: the_other_end is always set.
             * @todo throw exception if not.
             */
            vertex_descriptor get_the_other_end() const {
                return this->end(1);
            }

            /**
             * Set the critical point that connects to this leaf.
             *
             *
             * @param the_other_end: the critical points
             * @param link: the vertex in this arc that connects to the other end.
             * Useful for various analysis. Currently unused.
             */
            void set_the_other_end(vertex_descriptor the_other_end, vertex_descriptor link) {
                this->end(1) = the_other_end;
            }

            // thogh better than a hand-crafted comparison, not implemented in clang yet
//            bool operator==(Leaf_superarc const&) const = default;

            // currently this is not virtual on purpose
            bool operator==(Leaf_superarc const &that) const {
                return (this->get_leaf_vertex() == that.get_leaf_vertex()) && (this->get_the_other_end() == that.get_the_other_end());
            }
        };

        /**
         * first: lower vertex
         * second: upper vertex
         */
        struct neighbours_for_regular {
            neighbours_for_regular (
                    edge_descriptor& lower_out_edge,
                    edge_descriptor& upper_out_edge):
                    lower_out_edge {lower_out_edge},
                    upper_out_edge {upper_out_edge}
            {}
            vertex_descriptor lower (Graph& g) {
                return boost::target(lower_out_edge, g);
            }
            vertex_descriptor upper (Graph& g) {
                return boost::target(upper_out_edge, g);
            }

            edge_descriptor lower_out_edge, upper_out_edge;
        };

        std::optional<neighbours_for_regular>
                regular_vertex(vertex_descriptor vertex) const {
            std::optional<neighbours_for_regular> out;
            std::optional<edge_descriptor> lower_exists;
            std::optional<edge_descriptor> upper_exists;

            auto value = this->value(vertex);

            for (auto[ei, ee] = boost::out_edges(vertex, *g);
                 ei != ee;
                 ++ei
                    ) {
                if (this->value(boost::target(*ei, *g)) < value) {
                    if (lower_exists) return out;
                    lower_exists.emplace(*ei);
                } else {
                    if (upper_exists) return out;
                    upper_exists.emplace(*ei);
                }
            }

            if (upper_exists && lower_exists) {
                out.emplace(*lower_exists, *upper_exists);
            }

            return out;
        }

        /**
         * Gets the vertex_descriptor of a vertex from its *pedigree ID*.
         * The pedigree ID stays unchanged even after removing some vertices from the graph.
         * The vertex_id as defined in Boost Graph Library will actually change after the removal operation.
         *
         * This takes O(1) even for BGLGraph_list_based, for which the original boost::vertex() takes O(V).
         * @return
         */
        vertex_descriptor vertex(unsigned int id) const {
            return id_2_vertex->at(id);
        }

        /**
         *
         * @param upper_end
         * @param v_start
         * @param end_of_path
         *
         * @return whether the direction leads to a dead end of a path.
         * This is true if and only if the end of the path is a leaf node of the graph.
         * the endpoint of the path will be given whether or not the path leads to a dead end.
         */
        bool leads_to_dead_end(
                bool upper_end,
                const vertex_descriptor v_start,
                vertex_descriptor &end_of_path // @todo :debug: this has a problem
        ) {
            std::optional<vertex_descriptor> dummy;
            std::tie(dummy, end_of_path) = follow_path(upper_end, v_start);

            return (bool) degree_one(end_of_path);
        }

    private:
        Graph *g = nullptr;
        std::shared_ptr<std::vector<vertex_descriptor>> id_2_vertex;
        value_map_type *value_map = nullptr; //!< map a vertex to the field value defined on it
        // the locations of the vertex descriptors
        std::vector<typename value_map_type::iterator> vertex_property_locations;
    };

    /**
     * Merge two edges attached to a degree-two vertex
     * @tparam Graph
     * @param first_vertex
     * @param middle_vertex the vertex to be removed
     * @param last_vertex
     * @param first_edge_properties: edge with the first and the middle vertex
     * @param second_edge_properties:edge with the middle and the last vertex
     * @param graph_util: common and auxiliary object
     * @return
     */
    template<class Traits /*= Contour_tree_traits<double>*/>
    typename Traits::Graph::edge_descriptor merge_edges (
            typename Traits::Graph::vertex_descriptor first_vertex,
            typename Traits::Graph::vertex_descriptor middle_vertex,
            typename Traits::Graph::vertex_descriptor last_vertex,
            typename Traits::Graph::edge_property_type& first_edge_properties,
            typename Traits::Graph::edge_property_type& second_edge_properties,
            Graph_util<Traits>& graph_util)
             {
        auto& g = *graph_util.get_graph();
        auto edge_inserted = boost::add_edge(first_vertex, last_vertex, g);
        if (!edge_inserted.second) throw std::runtime_error{"edge insertion failed; probably an edge already exists."};

        auto new_edge = edge_inserted.first;

        // merge the edge properties
        g[new_edge] = merge_properties(first_edge_properties, second_edge_properties);

        graph_util.clear_and_remove_vertex(middle_vertex);
        return new_edge;
    };

    /**
     * Potentially slower depending on how the edge properties are accessed from the graph.
     * Should not be slow for graphs in Boost Graph Library because descriptors are iterators.
     *
     * This removes properties of the removed vertex as long as the graph_util is passed the property map.
     */
    template<class Traits /*= Contour_tree_traits<double>*/>
    typename Traits::Graph::edge_descriptor merge_edges (
            typename Traits::Graph::vertex_descriptor first_vertex,
            typename Traits::Graph::vertex_descriptor middle_vertex,
            typename Traits::Graph::vertex_descriptor last_vertex,
            typename Traits::Graph::edge_descriptor   first_edge,
            typename Traits::Graph::edge_descriptor   second_edge,
            Graph_util<Traits>& graph_util
            ) {
        auto& g = *graph_util.get_graph();
        auto& first_edge_properties = g[first_edge];
        auto& second_edge_properties = g[second_edge];

        return merge_edges(
                first_vertex,
                middle_vertex,
                last_vertex,
                first_edge_properties,
                second_edge_properties,
                graph_util
        );
    }



    template<class Contour_tree_traits>
    class Compute_sensitive_contour_tree {
    public:
        using Graph = typename Contour_tree_traits::Graph;
        using vertex_descriptor = typename boost::graph_traits<Graph>::vertex_descriptor;
        using edge_descriptor = typename boost::graph_traits<Graph>::edge_descriptor;
        using Graph_util_ = Graph_util<Contour_tree_traits>;

        Compute_sensitive_contour_tree(std::shared_ptr<Graph_util_> graph_util):
            graph_util {graph_util}
        { }

        edge_descriptor get_edge(unsigned int edge, std::vector<vertex_descriptor>& accumulated, Graph& g) {
            auto& lower_vertex = accumulated[edge];
            auto& upper_vertex = accumulated[edge + 1];

            auto edge_exists = boost::edge(lower_vertex, upper_vertex, g);
            // edge does not exist?
            if (!edge_exists.second) throw std::runtime_error {"Edge not found when it should be there"};

            return edge_exists.first;
        }

        using edge_property_type = typename Graph::edge_property_type;

        bool degree_zero(vertex_descriptor v, Graph& g) {
            auto [ai, ae] = boost::adjacent_vertices(v, g);
            return ai == ae;
        }


        void do_it() {
            auto& g = *graph_util->get_graph();

            // treat edges
            for ( auto [vi, ve] = boost::vertices(g);
                  vi != ve;
                  ++vi
                    ) {

                auto vertex = *vi;

                auto value = graph_util->value(vertex);

                using accumulate_edges = typename Graph_util_::accumulate_edges;
                accumulate_edges accumulate;
                // vector of vertexes
                auto& accumulated = accumulate.get_accumulated();

                // get the vertexes on the path
                // @todo Note: currently we use two vertices to access an edge.
                // it is faster to return edge descriptor in the follow_path.
                auto followed_path_lower = graph_util->follow_path(/*go_up*/false, vertex, accumulate);

                std::reverse(accumulated.begin(), accumulated.end());

                // get the vertexes on the path
                auto followed_path_upper = graph_util->follow_path(/*go_up*/true,  vertex, accumulate);

                // vertex on the path: sorted from bottom to top
                unsigned int cnt = 0;
                auto first_edge = accumulated[cnt];
                vertex_descriptor first_vertex;
                {
                    auto s = boost::source(first_edge, g);
                    auto t = boost::source(first_edge, g);
                    first_vertex
                            = (graph_util->value(s) < graph_util->value(t))?
                              s : t;
                }

                auto first_edge_properties = g[first_edge];

                unsigned int last_vertex_id;
                for (cnt = 1; cnt < accumulated.size(); ++cnt) {
                    // merge the edges
                    auto current_edge = accumulated[cnt];
                    auto current_edge_properties = g[current_edge];

                    auto s = boost::source(current_edge,g);
                    auto t = boost::target(current_edge,g);
                    auto [middle_vertex,last_vertex]
                    = (graph_util->value(s) < graph_util->value(t))?
                      std::tie(s,t) : std::tie(t,s);

                    // last?
                    if (cnt == accumulated.size() - 1) last_vertex_id = g[last_vertex].pedigree_id;

                    auto middle_vertex_id = g[middle_vertex].pedigree_id;
                    merge_edges(
                            first_vertex, middle_vertex, last_vertex,
                            first_edge_properties,
                            current_edge_properties,
                            *graph_util);
                }
            }
        }
    private:
        std::shared_ptr<Graph_util_> graph_util = nullptr;
    };

    template <class SimplificationTraits>
    class Prunability_checker {
    public:
        using Graph_util_ = Graph_util<SimplificationTraits>;
        using Graph = typename SimplificationTraits::Graph;
        using vertex_descriptor = typename Graph::vertex_descriptor;
        using Leaf_superarc = typename Graph_util_::Leaf_superarc;

        Prunability_checker (const std::shared_ptr<Graph_util_>& graph_util):
                graph_util {graph_util}
        {
        }

        /**
         * @page prunability_doc
         *
         * Prunability
         * -----------
         * Carr \cite Carr:PhD argues not all leaves of a contour tree ought to be pruned in the simplification.
         * Specifically, those leaves that may be pruned are said to be *prunable*.
         *
         * Let \f$ f(v) \f$ denote the field value for the graph vertex \f$ v \f$.
         * A superarc with the leaf vertex \f$ v_0 \f$ is prunable if it is a leaf and the vertex u connecting to the rest of the graph
         * has another superarc with its leaf vertex \f$ v_1\f$, on the same side, that is,
         * \f{equation}{ (f(v_0) - f(u)) (f(v_1) - f(u)) > 0. \f}
         *
         * Prunability is often impletented by setting the persistence to ∞.
         * This, however, requries some consideration in the algorithm
         * as to when in the algorithmic procedure a leaf should be checked for prunability.
         *
         * For now, we avoid using ∞ by checking the prunability every time we before pruning the leaf.
         */

        /**
         * If this is a *prunable* superarc, return the other end of the leaf superarc.
         *
         */
        std::optional<vertex_descriptor>
        prunable(vertex_descriptor v, vertex_descriptor the_other_end, bool *v_is_positioned_down_p = nullptr) {
            using out = std::optional<vertex_descriptor>;

            bool v_is_positioned_down;
            if (v_is_positioned_down_p) {
                v_is_positioned_down = *v_is_positioned_down_p;
            } else {
                v_is_positioned_down = graph_util->value(v) < graph_util->value(the_other_end);
            }

            int did_find_same_side = 0;

            auto g = graph_util->get_graph();

            // check if we can prune this leaf
            for (auto[ai, ae] = boost::adjacent_vertices(the_other_end, *g);
                 ai != ae;
                 ++ai
                    ) {
                // we found another leaf on this side
                bool same_side = (v_is_positioned_down == (graph_util->value(*ai) < graph_util->value(the_other_end)));

                if (did_find_same_side && same_side) return out{the_other_end};

                if (same_side) did_find_same_side = true;
            }

            return std::nullopt;
        }

        /**
         * Check if we can prune the given leaf.
         */
        std::optional<vertex_descriptor> prunable(Leaf_superarc &leaf_to_prune) {
            return prunable(leaf_to_prune.get_leaf_vertex(), leaf_to_prune.get_the_other_end());
        }

        /**
         * if this is a prunable leaf, return the other end of the leaf superarc
         * @param v
         * @return
         */
        std::optional<vertex_descriptor> prunable(vertex_descriptor v) {
            using out = std::optional<vertex_descriptor>;
            decltype(graph_util->find_the_other_end(v)) the_other_end;
            bool v_is_positioned_down;

            try {
                the_other_end = graph_util->find_the_other_end(v, &v_is_positioned_down);
            } catch (const typename Graph_util_::not_degree_one &e) {
                // not degree-one: not a leaf
                return std::nullopt;
            }

            return prunable(v, the_other_end);
        }

    private:
        std::shared_ptr<Graph_util_> graph_util;
    };

    template <class SimplificationTraits>
    class Leaf_pruner  {
    public:
        using Graph_util_ = Graph_util<SimplificationTraits>;
        using Graph = typename SimplificationTraits::Graph;
        using vertex_descriptor = typename Graph::vertex_descriptor;
        using Leaf_superarc = typename Graph_util_::Leaf_superarc;

        Leaf_pruner (const std::shared_ptr<Graph_util_>& graph_util
        ) :
                graph_util {graph_util}
        {
            prunability_checker = std::make_shared<Prunability_checker<SimplificationTraits>>(graph_util);
        }

        struct leaf_not_prunable : public std::runtime_error {
            leaf_not_prunable() : std::runtime_error{"this leaf is not allowed to be pruned"} {}
        };

        template <bool check_prunable>
        std::optional<vertex_descriptor>
        prune_from_graph(Leaf_superarc& leaf_to_prune) {

            if constexpr (check_prunable) {
                if (!this->prunable(leaf_to_prune)) {
                    throw typename Graph_util_::leaf_not_prunable {};
                }
            }

            auto leaf_vertex = leaf_to_prune.get_leaf_vertex();
            bool go_up = graph_util->goes_up_from_leaf(leaf_vertex);

            typename Graph_util_::accumulate_vertexes accumulated;
            // accumulate regular vertexes
            auto path = graph_util->template follow_path<typename Graph_util_::accumulate_vertexes>(go_up, leaf_vertex, accumulated);
            // delete the vertexes in the superarc
            for (auto& v: accumulated.get_accumulated()) {
                graph_util->clear_and_remove_vertex(v);
            }
            // delete the leaf vertex
            graph_util->clear_and_remove_vertex(leaf_vertex);

            auto end_of_visit = path.end_of_visit;
            // Did this create a new end?
            // this may happen when the leaf was connected to a degenerate critical point
            // where the point was a local extremum and a saddle at the same time.
            if (graph_util->degree_one(end_of_visit)) {
                return std::make_optional(end_of_visit);
            }

            return std::nullopt;
        }

        struct leafs_to_update {
            std::optional<Leaf_superarc> existing_leaves[2] = {std::nullopt, std::nullopt};
            std::optional<Leaf_superarc> new_leaf = std::nullopt;
        };

        void set_prunability_checker(
                const std::shared_ptr<Prunability_checker<SimplificationTraits>>& checker
        ) {
            prunability_checker = checker;
        }

        /**
         * If no prunability check is required, a null pointer is returned.
         * @return
         */
        std::shared_ptr<Prunability_checker<SimplificationTraits>>& get_prunability_checker() {
            return prunability_checker;
        }

        /**
         * Prune a leaf.
         * If the leaf pruning updates another leaf, return the result.
         *
         * This function does not modify the leaf queue.
         *
         * @param leaf_to_prune
         * @return updated leaf arc
         */
        template <bool check_prunable>
        auto prune_one_leaf(
                Leaf_superarc& leaf_to_prune
        ) {
            using out_type = leafs_to_update;

            // We don't check whether the given leaf is actually a leaf below because -
            // 1. leaf_to_prune should have checked that already
            // 2. it confuses with another throw from `prunable()`
//            if (!degree_one(leaf_to_prune.get_leaf_vertex())) throw not_a_leaf {};

            // Check
            if constexpr (check_prunable) {
                if (prunability_checker && !prunability_checker->prunable(leaf_to_prune)) { throw leaf_not_prunable { };}
            }

            out_type out {};

            // keep the other end of the leaf
            auto old_end = leaf_to_prune.get_the_other_end();

            // remove the leaf from the graph (no safety check because we did it already)
            auto new_leaf = prune_from_graph</*check prunable = */false>(leaf_to_prune);
            if (new_leaf) {
                auto leaf_superarc = Leaf_superarc{*new_leaf, graph_util.get()};
                // compute the persistence
                out.new_leaf = std::make_optional(leaf_superarc);
            }

            // no additional leaf that is updated after the removal of the input leaf
            if( ! graph_util->regular_vertex(old_end) ) return out;

            vertex_descriptor upper_endpoint;
            vertex_descriptor lower_endpoint;
            auto upper_is_dead_end = graph_util->leads_to_dead_end(/*up*/true, old_end, upper_endpoint);
            auto lower_is_dead_end = graph_util->leads_to_dead_end(/*up*/false, old_end, lower_endpoint);

            auto g = graph_util->get_graph();

            if (upper_is_dead_end) {
                // the upper leaf superarc got longer due to the removal of the input leaf superarc
                out.existing_leaves[0].emplace(upper_endpoint, lower_endpoint, *g);
            }
            if (lower_is_dead_end) {
                // the lower leaf superarc got longer
                out.existing_leaves[1].emplace(lower_endpoint, upper_endpoint, *g);
                // It is tempting to change the if clause above to `else if` because
                // generally the other end would not be a leaf if the previous one was a leaf.
                // However, a tree with two leaves is a special case,
                // where the other side is a leaf, too.
            }
            return out;
        }

        /**
         * v must be a leaf vertex
         * @param v
         * @return
         */
        bool prunable(vertex_descriptor v) {
            // check not required => return true
            if (!prunability_checker) return true;
            // check
            return (prunability_checker && prunability_checker->prunable(v));
        }
    private:
        std::shared_ptr<Graph_util_> graph_util;
        std::shared_ptr<Prunability_checker<SimplificationTraits>> prunability_checker;
    };

    template<class SimplifyContourTree_>
    class Ending_condition {
    public:
        Ending_condition() {}

        Ending_condition(SimplifyContourTree_ *algorithm) :
                algorithm(algorithm) {}

        /**
         * quit the simplification loop?
         */
        virtual bool exit_loop() = 0;

        bool continue_loop() {
            return !exit_loop();
        }

        void set_algorithm(SimplifyContourTree_ *algorithm) {
            this->algorithm = algorithm;
        }

        SimplifyContourTree_ *get_algorithm() {
            return algorithm;
        }

    protected:
        SimplifyContourTree_ *algorithm = nullptr;
    };

    /**
     * Maintian the queue
     * @tparam Value_map
     */
    template <class SimplificationTraits>
    class SimplifyContourTree {
    public:
        using Graph_util_ = Graph_util<SimplificationTraits>;
        using Simplification_traits = SimplificationTraits;
        using Graph = typename Simplification_traits::Graph;
        using vertex_descriptor = typename Graph::vertex_descriptor;
        using Superarc = typename Graph_util_::Superarc;
        using Leaf_superarc = typename Graph_util_::Leaf_superarc;
        using Value_map = typename SimplificationTraits::Value_map;
        using Leaf_pruner_ = Leaf_pruner<SimplificationTraits>;

        void set_graph(Graph &g, Value_map &value_map,
                       std::shared_ptr<std::vector<vertex_descriptor>> id_2_vertex = nullptr) {
            graph_util = std::make_shared<Graph_util_>();
            graph_util->set_graph(g, value_map, id_2_vertex);
            leaf_pruner = std::make_shared<Leaf_pruner_>(graph_util);
        }

        auto set_graph_util(std::shared_ptr<Graph_util_>& graph_util) {
            this->graph_util = graph_util;
        }
        auto get_graph_util() {
            return graph_util;
        }

        auto get_leaf_pruner() {
            return leaf_pruner;
        }

        void set_ending_condition(Ending_condition<SimplifyContourTree<SimplificationTraits>>* ending_condition) {
            ending_condition->set_algorithm(this);
            this->ending_condition = ending_condition;
        }

        /**
         * This does not take into account the Simplicity of Simulation.
         * Doing so requires some tricky math.
         */
        class Persistence {
        public:
            using value_type = double;
            using Superarc = typename Graph_util_::Superarc;

            Persistence (Superarc& arc, Graph_util_& algorithm): value_ (compute(arc,algorithm)) {
            }
            value_type value() const {return value_;}

            static value_type compute(Superarc& superarc, Graph_util_& algorithm) {
                auto vtx = superarc.end(0);
                auto val = algorithm.value(vtx);

                auto other_end_vtx = superarc.end(1);
                auto other_end_val = algorithm.value(other_end_vtx);

                return std::abs(val.value() - other_end_val.value());
            }

            bool operator < (const Persistence& p) const {
                return this->value() < p.value();
            }

            friend
            std::ostream& operator << (std::ostream& o, const Persistence& p) {
                return o << "{persistence value: " <<  p.value_ << "}" ;
            }
        private:
            /// This makes it easier to notice when the persistence value is unset.
            value_type value_ = std::numeric_limits<double>::max();
        };

        /**
         * Map a superarc to
         * @tparam Position
         */
        template <class Position>
        class Position_in_queue {
        public:
            Position_in_queue () {}
            Position_in_queue (std::shared_ptr<Graph_util_>& graph_util) : graph_util {graph_util}  {}
            using Superarc = typename Graph_util_::Superarc;

            void set_graph_util(std::shared_ptr<Graph_util_>& graph_util) {this->graph_util = graph_util;}

            Position& get(Superarc& superarc) {
                auto leaf = dynamic_cast<Leaf_superarc*>(&superarc);
                if (!leaf) {
                    throw std::runtime_error {std::string{"unexpected type" __FILE__} + std::to_string(__LINE__)};
                }
                auto id = get_pedigree_id(*leaf);
                return positions.at(id);
            }
            void set(Superarc& superarc, Position& position) {
                auto leaf = dynamic_cast<Leaf_superarc*>(&superarc);
                if (!leaf) {
                    throw std::runtime_error { std::string{"unexpected type" __FILE__} + std::to_string(__LINE__)};
                }
                auto id = get_pedigree_id(*leaf);
                if(positions.size() < id +1) {
                    positions.resize(id + 1);
                }
                positions[id] = position;
            }

        protected:
            auto get_pedigree_id(Leaf_superarc& leaf) {
                auto v = leaf.get_leaf_vertex();
                auto g = graph_util->get_graph();
                auto id = (*g)[v].pedigree_id;

                return id;
            }

        private:
            std::shared_ptr<Graph_util_> graph_util = nullptr;
            std::vector<Position> positions;
        };

        /**
         * We do tricky low-level operations here.
         * Especially, we need to maintain the consistency between the superarcs and their position.
         * Whenever we update a position of a superarc in a queue by (e.g.) embrace,
         * we update the map from the superarcs to their positions.
         */
        class Superarc_queue_details {
        protected:
            using queue_type = std::multimap<Persistence,std::unique_ptr<Superarc>>;

        public:
            Superarc_queue_details() {}
            Superarc_queue_details(SimplifyContourTree* graph_util): graph_util {graph_util} {}

            void set_graph_util(std::shared_ptr<Graph_util_>& graph_util) {
                this->graph_util = graph_util;
                this->positions.set_graph_util(graph_util);
            }

            using Leaf_superarc = typename Graph_util_::Leaf_superarc;

            /**
             * Iterator in a queue pointing at a superarc
             */
            using iterator = typename queue_type::iterator;
            using supearc_position_in_queue = iterator;

            using const_iterator = typename queue_type::const_iterator;
            using key_type = typename queue_type::key_type;
            using mapped_type = typename queue_type::mapped_type;
            // We avoid value_type because it's confusing with mapped_type

            iterator begin(){
                return queue.begin();
            }

            iterator end(){
                return queue.end();
            }

            void clear() {
                num_leaves = 0;
                queue.clear();
            }

            auto erase(iterator position, mapped_type& content) {
                bool is_leaf = dynamic_cast<Leaf_superarc*>(position->second.get());
                if (is_leaf) --num_leaves;
                content = std::move(position->second);
                auto out = queue.erase(position);
                return out;
            }

            auto emplace(key_type& key, mapped_type&& superarc) {
                bool is_leaf = dynamic_cast<Leaf_superarc*>(superarc.get());
                auto x =  queue.emplace(key, std::move(superarc));
                if (is_leaf) ++num_leaves;
                return remember_position(x, x->second);
            }

            auto emplace_hint(iterator& hint, const key_type& key, mapped_type&& supearc) {
                bool is_leaf = dynamic_cast<Leaf_superarc*>(supearc.get());
                auto x =  queue.emplace_hint(hint, key, std::move(supearc));
                if (is_leaf) ++num_leaves;
                return remember_position(x, x->second);
            }

            iterator find_position(Superarc& superarc) {
                return positions.get(superarc);
            }

            auto size() {return queue.size();}

//            friend
//            std::ostream& operator << (std::ostream& o, const Superarc_queue_details& queue) {
//                if (queue.graph_util) {
//                    for (auto& p: queue.queue) {
//                        o << "{" << p.first << ",";
//                        o << "{leaf vertex:" <<  (*queue.graph_util->get_graph())[p.second.get_leaf_vertex()].pedigree_id <<"}";
//                        o << "},";
//                    }
//                } else {o << "leaf queue unset ";}
//                return o;
//            }

            auto get_graph_util() const {
                return graph_util;
            }

            size_t get_num_leaves() {
                return num_leaves;
            }

        private:
            auto remember_position(iterator& x, mapped_type& superarc) {
                // fail
                if (x == queue.end()) {
                    return x;
                }

                positions.set(*superarc, x);

                return x;
            }

            std::shared_ptr<Graph_util_> graph_util = nullptr;
            Position_in_queue<supearc_position_in_queue> positions;
            queue_type queue;
            size_t num_leaves = 0;
        };

        /**
         * Container for the superarcs.
         * It sorts the superarcs according to their persistence (smaller ones in front).
         *
         * The persistence is defined in this class because it is tied to the sorting
         * rather than to the superarcs themselves.
         *
         * This queue stores a pointer instead of value in order to update the value, which requires
         * a temporary extraction of the value from the queue.
         */
        class Superarc_queue: public Superarc_queue_details {
        protected:
            using queue_type = typename Superarc_queue_details::queue_type;
        public:

            Superarc_queue(): Superarc_queue_details {} {}
            Superarc_queue(SimplifyContourTree* s): Superarc_queue_details {s} {}

            /**
             * Update one leaf at a position
             *
             * @param old_position
             */
            void update(Superarc& superarc){

                auto old_position = this->find_position(superarc);

                if (old_position == this->end()) {
                    throw std::runtime_error {"attempting to treat a leaf that is not in the queue."};
                }

                using mapped_type = typename Superarc_queue_details::mapped_type;

                typename queue_type::iterator hint;
                {
                    hint = old_position;
                    ++hint;

                    mapped_type dummy;
                    this->erase(old_position,dummy);
                }

                mapped_type superarc_in_queue = nullptr;
                if (auto leaf_superarc = dynamic_cast<Leaf_superarc*>(&superarc)){
                    superarc_in_queue = std::unique_ptr<Superarc>(new Leaf_superarc{*leaf_superarc});
                } else {
                    std::cerr <<"not implemented yet\t"<< __FILE__ << ":" << __LINE__ << std::endl;
                    abort();
                }

                this->emplace_hint(hint, Persistence{superarc, *this->get_graph_util()}, std::move(superarc_in_queue));
            }
        };

    public:
        using supearc_position_in_queue = typename Superarc_queue::supearc_position_in_queue;

        class no_prunable_superarc_left : std::runtime_error  {
        public:
            no_prunable_superarc_left () : std::runtime_error{"No prunable leaf left"} {}
        };

        void prune_one_leaf_superarc_from_queue(Leaf_superarc& leaf_superarc) {

            // remove from the graph (i) the leaf vertex (ii) and the vertices inside the superarc
            // be careful because this invalidates the vertex descriptors in the pruned leaf
            auto leaves_updated = (leaf_pruner->template prune_one_leaf</*check prunable = */false>)(leaf_superarc);

            // Did we update a leaf by this pruning?
            // => maintain the queue of leaves
            for (auto& leaf: leaves_updated.existing_leaves) {
                if (leaf) supearc_queue.update(*leaf);
            }

            if (auto& new_leaf  = leaves_updated.new_leaf) {
                auto p = Persistence{*new_leaf, *graph_util.get()};
                auto leaf_in_queue = std::make_unique<Leaf_superarc>(*new_leaf);
                supearc_queue.emplace(p, std::move(leaf_in_queue));
            }
        }

        /**
         * Prune a superarc that can be pruned and is placed at the front most of the queue.
         */
        void prune_one_superarc_from_queue() {

            using superarc_queue_type = decltype(supearc_queue);
            using supearc_position_in_queue = typename superarc_queue_type::supearc_position_in_queue;

            // We...
            // - erase the supearc from the graph and the queue
            // - if another superarc is changed by this operation...
            //   - if it is a leaf
            //     - update the leaf, and
            //     - update its position in the queue

            // the vertex connecting the leaf to the graph
            typename decltype(supearc_queue)::mapped_type superarc_popped;

            for (auto iter = supearc_queue.begin(); iter != supearc_queue.end(); ++iter) {
                auto& [persistence, superarc] = *iter;
                if (auto leaf = dynamic_cast<Leaf_superarc*>(superarc.get())){
                    // TODO It seems as if we could just pass the Leaf_superarc instance to `leaf_pruner` here.
                    if( leaf_pruner->prunable(leaf->get_leaf_vertex()) ) {
                        supearc_queue.erase(iter, superarc_popped);
                        leaf = dynamic_cast<Leaf_superarc*>(superarc_popped.get());
                        prune_one_leaf_superarc_from_queue(*leaf);
                        return;
                    }
                }
            }
            throw no_prunable_superarc_left{};
        }

        auto& get_queue() {
            return supearc_queue;
        }

        void setup_queue() {
            supearc_queue.clear();
            supearc_queue.set_graph_util(graph_util);

            auto&g = *graph_util->get_graph();
            // for each vertex...
            // if it is a leaf
            // insert it in the queue
            for (auto [vi, ve] = boost::vertices(g);
                vi != ve;
                ++vi
            ) {
                // a leaf candidate
                auto leaf_vertex = *vi;

                // we are interested only in leaves
                if (!graph_util->degree_one(leaf_vertex)) continue;

                typename decltype(supearc_queue)::mapped_type
                leaf_superarc = std::make_unique<Leaf_superarc>(leaf_vertex, graph_util.get());

                // compute the persistence
                typename decltype(supearc_queue)::key_type
                        p = Persistence{*leaf_superarc, *graph_util};

                // keep it in the queue
                supearc_queue.emplace(p, std::move(leaf_superarc));
            }
        }

        void doIt() {

            if (!ending_condition) {
                throw std::runtime_error("ending condition for simplification not set");
            }
            setup_queue();

            while (ending_condition->continue_loop()) {
                try {
                    prune_one_superarc_from_queue();
                } catch (const no_prunable_superarc_left& e) {
                    break;
                }
            }
        }
    private:
        std::shared_ptr<Graph_util_> graph_util = nullptr;
        std::shared_ptr<Leaf_pruner_> leaf_pruner;
        Superarc_queue supearc_queue;
        Ending_condition<SimplifyContourTree<SimplificationTraits>>* ending_condition = nullptr;
    };

    template<class SimplifyContourTree_>
    class End_with_leaf_count: public Ending_condition<SimplifyContourTree_> {
    public:
        End_with_leaf_count(): Ending_condition<SimplifyContourTree_>{} {}
        End_with_leaf_count(SimplifyContourTree_ * algorithm, int num_leaves) :
                Ending_condition<SimplifyContourTree_> {algorithm},
                num_leaves_in_the_end {num_leaves_in_the_end}
        { }
        void set_num_leaves_in_the_end(int num_leaves_in_the_end) {
            this->num_leaves_in_the_end = num_leaves_in_the_end;
        }
        bool exit_loop () override {
            auto current_num_leaves = this->get_algorithm()->get_queue().get_num_leaves();
            return num_leaves_in_the_end >= current_num_leaves;
        }

    private:
        int num_leaves_in_the_end = 2;
    };
}


#endif //TOPOCULE_SIMPLIFYCONTOURTREE_H
