//
// Created by Daisuke Sakurai on 2019/12/05.
//

#include <iostream>
#include <test.h>
#include <BGLGraph.h>
#include <GraphIO.h>
#include <sstream>

#include <prettyprint.hpp>

#include <test_compare_graphs.h>
#include "SimplifyContourTree.h"

template <class Graph>
class my_vertex_index_map {
public:
    using vertex_descriptor = typename boost::graph_traits<Graph>::vertex_descriptor;
    std::map<vertex_descriptor, boost::vertex_index_t> property_map;
};

namespace {
/*
 * graph           value
 *    2             1 + epsilon
 *    |
 *    1             1
 *    |
 *    0             0
 */
std::string linear_graph = R"(graph G {
0 [field=0];
1 [field=1];
2 [field=1];
0--1 ;
1--2 ;
}
)";

/*
 * graph             value
 *    3            3 + 2 * epsilon
 *  2 |            2 + epsilon
 *  \ |
 *    0            0
 *    |
 *    1            -1
 */
std::string y_shaped_graph = R"(graph G {
0 [field=0];
1 [field=-1];
2 [field=2];
3 [field=3];
0--1 ;
0--2 ;
0--3 ;
}
)";

/*
 * graph             value
 *
 *    3            3 + 3 * epsilon
 *  2 |            2 + 2 * epsilon
 *  | |
 *  5 |            1 + 5 * epsilon
 *  | |
 *  4 |            1 + 4 * epsilon
 *  \ |
 *    0            0
 *    |
 *    1            -1 + 1 * epsilon
 */
    std::string y_shaped_long_graph = R"(graph G {
0 [field=0];
1 [field=-1];
2 [field=2];
3 [field=3];
4 [field=1];
5 [field=1];
0--1 ;
0--3 ;
0--4 ;
4--5 ;
5--2 ;
}
)";

/*
 * graph             value
 *    3            3 + 3 * epsilon
 *    |
 *    | 1          2 + 1 * epsilon
 *  2 | |          1 + 2 * epsilon
 *  \ |/
 *    0            0
 */
std::string three_fork_graph = R"(graph G {
0 [field=0];
1 [field=2];
2 [field=1];
3 [field=3];
0--1 ;
0--2 ;
0--3 ;
}
)";
}

/*
 * graph             value
 *    3            10 + 2 * epsilon
 *  2 |            9 + epsilon
 *  | |
 *  \ |
 *    0            0
 *    |
 *    1           -1
 *    |
 *  5 |           -3 // <- going to be pruned
 *   \|
 *    4           -4
 */
std::string hook_shaped_graph = R"(graph G {
0 [field=0];
1 [field=-1];
2 [field=2];
3 [field=3];
4 [field=-3];
5 [field=-2];
0--1 ;
0--2 ;
0--3 ;
}
)";

template <class Graph>
using map_type = std::map<typename Graph::vertex_descriptor, double>;

template <class Graph_util>
bool edge_exists_for_the_two_directions(
        unsigned int v0,
        unsigned int v1,
        Graph_util util) {
    auto& g = *util->get_graph();
    return boost::edge(
            util->vertex(v0),
            util->vertex(v1),
            g).second
           && boost::edge(
            util->vertex(v1),
            util->vertex(v0),
            g).second;
}

template<class Graph_util>
void check_one_leaf_pruning(
        typename Graph_util::Graph& g,
        Graph_util* util,
        std::string called_location
) {
    using Leaf_superarc = typename Graph_util::Leaf_superarc;

    // the number of edges are reduced
    INFO("called at " + called_location);
    CHECK(boost::num_vertices(g) == 3);
    CHECK(boost::num_edges(g) == 2);

    auto [vi, ve] = boost::vertices(g);
    CHECK(g[*vi].pedigree_id == 0);
    ++vi;
    CHECK(g[*vi].pedigree_id == 1);
    ++vi;
    CHECK(g[*vi].pedigree_id == 3);

    // these edges exist
    CHECK(boost::edge(
            util->vertex(0),
            util->vertex(1),
            g).second);
    CHECK(boost::edge(
            util->vertex(1),
            util->vertex(0),
            g).second);
    // this edge doesn't
    CHECK( !boost::edge(
            util->vertex(0),
            util->vertex(2),
            g).second);
    CHECK(boost::edge(
            util->vertex(0),
            util->vertex(3),
            g).second);
}

TEST_CASE ("Test SimplifyContourTree") {
    using namespace tpcl;

    /// @todo use the dot graph reader instead of hard-coding the graph structure
    BGLGraph_list_based g;

    using vertex_descriptor = boost::graph_traits<decltype(g)>::vertex_descriptor;

    map_type<decltype(g)> values;

    using traits = Contour_tree_traits<decltype(values)::mapped_type>;
    SimplifyContourTree<traits> computation;
    using Graph_util = tpcl::Graph_util<traits>;

    using Leaf_superarc = Graph_util::Leaf_superarc;
    using Leaf_pruner = Leaf_pruner<traits>;

    GIVEN ("a linear graph") {
        // load linear graph
        std::stringstream{linear_graph} >> BGLGraphLoader{g}.with_property_a("field", values);
        computation.set_graph(g, values);
        auto util = computation.get_graph_util();

        decltype(util->regular_vertex(boost::vertex(0,g))) regular;

        regular = util->regular_vertex(boost::vertex(0,g));
        CHECK(!regular);

        regular = util->regular_vertex(boost::vertex(1,g));
        CHECK(regular);
        CHECK(regular->lower(g) == boost::vertex(0,g));
        CHECK(regular->upper(g) == boost::vertex(2,g));

        regular = util->regular_vertex(boost::vertex(2,g));
        CHECK(!regular);


        decltype(util->follow_path(/*go_up*/ true, boost::vertex(0, g) ))
                followed_path;

        followed_path = util->follow_path(/*go_up*/ true, boost::vertex(0, g));
        CHECK(followed_path.end_of_visit       == boost::vertex(2, g));
        CHECK(followed_path.one_before.value() == boost::vertex(1, g));

        followed_path = util->follow_path(/*go_up*/ false, boost::vertex(0, g));
        CHECK( followed_path.end_of_visit == boost::vertex(0,g));
        CHECK(!followed_path.one_before);

        followed_path = util->follow_path(/*go_up*/ true, boost::vertex(1, g));
        CHECK(followed_path.end_of_visit       == boost::vertex(2, g));
        CHECK(followed_path.one_before.value() == boost::vertex(1, g));

        followed_path = util->follow_path(/*go_up*/ false, boost::vertex(1, g));
        CHECK(followed_path.end_of_visit       == boost::vertex(0, g));
        CHECK(followed_path.one_before.value() == boost::vertex(1, g));

        followed_path = util->follow_path(/*go_up*/ true, boost::vertex(2, g));
        CHECK(followed_path.end_of_visit       == boost::vertex(2, g));
        CHECK(!followed_path.one_before);

        followed_path = util->follow_path(/*go_up*/ false, boost::vertex(2, g));
        CHECK( followed_path.end_of_visit == boost::vertex(0,g));
        CHECK(followed_path.one_before.value() == boost::vertex(1, g));


        decltype(g)::vertex_descriptor v_end;

        CHECK(util->leads_to_dead_end(
                /*upwards = */ true,
                               boost::vertex(0,g),
                               v_end
        ));
        CHECK(v_end == boost::vertex(2,g));

        CHECK(util->leads_to_dead_end(
                /*upwards = */ false,
                               boost::vertex(0,g),
                               v_end
        ));
        CHECK(v_end == boost::vertex(0,g));

        CHECK_THROWS_AS((Leaf_superarc {boost::vertex(1,g), util.get()}), Graph_util::not_a_leaf);

        Compute_sensitive_contour_tree<traits> sensitive {util};
        sensitive.do_it();

        CHECK(boost::num_vertices(g) == 2);
        CHECK(boost::num_edges(g) == 1);
        CHECK(boost::vertex(0,g) == util->vertex(0));
        CHECK(boost::vertex(1,g) == util->vertex(2));

        auto n = boost::num_vertices(g);

        auto leaf_pruner = computation.get_leaf_pruner();

        CHECK(boost::num_vertices(g) == 2);
        CHECK(boost::num_edges(g) == 1);
        CHECK(boost::vertex(0,g) == util->vertex(0));
        CHECK(boost::vertex(1,g) == util->vertex(2));

        WHEN("using the queue") {
            computation.setup_queue();
            CHECK_THROWS_AS(computation.prune_one_superarc_from_queue(), decltype(computation)::no_prunable_superarc_left);
        }
        WHEN("pruning with an ending condition") {
            End_with_leaf_count<decltype(computation)> ending_condition;
            computation.set_ending_condition(&ending_condition);
            computation.doIt();
            CHECK(boost::num_vertices(g) == 2);
            CHECK(boost::num_edges(g) == 1);
            auto [vi, ve] = boost::vertices(g);
            CHECK(g[*vi].pedigree_id == 0);
            ++vi;
            CHECK(g[*vi].pedigree_id == 2);
            CHECK(edge_exists_for_the_two_directions(0,2, util));
        }
    }

    GIVEN ("a y-shaped graph") {
        std::stringstream{y_shaped_graph} >> BGLGraphLoader{g}.with_property_a("field", values);
        computation.set_graph(g, values);
        auto util = computation.get_graph_util();
        auto leaf_pruner = computation.get_leaf_pruner();

        auto followed = util->follow_path(/*go_up*/ true, boost::vertex(0,g));
        CHECK(followed.end_of_visit == boost::vertex(0,g));
        CHECK(!followed.one_before);

        for (int i = 0; i < 4; ++i) {
            auto regular = util->regular_vertex(boost::vertex(i,g));
            CHECK(!regular);
        }

        decltype(util->follow_path(/*go_up*/ true, boost::vertex(0, g) ))
                followed_path;

        followed_path = util->follow_path(/*go_up*/ true, boost::vertex(0, g));
        CHECK( followed_path.end_of_visit       == boost::vertex(0, g));
        CHECK(!followed_path.one_before);

        followed_path = util->follow_path(/*go_up*/ false, boost::vertex(0, g));
        CHECK( followed_path.end_of_visit       == boost::vertex(1, g));
        CHECK( followed_path.one_before.value() == boost::vertex(0, g));

        followed_path = util->follow_path(/*go_up*/ true, boost::vertex(1, g));
        CHECK( followed_path.end_of_visit       == boost::vertex(0, g));
        CHECK( followed_path.one_before.value() == boost::vertex(1, g));

        followed_path = util->follow_path(/*go_up*/ false, boost::vertex(1, g));
        CHECK( followed_path.end_of_visit       == boost::vertex(1, g));
        CHECK(!followed_path.one_before);

        followed_path = util->follow_path(/*go_up*/ true, boost::vertex(2, g));
        CHECK( followed_path.end_of_visit       == boost::vertex(2, g));
        CHECK(!followed_path.one_before);

        followed_path = util->follow_path(/*go_up*/ false, boost::vertex(2, g));
        CHECK( followed_path.end_of_visit       == boost::vertex(0, g));
        CHECK( followed_path.one_before.value() == boost::vertex(2, g));


        decltype(g)::vertex_descriptor v_end;

        CHECK(!util->leads_to_dead_end(
                /*upwards = */ true,
                               boost::vertex(0,g),
                               v_end
        ));
        CHECK(v_end == boost::vertex(0,g));

        CHECK( util->leads_to_dead_end(
                /*upwards = */ false,
                               boost::vertex(0,g),
                               v_end
        ));
        CHECK(v_end == boost::vertex(1,g));

        CHECK(!util->leads_to_dead_end(
                /*upwards = */ true,
                               boost::vertex(1,g),
                               v_end
        ));
        CHECK(v_end == boost::vertex(0,g));

        CHECK(boost::num_vertices(g) == 4);
        CHECK(boost::num_edges(g) == 3);

        WHEN ("prune an endpoint") {
            Leaf_superarc leaf_path{boost::vertex(2, g), util.get()};

            AND_WHEN ("prune an endpoint with a low-level function") {
                leaf_pruner->prune_from_graph</*check prunable = */false>(leaf_path);
                check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));
            }
            AND_WHEN ("prune an endpoint with a high-level function") {
                auto updated_leaves = leaf_pruner->prune_one_leaf<true>(leaf_path);

                check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));

                CHECK(Leaf_superarc{
                        util->vertex(3),
                        util->vertex(1), g}
                      == updated_leaves.existing_leaves[0].value());
                CHECK(Leaf_superarc{
                        util->vertex(1),
                        util->vertex(3), g}
                      == updated_leaves.existing_leaves[1].value());
            }
        }
        WHEN ("try pruning a leaf that shall NOT be pruned") {
            WHEN("case of vertex 1") {
                Graph_util::Leaf_superarc leaf_path {boost::vertex(1,g), util.get()};
                CHECK_THROWS_AS(leaf_pruner->prune_one_leaf</*check if prunable*/true>(leaf_path), Leaf_pruner::leaf_not_prunable);
            }
        }
        WHEN("pruning a leaf using the queue") {
            computation.setup_queue();
            computation.prune_one_superarc_from_queue();
            check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));

            CHECK_THROWS_AS(computation.prune_one_superarc_from_queue(), decltype(computation)::no_prunable_superarc_left);
        }
        WHEN("pruning with an ending condition") {
            End_with_leaf_count<decltype(computation)> ending_condition;
            computation.set_ending_condition(&ending_condition);
            computation.doIt();
            check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));
        }
    }

    GIVEN ("a y-shaped graph with an additional vertex in the path to be removed") {
        std::stringstream{y_shaped_long_graph} >> BGLGraphLoader{g}.with_property_a("field", values);
        computation.set_graph(g, values);
        auto util = computation.get_graph_util();
        auto leaf_pruner = computation.get_leaf_pruner();

        decltype(g)::vertex_descriptor v_end;

        WHEN("Pruning one leaf") {
            Leaf_superarc leaf_path{boost::vertex(2, g), util.get()};
            CHECK(g[leaf_path.get_the_other_end()].pedigree_id == 0);

            auto updated_leaves = leaf_pruner->prune_one_leaf<true>(leaf_path);

            check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));

            CHECK(Leaf_superarc{
                    util->vertex(3),
                    util->vertex(1), g}
                  == updated_leaves.existing_leaves[0].value());
            CHECK(Leaf_superarc{
                    util->vertex(1),
                    util->vertex(3), g}
                  == updated_leaves.existing_leaves[1].value());
        }
        WHEN("pruning a leaf using the queue") {
            computation.setup_queue();

            computation.prune_one_superarc_from_queue();
            check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));

            CHECK_THROWS_AS(computation.prune_one_superarc_from_queue(), decltype(computation)::no_prunable_superarc_left);
        }
        WHEN("pruning with an ending condition") {
            End_with_leaf_count<decltype(computation)> ending_condition;
            computation.set_ending_condition(&ending_condition);
            computation.doIt();
            check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));
        }
    }
    GIVEN ("a three-fork graph") {
        std::stringstream{three_fork_graph} >> BGLGraphLoader{g}.with_property_a("field", values);
        computation.set_graph(g, values);
        auto util = computation.get_graph_util();
        auto leaf_pruner = computation.get_leaf_pruner();

        decltype(g)::vertex_descriptor v_end;

        WHEN("identifying a (non-)dead end") {
            CHECK(!util->leads_to_dead_end(
                    /*upwards = */ false,
                                   boost::vertex(0, g),
                                   v_end
            ));
            CHECK(v_end == boost::vertex(0, g));
        }
        WHEN ("pruning leaves") {
            Leaf_superarc leaf_path {boost::vertex(2,g), util.get()};
            auto updated_leaves = leaf_pruner->prune_one_leaf<true>(leaf_path);

            check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));

            Leaf_superarc leaf_path_1 {boost::vertex(1,g), util.get()};
            updated_leaves = leaf_pruner->prune_one_leaf<true>(leaf_path_1);

            CHECK(boost::num_vertices(g) == 2);
            CHECK(boost::num_edges(g) == 1);

            auto [vi, ve] = boost::vertices(g);
            CHECK(g[*vi].pedigree_id == 0);
            ++vi;
            CHECK(g[*vi].pedigree_id == 3);
            CHECK(edge_exists_for_the_two_directions(0,3, util));
        }
        WHEN("pruning a leaf using the queue") {
            computation.setup_queue();

            computation.prune_one_superarc_from_queue();
            check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));

            computation.prune_one_superarc_from_queue();

            CHECK(boost::num_vertices(g) == 2);
            CHECK(boost::num_edges(g) == 1);

            auto [vi, ve] = boost::vertices(g);
            CHECK(g[*vi].pedigree_id == 0);
            ++vi;
            CHECK(g[*vi].pedigree_id == 3);

            edge_exists_for_the_two_directions(0,3,util);

            CHECK_THROWS_AS(computation.prune_one_superarc_from_queue(), decltype(computation)::no_prunable_superarc_left);
        }
        WHEN("pruning with an ending condition") {
            End_with_leaf_count<decltype(computation)> ending_condition;
            computation.set_ending_condition(&ending_condition);
            computation.doIt();
            check_one_leaf_pruning(g, util.get(), __FILE__ ":" + std::to_string(__LINE__));
        }
    }
    GIVEN ("a hook shaped graph") {
        std::stringstream{hook_shaped_graph} >> BGLGraphLoader{g}.with_property_a("field", values);
        computation.set_graph(g, values);

        End_with_leaf_count<decltype(computation)> ending_condition;
        ending_condition.set_num_leaves_in_the_end(3);
        computation.set_ending_condition(&ending_condition);
        computation.doIt();
    }
}
