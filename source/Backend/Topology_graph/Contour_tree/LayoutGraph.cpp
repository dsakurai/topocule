//
// Created by Daisuke Sakurai on 2020/03/08.
//

#include <LayoutGraph.h>

void set_coords(
        tpcl::BGLGraph_list_based& g,
        const std::vector<double> &ys_vect, std::vector <std::array<double, 3>> &coords
                ) {

    // @todo use osg::Vec3Array
    std::array<double,6> original_bounds = {
            std::numeric_limits<double>::max(),
            -std::numeric_limits<double>::max(),
            std::numeric_limits<double>::max(),
            -std::numeric_limits<double>::max(),
            std::numeric_limits<double>::max(),
            -std::numeric_limits<double>::max()
    }; // x-min, x-max, y-min, y-max, z-min, z-max

    {// set coordinates
        for (auto [vi, ve] = boost::vertices(g); vi != ve; ++vi) {
            auto vid = g[*vi].pedigree_id;
            auto y = ys_vect[vid];

            if (coords.size() < vid + 1) coords.resize(vid + 1);

            // set the x coord to the vertex ID
            coords[vid] = std::array<double,3>{(double)vid,(double)y,0.0} ;

            // get the bounds of the coords
            for (int c = 0; c < 3; ++c) {
                original_bounds[2 * c]     = (coords[vid][c] < original_bounds[2 * c]    ) ? coords[vid][c] : original_bounds[2 * c];
                original_bounds[2 * c + 1] = (coords[vid][c] > original_bounds[2 * c + 1]) ? coords[vid][c] : original_bounds[2 * c + 1];
            }
        }
    }

    {// rescale the coordinates (better done with a model matrix in a graphics library)
        for (auto [vi, ve] = boost::vertices(g); vi != ve; ++vi) {
            auto vid = g[*vi].pedigree_id;
            for (int x = 0; x < 3; ++x) {
                coords[vid][x] = (coords[vid][x] - original_bounds[2 * x]) / (original_bounds[2 * x + 1] - original_bounds[2 * x]);

                // can be infinity => set to 0
                if(std::isnan(coords[vid][x]) || std::isinf(coords[vid][x])){ coords[vid][x] = 0.;}
            }
        }
    }
}
