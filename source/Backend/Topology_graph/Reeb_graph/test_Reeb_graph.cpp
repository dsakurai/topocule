//
// Created by Daisuke Sakurai on 2020-08-11.
//

#include <test.h>


#include <VTK_reader.h>

#include <Cell_connectivity_as_ttk.h>
#include <ExplicitTriangulation.h>
#include <FTRGraph.h>

#include <string>
#include <fstream>

#include <nlohmann/json.hpp>

using namespace tpcl;

TEST_CASE ("Test Reeb_graph") {

    auto multi_fields = [&]() {
        nlohmann::json j;
        std::ifstream{TRIANGLE_VTK_CONFIG} >> j;

        VTK_reader reader {j["data_file"], j["fields"]};

        reader.do_it();
        return reader.get_multi_fields();
    }();

    const int dim_2 = 2;

    auto num_cells = multi_fields->get_tessellation()->get_number_of_cells_for_dim(dim_2);

    auto cell_connectivity_as_ttk = [&]() {
        auto connectivity = multi_fields->get_tessellation()->get_cell_incidence();
        return std::make_shared<Cell_connectivity_as_ttk>(*connectivity);
    }();

#warning this from TTK fails
    ttk::ExplicitTriangulation triangulation;
    triangulation.setInputCells(
            dim_2,
            cell_connectivity_as_ttk->get_ttk_cells_connectivity()->data(),
            cell_connectivity_as_ttk->get_ttk_cells_offset()->data()
    );

//    ttk::ftr::FTRGraph<double, decltype(triangulation)> graph {&triangulation};
    // This?
#warning is this correct?
//    graph.build();
}
