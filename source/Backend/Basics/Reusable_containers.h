//
// Created by Daisuke Sakurai on 2020-08-09.
//

#ifndef TOPOCULE_REUSABLE_CONTAINERS_H
#define TOPOCULE_REUSABLE_CONTAINERS_H

namespace tpcl {

    template<class Containers>
    class Reusable_containers : public Containers {
    public:

        template<enum Containers::container_name container>
        auto &get_cleared() {
            auto &c = get_dirty<container>();
            c.clear();
            return c;
        }

        template<enum Containers::container_name container>
        auto &get_dirty() {
            return std::get<container>(*this);
        }

    };

}

#endif //TOPOCULE_REUSABLE_CONTAINERS_H
