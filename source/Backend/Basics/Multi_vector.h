//
// Created by Daisuke Sakurai on 2020-10-13.
//

#ifndef TOPOCULE_MULTI_VECTOR_H
#define TOPOCULE_MULTI_VECTOR_H

#include <vector>

namespace tpcl {
    template<int i, class T>
    struct multivector_ {
        using vector_value_type = typename multivector_<i - 1, T>::type;
        using type = std::vector <vector_value_type, std::allocator<vector_value_type>>;
    };

    template<class T>
    struct multivector_<1, T> {
        using type = std::vector <T, std::allocator<T>>;
    };

/**
 * An alias for multidimensional vector types.
 * For example, Multi_vector<2, int> is std::vector<std::vector<int>>.
 *
 * @tparam i: number of recursion
 * @tparam T: element type to be passed to the vector
 */
    template<int i, class T>
    using Multi_vector = typename multivector_<i, T>::type;
}


#endif //TOPOCULE_MULTI_VECTOR_H
