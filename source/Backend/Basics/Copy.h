
#ifndef TOPOCULE_COPY_H
#define TOPOCULE_COPY_H

#include <algorithm>
#include <array>

namespace tpcl {
    namespace basics {
        template <class Container_source, class value_type, size_t N>
        auto copy (const Container_source& source, std::array<value_type, N>& target ) {
            return std::copy(source.begin(), source.end(), target.begin());
        }
    }
}

#endif //TOPOCULE_COPY_H
