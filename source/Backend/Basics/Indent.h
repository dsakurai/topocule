//
// Created by Daisuke Sakurai on 2020-12-31.
//

#ifndef TOPOCULE_INDENT_H
#define TOPOCULE_INDENT_H

#include <ostream>

namespace tpcl {
    /**
     * ~~~
     * std::cerr << "Indent:" << std::endl;
     * std::cerr << tpcl::indent << "Hello, world!" << std::endl;
     * ++tpcl::indent;
     * std::cerr << tpcl::indent << "Hello, world!" << std::endl;
     * ~~~
     *
     * Prints
     * ~~~
     * Indent:
     *     Hello, world!
	 *         Hello, world!
     * ~~~
     *
     */
    struct Indent {
        friend std::ostream& operator<<(std::ostream &os, const Indent& indent) {
            for (int l = 0; l < indent.level; ++l) {
                os << "    ";
            }
            return os;
        }

        void operator ++() {
            ++level;
        }
        void operator --() {
            --level;
        }


    private:
        int level = 1;
    } indent;
}

#endif //TOPOCULE_INDENT_H
