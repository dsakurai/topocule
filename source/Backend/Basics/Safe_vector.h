//
// Created by Daisuke Sakurai on 2020-10-11.
//

#ifndef TOPOCULE_SAFE_VECTOR_H
#define TOPOCULE_SAFE_VECTOR_H

#include <Basics.h>
#include <vector>

namespace tpcl {
    template<class T, class M = typename std::vector<T>::allocator_type>
    class Safe_vector : public std::vector <T, M> {
    public:
        T& safe_ref(size_t pos);
    };

    template<class T, class M>
    T &Safe_vector<T, M>::safe_ref(size_t pos) {
        if ( this->size() <= pos ) TPCL_unlikely {
            this->resize(pos + 1);
        }
        return (*this)[pos];
    }
}

#endif //TOPOCULE_SAFE_VECTOR_H
