//
// Created by Daisuke Sakurai on 2020-06-06.
//

#ifndef TOPOCULE_BASICS_H
#define TOPOCULE_BASICS_H

// From TTK
#include <DataTypes.h>

#include <iosfwd>

#ifndef TPCL_cplusplus_20
// Some clang versions pretends to support C++20 when it doesn't do so in full... In that case, pass -DTPCL_cplusplus_20=(false) to the compiler to disable C++20 features.
#define TPCL_cplusplus_20 (__cplusplus >= 202002L)
#endif // TPCL_cplusplus_20

#if TPCL_cplusplus_20
#warning C++20 on GCC tends to require a specifically compiled binary, which is incompatalbe with C++17 on clang. E.g. the Boost library (graphviz) fails.
#define TPCL_concept(concept_name) concept_name
#else
#define TPCL_concept(concept_name) class
#endif

#if TPCL_cplusplus_20
#define TPCL_unlikely [[unlikely]]
#define TPCL_likely [[likely]]
#else
#define TPCL_unlikely
#define TPCL_likely
#endif

// TPCL_private is normally just the the `private` keyword.
// When entering this header during compiling a test code,
// we change it to `public` so that we can do some ad-hoc testing.
#ifdef TPCL_IS_COMPILING_TEST
#define TPCL_private public
#else
#define TPCL_private private
#endif // TPCL_IS_COMPILING_TEST

/**
 * Flag to indicate if a debug code should be inserted.
 * We use a macro and not an integer because a library
 * can be compiled with TPCL_DEBUG == false
 * while the test may be compiled with TPCL_DEBUG == true
 * All the libraries using this macro should be compiled either with or without NDEBUG
 * because at the run time the executable picks only one instance of the compiled function
 * (barring inlined instances).
 */
#if !defined(NDEBUG)
#define TPCL_DEBUG (true)
#else
#define TPCL_DEBUG (false)
#endif

#define TPCL_STR_HELPER(x) #x
#define TPCL_STR(x) TPCL_STR_HELPER(x)

#define TPCL_ERROR(message) std::string(message) + "\t" + std::string(__FILE__) + ":" + TPCL_STR(__LINE__)

namespace tpcl {
    using Cell_id = ttk::SimplexId;
    using Cell_id_long = ttk::LongSimplexId;


    /**
     *  Just a constant integer to enhance readability.
     *  If necessary, code using this template can use bare int.
     *
     *  ~~~
     *  // This
     *  constexpr int x = 0;
     *  int a_dim = dim<x>;
     *
     *  // can be replaced with dynamical insertion without `dim`.
     *  int y = 0;
     *  int b_dim = y;
     *  ~~~
     *
     * @tparam d: dimension
     *
     * Usage: dim<3>
     */
    template <int d>
    constexpr int dim = d;

    /**
     *  Just a constant integer to avoid magic numbers.
     * @tparam c: cell id
     *
     * Usage: cell<3>
     */
    template <int c>
    const Cell_id cell = c;

    /**
     *  Just a constant integer to avoid magic numbers.
     *  Note that a vertex is also a cell in this library.
     * @tparam v: vertex id
     *
     * Usage: vertex<3>
     */
    template <int v>
    const Cell_id vertex = v;
}

#endif //TOPOCULE_BASICS_H
