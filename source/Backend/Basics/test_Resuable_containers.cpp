//
// Created by Daisuke Sakurai on 2020-08-09.
//

#include <test.h>
#include <Reusable_containers.h>

#include <tuple>
#include <vector>

struct Containers: public std::tuple<
        std::vector<int>,
        std::vector<double>
> {
    enum container_name {
        integers,
        doubles
    };
};

TEST_CASE ("Test Reusable_containers") {
    using namespace tpcl;

    Reusable_containers <Containers> containers;
    {
        INFO("Initializing a container");
        auto &i = containers.get_cleared<Containers::integers>();
        i = {0};
        CHECK(i.size() == 1);
    }

    {
        INFO("Adding an element in a container");
        auto &i = containers.get_dirty<Containers::integers>();
        i.emplace_back(1);
        CHECK(i.size() == 2);
    }

    {
        INFO("Re-initalizing a container");
        auto &i = containers.get_cleared<Containers::integers>();
        CHECK(i.size() == 0);
    }

    {
        auto &d = containers.get_dirty<Containers::doubles>();
        INFO("Accessing a different container");
        CHECK(d.size() == 0);
    }
}
