//
// Created by Daisuke Sakurai on 2020-10-28.
//

#ifndef TOPOCULE_TYPE_TRAITS_H
#define TOPOCULE_TYPE_TRAITS_H

#include <type_traits>

/**
 * Enable a template class / function / etc.
 * See also `std::enable_if`.
 */
#define TPCL_enable_if(condition) typename = std::enable_if_t<condition>

#endif //TOPOCULE_TYPE_TRAITS_H
