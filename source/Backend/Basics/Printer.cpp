//
// Created by Daisuke Sakurai on 2019-09-15.
//

#include <Printer.h>

std::string tpcl::error_string(const std::string &message, const char *file, int line) {
    return "Error: " + message + "\t" + file + ":" + std::to_string(line);
}

void tpcl::print_error(const std::string &message, const char *file, int line) {
    std::cerr << error_string(message, file, line) << std::endl;
}
