//
// Created by Daisuke Sakurai on 2019-09-15.
//

#ifndef TOPO_MOLECULE_STATES_PRINTER_H
#define TOPO_MOLECULE_STATES_PRINTER_H

#include <string>
#include <iostream>

namespace tpcl {

    std::string error_string(const std::string &message, const char *file, int line);

    void print_error(const std::string &message, const char *file, int line);

}

#define ERROR_MESSAGE(message) tpcl::print_error(message, __FILE__, __LINE__);
#define THROW_MESSAGE(message) throw std::runtime_error{tpcl::error_string(message, __FILE__, __LINE__)};

#endif //TOPO_MOLECULE_STATES_PRINTER_H
