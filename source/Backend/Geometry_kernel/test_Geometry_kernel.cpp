//
// Created by Daisuke Sakurai on 2020-12-30.
//

#include <test.h>

#include <Geometry_kernel.h>
#include <Convex_hull.h>

TEST_CASE ("Test Geometry_kernel") {

    using namespace tpcl;

    // SoS kernel
//    using Kernel = CGAL::Exact_predicates_exact_constructions_kernel;
    using Kernel = geometry::List_CGAL_implementation_kernel<typename geometry::CGAL_base_classes>;
//    using Kernel = geometry::Kernel<typename geometry::CGAL_base_classes>;

    constexpr auto create_point = [](double x, double y, Cell_id i) {
        using Point_2 = typename Kernel::Point_2;
        if constexpr (
                std::is_same_v<Point_2, geometry::CGAL_base_classes::Point_2>
                ){
            return Point_2{x, y};
        } else {
            return Point_2{x, y, i};
        }
    };

#ifdef CONVEX_HULL
    SUBCASE ("With convex hull") {
        using Point_2 = typename Kernel::Point_2;

        std::vector<Point_2> points = {
                // Inside
                create_point(0.1, 0.1, 0),

                // hull

                create_point(0.0, 0.0, 2),
                create_point(0.0, 0.0, 3),
                //
                create_point(1.0, 0.0, 1),
                create_point(0.0, 1.0, 4)
        };

        std::vector<Point_2> expected = {
                create_point(0, 0, 2),
                create_point(1, 0, 1),
                create_point(0, 1, 4),
                create_point(0, 0, 3)
        };

        std::vector<Point_2> convex_hull;
        tpcl::geometry::convex_hull_graham_andrew(points.begin(),
                               points.end(),
                               std::back_inserter(convex_hull),
                               Kernel{}
        );

        CHECK(convex_hull == expected);
    }
#endif // CONVEX_HULL

    SUBCASE("With arrangement") {

        // CGAL Arrangement book page 71

        using Number_type = typename Kernel::FT;
//        using Arrangement_traits = tpcl::geometry::Arr_linear_traits_2<Kernel>;
//        using Arrangement_traits = CGAL::Arr_linear_traits_2<Kernel>;
        using Arrangement_traits = CGAL::Arr_segment_traits_2<Kernel>;
        using Point_2 = typename Arrangement_traits::Point_2;
        using Segment_2 = typename Arrangement_traits::Segment_2;
        using Line_2 = typename Arrangement_traits::Line_2;

        using Arrangement = CGAL::Arrangement_2<Arrangement_traits>;

//        using Vertex_handle = Arrangement::Vertex_handle;
//        using Halfedge_handle = Arrangement::Halfedge_handle;
//        using Face_handle = Arrangement::Face_handle;

        WHEN("Degenerate: meet at 1 point"){
            std::vector<Segment_2> cv = {
                    Segment_2{
                            create_point(-1.0, 0.0, 0),
                            create_point(1.0,  0.0, 1),
                    },
                    Segment_2{
                            create_point(0.0, -1.0, 2),
                            create_point(0.0,  1.0, 3),
                    },
                    Segment_2{
                            create_point(-1.0, -1.0, 4),
                            create_point(1.0,  1.0, 5),
                    },
            };

            Arrangement arr;
            CGAL::insert(arr, cv.begin(), cv.end());
            CHECK(arr.number_of_vertices() == 7);
        }
        WHEN("Non-degenerate: meet at 3 points") {
            std::vector<Segment_2> cv = {
                    Segment_2{
                            create_point(-1.0,  0.0, 0),
                            create_point( 1.0,  0.0, 1),
                    },
                    Segment_2{
                            create_point( 0.0, -1.0, 2),
                            create_point( 0.0,  1.0, 3),
                    },
                    Segment_2{
                            create_point( 1.0, 1.0, 4),
                            create_point(-1.0,-1.0, 5),
                    },
            };

            Arrangement arr;
            CGAL::insert(arr, cv.begin(), cv.end());
                    CHECK(arr.number_of_vertices() == 7);
        }
    }
}
