//
// Created by Daisuke Sakurai on 2020-12-30.
//

#ifndef TOPOCULE_GEOMETRY_KERNEL_H
#define TOPOCULE_GEOMETRY_KERNEL_H

#include <Basics.h>

#include <Simulation_of_simplicity.h>
#include <CGAL/Point_2.h>

#include <Arrangement.h>

#include <ostream>

namespace tpcl {
    namespace geometry {

        /**
         * We delegate many operations on geometry to CGAL's built-in types.
         * We collect such types here and pass it to topocule's
         * classes using the traits pattern.
         */
        class CGAL_base_classes {
        public:
            using Kernel  = CGAL::Exact_predicates_exact_constructions_kernel;
            using Point_2 = CGAL::Point_2<Kernel>;
            using Line_2  = CGAL::Line_2<Kernel>;
            using Segment_2  = CGAL::Segment_2<Kernel>;
            using Ray_2  = CGAL::Ray_2<Kernel>;
        };

        /**
         * Don't confuse the ID held in this class with that of
         * the CGAL Point_2, which is actually a pointer.
         * It even has `Point_2::id()`.
         *
         * Actually we could replace our ID in this class with that one,
         * but doing so would use a point ID different
         * from topocule, which is passed to TTK.
         * Better stay consistent and use topocule's ID.
         *
         * @tparam CGAL_base_classes
         */
        template<class CGAL_base_classes>
        class tpcl_Point_2: public CGAL_base_classes::Point_2 {
        public:

            using Superclass = typename CGAL_base_classes::Point_2;
            tpcl_Point_2 () : Superclass{} {}

            explicit
            tpcl_Point_2 (double x, double y, tpcl::Cell_id id):
                    Superclass {x, y},
                    id {id}
            {}

            tpcl_Point_2 (double x, double y):
                Superclass {x, y}
            {}

            tpcl::Cell_id get_id() const {
                return id;
            }

            static constexpr tpcl::Cell_id UNSET = -1;

            friend std::ostream& operator<<(std::ostream& o, const tpcl_Point_2& p) {
                return o << "{" << p.x() << ", " << p.y() << " (id: " << p.get_id() << ")}" ;
            }
        private:
            tpcl::Cell_id id = UNSET;
        };

        template<class CGAL_base_classes>
        class tpcl_Line_2: public CGAL_base_classes::Line_2 {
        public:
            using Point_2 = tpcl_Point_2<CGAL_base_classes>;
        private:
            Cell_id point_p_id = Point_2::UNSET;
            Cell_id point_q_id = Point_2::UNSET;
        };

        template<class CGAL_base_classes>
        class tpcl_Segment_2: public CGAL_base_classes::Segment_2 {
        public:
            using Point_2 = tpcl_Point_2<CGAL_base_classes>;

            tpcl_Segment_2 (const Point_2& p, const Point_2& q):
                    point_p_id {p.get_id()},
                    point_q_id {q.get_id()}
            {}
        private:
            Cell_id point_p_id = Point_2::UNSET;
            Cell_id point_q_id = Point_2::UNSET;
        };

        template<class CGAL_base_classes>
        class tpcl_Ray_2: public CGAL_base_classes::Ray_2 {
        public:
            using Point_2 = tpcl_Point_2<CGAL_base_classes>;
        private:
            Cell_id point_id = Point_2::UNSET;
        };

#define TPCL_USING(NAME) using Superclass::NAME;

        /**
         * Useful for listing what is used by geometry algorithms
         */
        template <class CGAL_base_classes>
        class List_CGAL_implementation_kernel: private CGAL::Exact_predicates_exact_constructions_kernel {
        public:
            using Superclass = CGAL::Exact_predicates_exact_constructions_kernel;
            using FT = Superclass::FT;

            // Borrow classes from the CGAL kernel
            TPCL_USING(Point_2);
            TPCL_USING(Line_2);
            TPCL_USING(Segment_2);
//            TPCL_USING(Ray_2);

            // DONE
            // Used for convex hulls
            TPCL_USING(equal_2_object);
            TPCL_USING(Equal_2);
            TPCL_USING(Less_xy_2);
            TPCL_USING(less_xy_2_object);
            TPCL_USING(Left_turn_2);
            TPCL_USING(left_turn_2_object);

            // TODO
            TPCL_USING(orientation_2_object);
            TPCL_USING(compare_x_2_object);
            TPCL_USING(compare_xy_2_object);
            TPCL_USING(compare_y_2_object);
            TPCL_USING(construct_vertex_2_object);
            TPCL_USING(construct_line_2_object);
            TPCL_USING(is_vertical_2_object);
            TPCL_USING(compare_slope_2_object);
            TPCL_USING(intersect_2_object);
            TPCL_USING(has_on_2_object);

            TPCL_USING(Compare_y_2);
            TPCL_USING(Compare_x_2);
            TPCL_USING(Compare_xy_2);
        };

        /**
         * Override the CGAL kernel to support simulation of simplicity
         *
         * @tparam CGAL_base_classes
         */
        template <class CGAL_base_classes>
        class Kernel :  public List_CGAL_implementation_kernel<CGAL_base_classes>{

        public:
            // Instead of CGAL's Point_2 etc., use topocule's custom Point_2 etc.
            using Point_2 = tpcl::geometry::tpcl_Point_2<CGAL_base_classes>;
            using Line_2  = tpcl::geometry::tpcl_Line_2<CGAL_base_classes>;
            using Ray_2   = tpcl::geometry::tpcl_Ray_2<CGAL_base_classes>;
            using Segment_2   = tpcl::geometry::tpcl_Segment_2<CGAL_base_classes>;

            // Used for simulation of simplicity
            constexpr static Convention Sos_convention = VISUALIZATION;

            struct Equal_2 {
                using Point_2 = Point_2;
                bool operator () (const Point_2& a, const Point_2& b) const {
                    return a.get_id() == b.get_id();
                }
            };

            Equal_2 equal_2_object() const {
                return Equal_2 {};
            }

            struct Less_xy_2 {
                bool operator() (const Point_2& p, const Point_2& q) const {
                    using coord_type = typename std::remove_reference<decltype(p.x())>::type;
                    // We only need x because simulation of simplicity guarantees that no
                    // two points share the same x coordinates.
                    std::array<coord_type, 2> pis = {
                            p.x(),
                            q.x()
                    };
                    std::array<Cell_id, 2> ids = {p.get_id(), q.get_id()};
                    return (sign_determinant_lambda<Sos_convention>(
                            pis,
                            ids
                            ) == Sign_of_determinant::NEGATIVE);

                    // Must be identical to
//                    if (p_x != q_x) {
//                        return p_x < q_x;
//                    }
//
//                    using Comparator = details::Comparator_type<Sos_convention>;
//                    return Comparator{}(q.get_id(), /*>*/ p.get_id());
                }
            };

            Less_xy_2 less_xy_2_object() const {
                return Less_xy_2{};
            }

            struct Left_turn_2 {
                bool operator() (const Point_2& p, const Point_2& q,  const Point_2& r) {
                    std::array<decltype(p.x()), 2*3> coords = {p.x(), p.y(), q.x(), q.y(), r.x(), r.y()};
                    std::array<Cell_id, 3> ids = {p.get_id(), q.get_id(), r.get_id()};

                    bool out = geometry::sign_determinant_lambda<Sos_convention>(coords, ids) == tpcl::geometry::POSITIVE;
                    return out;
                }
            };

            Left_turn_2 left_turn_2_object() const {
                return Left_turn_2{};
            }

#define TPCL_DUMMY_PREDICATE(NAME)                          \
            struct NAME {                                   \
                template<class... Args>                     \
                CGAL::Sign operator()(Args... args) {       \
                    THROW_MESSAGE("Call to NAME"); \
                    return CGAL::POSITIVE;                  \
                }                                           \
            };


#define TPCL_PROVIDE(api,CLASS)\
    auto api() const {         \
        return CLASS{} ;        \
    }
#define TPCL_EXPAND(n,N,ame)       \
    auto n##ame##_object() const { \
        return N##ame{};           \
    }

#define TPCL_EXPAND_DUMMY(n,N,ame,ret)                  \
    struct N##ame {                                 \
        template<class... Args>                     \
        ret operator()(Args... args) {       \
            THROW_MESSAGE("Call to N##ame");        \
            return ret{};                  \
        }                                           \
    };                                              \
    auto n##ame##_object() const {                  \
        return N##ame{};                            \
    }
            TPCL_EXPAND_DUMMY(o,O,rientation_2, CGAL::Sign);
            TPCL_EXPAND_DUMMY(c,C,ompare_x_2,CGAL::Sign);
            TPCL_EXPAND_DUMMY(c,C,ompare_xy_2,CGAL::Sign);
//            TPCL_EXPAND_DUMMY(c,C,ompare_y_2,CGAL::Sign);
//            TPCL_EXPAND_DUMMY(c,C,ompare_slope_2,CGAL::Sign);
//            TPCL_EXPAND_DUMMY(i,I,s_degenerate_2,bool);
//            TPCL_EXPAND_DUMMY(i,I,s_vertical_2,bool);
//            TPCL_EXPAND_DUMMY(i,I,s_horizontal_2,bool);
//            TPCL_EXPAND_DUMMY(i,I,ntersect_2,bool);
//            TPCL_EXPAND_DUMMY(h,H,as_on_2,bool);
//            TPCL_EXPAND_DUMMY(c,C,onstruct_point_2,Point_2);
//            TPCL_EXPAND_DUMMY(c,C,onstruct_vertex_2, Point_2);
//            TPCL_EXPAND_DUMMY(c,C,onstruct_line_2, Line_2);
//            TPCL_EXPAND_DUMMY(c,C,onstruct_opposite_line_2, Line_2);

        };

    }
}

#endif //TOPOCULE_GEOMETRY_KERNEL_H
