//
// Created by Daisuke Sakurai on 2020-10-20.
//

#ifndef TOPOCULE_SIMULATION_OF_SIMPLICITY_H
#define TOPOCULE_SIMULATION_OF_SIMPLICITY_H

#include <Basics.h>
#include <Type_traits.h>

// @todo Remove CGAL includes from header files because it can conflict with user programs!
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/predicates/sign_of_determinant.h>
#include <CGAL/Point_2.h>
#include <CGAL/Triangle_2.h>

/**
 * This module is for determinant computation specialized for Simulation of Simplicity.
 */
namespace tpcl {
    namespace geometry {

        /**
         * The actual values s.a. Positive == 1 is necessary for computing determinants.
         *
         */
        enum Sign_of_determinant : int /* We set the underlying type to int because we do arithmetics with enum. */ {
            POSITIVE = +1,
            ZERO = 0,
            NEGATIVE = -1
        };

        enum Convention {
            ORIGINAL = 0,
            VISUALIZATION = 1
        };

        namespace details {
            using Comparator_types = std::tuple<std::less<tpcl::Cell_id>, std::greater<tpcl::Cell_id>>;

            template <Convention convention = VISUALIZATION>
            using Comparator_type = decltype(std::get<convention>(details::Comparator_types{}));
        }

        namespace details {

            template<int Report_depth = 0>
            struct Test_trait { // NOLINT Doesn't have to be used
                constexpr static int report_depth = Report_depth;
            };

            ///@{
            /**
             * Matrix size for the determinant of the Cartesian.
             * The matrix is of size #vertices x #coordinates, which is actually
             * #vertices x (#vertices - 1).
             *
             * Note that the original matrix as defined by H Edelsbrunner and E Mucke had an additional column
             * filled by 1s at the end.
             */
            template<class Size>
            constexpr Size column_size_barring_last_column(Size num_vertices) {
                return num_vertices - 1;
            }

            //
            template<class Size>
            constexpr Size matrix_size_barring_last_column(Size num_vertices) {
                return num_vertices * column_size_barring_last_column(num_vertices);
            }
            ///@}

            template<class Func, typename Array, std::size_t... I>
            auto array_2_func_arguments(const Func &function, const Array &a, std::index_sequence<I...>) {
                return function(a[I]...);
            }

            /**
             * Expand array into function arguments
             *
             * @tparam Func
             * @tparam Coord
             * @tparam N
             * @param function
             * @param a
             * @return
             */
            template<class Coord, std::size_t N, class Func>
            auto array_2_func_arguments(const Func &function, const Coord *a) {
                using Indices = std::make_index_sequence<N>;
                return array_2_func_arguments(function, a, Indices{});
            }

            /**
             * We delegate the sign computation to CGAL, which returns its own enum type.
             *
             * There should be no const in performance for converting the enum type into ours because CGAL's enum
             * is internally an int, which is the same as our `Sign_of_determinant`.
             * We verify this in what follows.
             *
             * Given this CGAL sign enum, (which should be CGAL::Sign but is unclear from the description in the CGAL
             * header)
             * @tparam Sign_enum
             * @return
             */
            template<class Sign_enum>
            constexpr void verify_enum_compatability() {

                // Check the compatability
                static_assert((int) Sign_enum::POSITIVE == (int) Sign_of_determinant::POSITIVE);
                static_assert((int) Sign_enum::NEGATIVE == (int) Sign_of_determinant::NEGATIVE);
                static_assert((int) Sign_enum::ZERO == (int) Sign_of_determinant::ZERO);
                // And verify the type casting overhead
                static_assert(
                        std::is_same_v<std::underlying_type_t<Sign_enum>, std::underlying_type_t<Sign_of_determinant>>);
            }

            //@{
            /**
             * Various forms of computing determinants.
             */
            /**
             * Generic dimensionalities
             *
             * @performance `CGAL::sign_of_determinant` should be replaced with CGAL's filtered kernels, which
             * are faster.
             * See also:
             * - CGAL Documentation on filtered predicates https://doc.cgal.org/latest/Kernel_23/classCGAL_1_1Filtered__predicate.html
             * - Brönnimann et al. on exact geometry computation : https://hal.inria.fr/inria-00344281/document (Interval Arithmetic Yields Efficient Dynamic Filters for Computational Geometry)
             *
             * @tparam Args
             * @param args
             * @return
             */
            template<class... Args>
            Sign_of_determinant compute_sign_of_determinant(Args &&... args) {
                auto sign = CGAL::sign_of_determinant(args...);

                return (Sign_of_determinant) sign;

                // static sanity check using template metaprogramming
                verify_enum_compatability<decltype(sign)>();
            }
            /**
             * Specialize to 1-dim
             * @tparam Args
             * @param args
             * @return
             */
            template<class Coord, TPCL_enable_if(! std::is_pointer_v<Coord>) >
            Sign_of_determinant compute_sign_of_determinant(const Coord arg) {
                if (arg == 0) return Sign_of_determinant::ZERO;
                return std::signbit(arg) ? Sign_of_determinant::NEGATIVE : Sign_of_determinant::POSITIVE;
            }
            //@}
            /**
             * This is identical to `compute_sign_of_determinant`.
             * The semantics of barring_last_column of the matrix does not
             * mean anything because a 1-D matrix does not have the last column filled with 1.
             *
             * Specialize to 1-dim
             */
            template<class Coord, TPCL_enable_if(! std::is_pointer_v<Coord>)>
            Sign_of_determinant compute_sign_of_determinant_barring_last_column(const Coord arg) {
                // barring the last column has no effect for a 1-D square matrix (which has a single element)
                return compute_sign_of_determinant(arg);
            }
            /**
             * Compute the determinant of the input matrix.
             *
             * | pi_i1  pi_i2  1 |
             * | pi_j1  pi_j2  1 |
             * | pi_k1  pi_k2  1 |
             *
             * The actual arguments do not include the last column of the matrix,
             * whose elements are all 1.
             *
             * Specialize to 2-dim
             * @tparam Args
             * @param args
             * @return
             */
            template<class Coord, TPCL_enable_if(! std::is_pointer_v<Coord>)>
            Sign_of_determinant compute_sign_of_determinant_barring_last_column(
                    const Coord pi_i1, const Coord pi_i2,
                    const Coord pi_j1, const Coord pi_j2,
                    const Coord pi_k1, const Coord pi_k2
            ) {
                using Kernel = CGAL::Exact_predicates_exact_constructions_kernel;
                auto sign = CGAL::Triangle_2<Kernel> {
                        CGAL::Point_2<Kernel>{pi_i1, pi_i2},
                        CGAL::Point_2<Kernel>{pi_j1, pi_j2},
                        CGAL::Point_2<Kernel>{pi_k1, pi_k2}
                }.orientation();

                // As of CGAL 5.0, the above tries a simple IEEE double float computation for speed.
                // If the sign of the determinant cannot be decided,
                // it proceeds to `CGAL::Static_filtered_predicate` to do interval arithmetic.
                //
                // See also:
                // - CGAL Documentation on filtered predicates https://doc.cgal.org/latest/Kernel_23/classCGAL_1_1Filtered__predicate.html
                // - Brönnimann et al. on exact geometry computation : https://hal.inria.fr/inria-00344281/document (Interval Arithmetic Yields Efficient Dynamic Filters for Computational Geometry)

                return (Sign_of_determinant) sign;

                // static sanity check using template metaprogramming
                verify_enum_compatability<decltype(sign)>();
            }

            /**
             * | pi_i1  pi_i2  1 |
             * | pi_j1  pi_j2  1 |
             * | pi_k1  pi_k2  1 |
             *
             * @tparam Coord
             * @tparam num_matrix_elements
             * @tparam barring_last_column
             * @param numbers
             * @return
             */
            template<class Coord, size_t num_matrix_elements, int barring_last_column = 0>
            Sign_of_determinant
            sign_of_determinant(const Coord *numbers) {
                auto check_sign = [](auto... args) {
                    if constexpr (barring_last_column)
                        return compute_sign_of_determinant_barring_last_column(args...);
                    else
                        return compute_sign_of_determinant(args...);
                };
                return details::array_2_func_arguments<Coord, num_matrix_elements>(check_sign, numbers);
            }

            // Expand the std::array into function arguments
            template<class Coord, size_t num_matrix_elements, int barring_last_column = 0>
            Sign_of_determinant
            sign_of_determinant(const std::array<Coord, num_matrix_elements> &coordinates) {
                return sign_of_determinant<Coord, num_matrix_elements,barring_last_column>(coordinates.data());
            }

            template <class Non_void>
            constexpr bool trigger_false () {
                return std::is_same<Non_void, void>::value;
            }

            template<class Dummy = int>
            constexpr void not_implemented() {
                static_assert(trigger_false<Dummy>(), "Not implemented! You may need to implement the caller of this function.");
            }

            template<int depth>
            class Depth  : public std::runtime_error {
                public:
                    Depth () : std::runtime_error {"Depth " + std::to_string(depth)}
                    {}
            };
            ///@{
            /**
             * This API computes the sign for the determinant of the lambda matrix
             * as specified in
             *
             * Presumptions: pi are sorted according to the indices
             * In the original paper by H Edelsbrunner and E Mucke the sorting must was said to be in the ascending order.
             * It should, however, be backwards to follow the convention in contour tree computation.
             *
             * The API has an optional template parameter which reports the "depth" of the degeneracy.
             */
            //
            // Dummy implementation to notify about an unimplemented case
            template<class Dummy, class... Args, class Test = Test_trait<>>
            Sign_of_determinant sign_determinant_lambda_for_epsilons(
                    Dummy,
                    Args...
            ) {
                not_implemented<Dummy>();
            }
            template<class Coord, class Test = Test_trait<>>
            Sign_of_determinant sign_determinant_lambda_for_epsilons(
                    const Coord pi_i1,
                    const Coord pi_i2,
                    const Coord pi_j1,
                    const Coord pi_j2,
                    const Coord pi_k1,
                    const Coord pi_k2
            ) {
                const Coord pi_i3 = 1;
                const Coord pi_j3 = 1;
                const Coord pi_k3 = 1;

                //-------------------------------------------
                // Fix the vertex ID to i, decrease the dimension

                // coefficient of epsilon(i, 3)
                // not used for the lambda matrix

                // coefficient of epsilon(i, 2)
                if (auto nonzero = compute_sign_of_determinant(
                        pi_j1, pi_j3,
                        pi_k1, pi_k3
                )) {
                    if constexpr (Test::report_depth) throw details::Depth<1>{};
                    return (Sign_of_determinant) - nonzero ;
                }

                // coefficient of epsilon(i, 1)
                if (auto nonzero = compute_sign_of_determinant(
                        pi_j2, pi_j3,
                        pi_k2, pi_k3
                )) {
                    if constexpr (Test::report_depth) throw details::Depth<2>{};
                    return nonzero;
                }

                //-------------------------------------------
                // Decrease the vertex ID, decrease the dimension

                // coefficient of epsilon(j, 3)
                // coefficient of epsilon((j, 3), (i, 2))
                // coefficient of epsilon((j, 3), (i, 1))
                // not used for the lambda matrix

                // coefficient of epsilon(j, 2)
                if (auto nonzero = compute_sign_of_determinant(
                        pi_i1, pi_i3,
                        pi_k1, pi_k3
                )) {
                    if constexpr (Test::report_depth) throw details::Depth<3>{};
                    return nonzero;
                }

                // coefficient of epsilon((j, 2), (i, 1)):
                // sign of pi_k3, which is 1.
                if constexpr (Test::report_depth) throw details::Depth<4>{};
                return Sign_of_determinant::POSITIVE;
            }
            //
            template<class Coord, class Test = Test_trait<>, class Dummy = int>
            Sign_of_determinant sign_determinant_lambda_for_epsilons(
                    const Coord pi_i1,
                    // and pi_i2 == 1
                    const Coord pi_j1
                    // and pi_j2 == 1
            ) {
                if constexpr (Test::report_depth) throw details::Depth<1>{}; // NOLINT no need to derive from std::exception
                return Sign_of_determinant::POSITIVE;
#ifndef TPCL_IS_COMPILING_TEST
                static_assert(trigger_false<Dummy>(), "This case implementation may be used only for testing and debugging because there's a faster alternative in `sign_determinant_lambda`.");
#endif  // Note: this SHOULD be a macro to make this check independent from header inclusion order.
            }
            ///@}

            /**
             * This class is tailor-made for Simulation of Simplicity;
             * We sort the point IDs together with the vertex coordinates.
             * The vertex coordinates are in Cartesian.
             * Therefore, the number of coordinates are one less than that of the vertices.
             *
             * @performance Sorting takes O(N^2) steps due to the insertion sort algorithm.
             * This cost applies not only for ID sorting (which is mandatory for the insertion sort)
             * but also for the vertex coordinates in the current implementation.
             * This is left so because the number of items is very small, which is identical to the
             * number of vertices. In a 2-D space N = 3.
             * The sorting operation can actually be faster than other sorting algorithms due to the low overhead
             * thanks to the simplicity of the insertion sort.
             *
             * The class also reports whether the count of swaps is odd or even.
             *
             * This class should stay simple enough so that it can be inlined.
             */
            template<
                    class Comparator,
                    class Increment_swap_count,
                    int max_dim = 32
            >
            class Sort_with_swap_even {
            public:
                Sort_with_swap_even(
                        Cell_id num_vertices,
                        const Increment_swap_count &increment_swap_count
                ) :
                        num_vertex_ids{num_vertices},
                        num_coordinates{column_size_barring_last_column(num_vertices)},
                        increment_swap_count{increment_swap_count} {
                    if (num_vertices > max_dim) throw std::runtime_error{TPCL_ERROR("More IDs than specified!")};
                }

                /**
                 * We choose insertion sort due to its low overhead.
                 */
                template<class Coord>
                inline void operator()(Cell_id *__restrict__ ids, Coord *__restrict__ pis) {
                    insertion_sort(ids, pis);
                }

            private:

                template<class Coord>
                void overwrite_row(Coord *__restrict__ pis, Coord *__restrict__ in) {
                    // overwrite columns
                    for (Cell_id c = 0; c < num_coordinates; ++c) {
                        pis[c] = in[c];
                    }
                }

                template<class Coord>
                void shift(Cell_id *__restrict__ ids, Coord *__restrict__ pis, Cell_id r) {
                    Cell_id id_on_right = ids[r];

                    std::array<Coord, max_dim> pi_on_right;
                    overwrite_row(pi_on_right.data(), pis + num_coordinates * r);

                    int l = r - 1;

                    while (l >= 0 && Comparator{}(id_on_right, ids[l])) {
                        // sort a 1-D array
                        ids[l + 1] = ids[l];
                        // sort a matrix
                        overwrite_row(pis + num_coordinates * (l + 1), pis + num_coordinates * l);
                        increment_swap_count();
                        --l;
                    }

                    ids[l + 1] = id_on_right;
                    overwrite_row(pis + num_coordinates * (l + 1), pi_on_right.data());
                }

                template<class Coord>
                void insertion_sort(Cell_id *__restrict__ ids, Coord *__restrict__ pis) {
                    for (Cell_id r = 1; r < num_vertex_ids; ++r) {
                        shift(ids, pis, r);
                    }
                }

            protected:
                const Cell_id num_vertex_ids;
                const Cell_id num_coordinates;
                const Increment_swap_count increment_swap_count;
            };

            template<class Coord, size_t num_matrix_elements, class Test = Test_trait<>>
            Sign_of_determinant perturbed_sign_determinant_lambda(
                    const Coord *pis
            ) {
                // Select the appropriate function for computing the determinant

                auto check_sign = [&pis](auto... args) {
                    return details::sign_determinant_lambda_for_epsilons<Coord, Test>(args...);
                };

                return details::array_2_func_arguments<Coord, num_matrix_elements>(check_sign, pis);
            }

            template<class Comparator, class Coord, size_t num_vertices>
            bool sort_and_report_swaps(Cell_id *__restrict__ ids, Coord *__restrict__ pis) {
                bool is_swap_even = true;

                auto increment_swap_count = [&is_swap_even]() {
                    is_swap_even = !is_swap_even;
                };

                // We usually pass >, i.e. greater, instead of < because contour tree algorithm assumed the opposite of
                // the SoS.
                details::Sort_with_swap_even<Comparator, decltype(increment_swap_count), num_vertices>{num_vertices,
                                                                                                       increment_swap_count}(
                        ids, pis);

                return is_swap_even;
            }

            template<class Comparator, class Coord, size_t num_vertices>
            bool sort_and_report_swaps(
                    std::array<Cell_id, num_vertices> &ids,
                    std::array<Coord, matrix_size_barring_last_column(num_vertices)> &pis
            ) {
                return sort_and_report_swaps<Comparator, Coord, num_vertices>(ids.data(), pis.data());
            }
        }

        ///@{
        /**
         * Note: this function may sort the elements in the input arrays.
         * For the scalar case, i.e. num_vertices == 2, however, the sort does not happen in production
         * because there's no need.
         *
         * In contour tree / Reeb graph algorithms, the ordering of perturbation
         * is opposite to that of the Simulation of Simplicity (SoS) by Herbert Edelsbrunner.
         * By default, this method follows the convention in visualization.
         * which inverts the vertex sorting. To follow the original SoS, change it to
         * `std::less<Cell_int>`.
         *
         * @tparam Coord
         * @tparam sz
         * @tparam convention: Should be VISUALIZATION, which respects the convention in visualization. ORIGINAL follows that in the original article on simulation of simplicity.
         * @param pis
         * @return
         */
        template<class Coord, size_t num_vertices, Convention convention = VISUALIZATION, class Test = details::Test_trait<>>
        Sign_of_determinant sign_determinant_lambda(
                const Coord *__restrict__ pis,
                const Cell_id *__restrict__ ids
        ) {
            if constexpr (num_vertices == 2) {
                // This is just the classical case of comparing pointer addresses after comparing the scalar values

                // No need of perturbation?
                // The determinant is pis[0] - pis[1].
                //
                // In fact, the matrix is
                // | pis[0] 1 |
                // | pis[1] 1 |.
                //
                if (pis[0] != pis[1])
                    return (pis[0] > pis[1]) ? Sign_of_determinant::POSITIVE : Sign_of_determinant::NEGATIVE;

                // Need of perturbation.
                // The determinant is ids[0] - ids[1] in the convention in visualization
                if constexpr (Test::report_depth) throw details::Depth<1>{};

                using Comparator = details::Comparator_type<convention>;
                return (Comparator{}(ids[0], /* > */ ids[1])) ? Sign_of_determinant::POSITIVE
                                                              : Sign_of_determinant::NEGATIVE;
            } else {
                // First, compute the coordinates without perturbation

                if (Sign_of_determinant nonzero = details::sign_of_determinant<
                        Coord,
                        details::matrix_size_barring_last_column(num_vertices),
                        /*Omit the last column of the matrix*/ 1
                        >(pis))
                        TPCL_likely return nonzero;

                constexpr auto num_coords = num_vertices - 1;

                std::array<Cell_id, num_vertices> ids_sorted;
                std::array<Coord, num_coords * num_vertices> pis_sorted;
                std::copy(ids, ids + num_vertices, ids_sorted.data());
                std::copy(pis, pis + (num_coords * num_vertices), pis_sorted.data());

                using Comparator = details::Comparator_type<convention>;
                bool swap_even = details::sort_and_report_swaps<Comparator, Coord, num_vertices>(ids_sorted.data(), pis_sorted.data());

                const auto swap_sign = (swap_even) ? Sign_of_determinant::POSITIVE : Sign_of_determinant::NEGATIVE;

                return (Sign_of_determinant) (swap_sign *
                                              details::perturbed_sign_determinant_lambda<
                                                      Coord,
                                                      details::matrix_size_barring_last_column(num_vertices), Test > (
                                                      pis )
                );
            }
        }

        /**
         * The template parameters are shuffled from the function with the same name
         * to change the comparator easily.
         *
         * @tparam convention: Should be VISUALIZATION, which respects the convention in visualization. ORIGINAL follows that in the original article on simulation of simplicity.
         * @tparam Coord
         * @tparam num_vertices
         * @tparam Test: Unimportant for endusers. Just for testing
         * @param pi
         * @param ids
         * @return
         */
        template<Convention convention = VISUALIZATION, class Coord = double, size_t num_vertices = 3, class Test = details::Test_trait<>>
        Sign_of_determinant sign_determinant_lambda(
                const std::array<Coord, details::matrix_size_barring_last_column(num_vertices)> &pi,
                const std::array<Cell_id, num_vertices> &ids
        ) {
            return sign_determinant_lambda<
                    Coord,
                    num_vertices,
                    convention,
                    Test
            >(
                    pi.data(),
                    ids.data()
            );
        }
        ///@}

    }
} // namespace tpcl

#endif //TOPOCULE_SIMULATION_OF_SIMPLICITY_H
