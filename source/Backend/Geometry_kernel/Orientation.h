
#ifndef TOPOCULE_ORIENTATION_H
#define TOPOCULE_ORIENTATION_H

#include <Simulation_of_simplicity.h>

namespace tpcl::geometry {

    constexpr inline size_t num_points(size_t Dimensions) {
        return Dimensions + 1;
    }

    template <class Value_type = double>
    struct Orientation_with_degeneracy {
        constexpr static bool with_degeneracy = true;
        using value_type = Value_type;

        template<size_t dimension>
        static auto orientation(
                const std::array<value_type, /*Dimension * num_points(Dimension)*/ 6>& triangle_vertices,
                [[maybe_unused]] const std::array<Cell_id,/*num_points(Dimension)*/3>& triangle_vertex_ids
        ) {
            using value_type = typename std::remove_reference_t<decltype(triangle_vertices)>::value_type;
            constexpr int num_coords = dimension;
            constexpr int num_points = dimension + 1;
            std::array<value_type, num_points * (num_coords + 1)> matrix;
            constexpr int num_rows = num_points;
            constexpr int num_columns = num_points;

            for (int r = 0; r < num_rows; ++r) {
                for (int c = 0; c < num_columns - 1; ++c) {
                    matrix[c + num_columns * r] = triangle_vertices[c + num_coords * r];
                }
                // the last column of this matrix is always one
                matrix[(num_columns - 1) + num_columns * r] = value_type{1};
            }

            return geometry::details::sign_of_determinant(matrix);
        }
    };

    template <class Value_type = double>
    struct Orientation_with_simulation_of_simplicity {
        constexpr static bool with_degeneracy = true;
        using value_type = Value_type;

        template<size_t dimension>
        static auto orientation(
                const std::array<value_type, dimension * num_points(dimension)>& triangle_vertices,
                [[maybe_unused]] const std::array<Cell_id,num_points(dimension)>& triangle_vertex_ids
        ) {
            return geometry::sign_determinant_lambda(triangle_vertices, triangle_vertex_ids);
        }
    };


}

#endif //TOPOCULE_ORIENTATION_H
