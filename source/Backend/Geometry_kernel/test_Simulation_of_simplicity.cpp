//
// Created by Daisuke Sakurai on 2020-10-20.
//

#include <test.h>

#include <array>
#include <Simulation_of_simplicity.h>
#include <span>

template<int dimension>
struct id_coord_pair {
    std::array<double,dimension> coords;
    tpcl::Cell_id id;
};

enum test_type {
    COORDS,
    ID
};

template<int dimension, int type, class value_type, int out_size>
auto filter_items = [](const std::array<id_coord_pair<dimension>, dimension + 1>& pairs) {
    constexpr auto num_pairs = std::tuple_size_v<std::remove_reference_t<decltype(pairs)>>;
    std::array<value_type, out_size> out;
    for (int j = 0; j < num_pairs; ++j) {
        if constexpr ( type == COORDS ) {
            const auto size = pairs[j].coords.size();
            auto out_span = std::span<value_type, num_pairs>{out.data() + size*j, size};
            for (int i = 0; i < out_span.size(); ++i) {
                out_span[i] = pairs[j].coords[i];
            }
        } else {
            out[j] = pairs[j].id;
        }
    }
    return out;
};

template<size_t array_size>
auto check_sort (std::array<tpcl::Cell_id, array_size>& to_sort,
                     std::array<double,tpcl::geometry::details::matrix_size_barring_last_column(array_size)>& x,
                     bool swap_even_expected
) {

    auto sort_expected = [&to_sort, &x]() {
        std::array<id_coord_pair</*dimension*/array_size - 1>, array_size> out; // NOLINT Leave this unintialized
        for (int i = 0; i < array_size; ++i) {
            for (int j = 0; j < array_size - 1; ++j) {
                out[i].coords[j] = x[j + (array_size - 1)*i];
            }
            out[i].id = to_sort[i];
        }
        return out;
    }();

    std::sort(sort_expected.begin(), sort_expected.end(), [](
            const id_coord_pair<array_size-1>& a, const id_coord_pair<array_size-1>& b) {
        return a.id > b.id;
    });

    bool swap_even = tpcl::geometry::details::sort_and_report_swaps<std::greater<tpcl::Cell_id>>(to_sort, x);

    CHECK(swap_even == swap_even_expected);
    CHECK(x       == filter_items<array_size-1, COORDS,     double,        std::tuple_size_v<std::remove_reference_t<decltype(x)>>       >(sort_expected));
    CHECK(to_sort == filter_items<array_size-1, ID, tpcl::Cell_id, std::tuple_size_v<std::remove_reference_t<decltype(to_sort)>> >(sort_expected));
};

template<size_t array_size>
auto check_sort (std::array<tpcl::Cell_id, array_size>&& to_sort,
                 std::array<double,tpcl::geometry::details::matrix_size_barring_last_column(array_size)>&& x,
                 bool swap_even_expected
) {
    return check_sort<array_size>(to_sort, x, swap_even_expected);
}

// This needs to be macro because we pass the type `Depth`, which cannot be guessed by macros used within this macro
#define TPCL_DETERMINANTS_CHECK(                                               \
        pi_i1, pi_i2,                                                          \
        pi_j1, pi_j2,                                                          \
        pi_k1, pi_k2,                                                          \
        Depth,                                                                 \
        value                                                                  \
) {                                                                            \
    using namespace tpcl::geometry::details;                   \
    CHECK_THROWS_AS((sign_determinant_lambda_for_epsilons<int,Test_trait<1>>)( \
            pi_i1, pi_i2,                                                      \
            pi_j1, pi_j2,                                                      \
            pi_k1, pi_k2                                                       \
    ), Depth);                                                                 \
            CHECK(sign_determinant_lambda_for_epsilons<int>(                   \
            pi_i1, pi_i2,                                                      \
            pi_j1, pi_j2,                                                      \
            pi_k1, pi_k2                                                       \
    ) == value);                                                               \
}

TEST_CASE ("Test Geometry") {

    {
        INFO("1D determinants for Simulation of Simplicity");
        {
            std::array<double,4> x = {1.0, 1.0, 1.0, 1.0};
            auto d = tpcl::geometry::details::sign_of_determinant(x);
            CHECK(d == 0);
        }

        check_sort<2>({0, 1}, {0.0, 1.0}, false);
        check_sort<2>({1, 0}, {1.0, 0.0}, true);

        std::array<double,2> x = {0.0, 0.0};
        std::array<tpcl::Cell_id, 2> x_indices = {1, 0};
        auto e = tpcl::geometry::sign_determinant_lambda(x, x_indices);
        CHECK(e == 1);
    }

    {
        INFO("2D determinants for Simulation of Simplicity");
        std::array<tpcl::Cell_id, 3> x_indices = {2, 1, 0};
        std::array<double,6> x = {0.0, 0.0, 1.0, 0.0, 0.0, 0.0};
        check_sort(x_indices, x, /*even*/ true);
        auto e = tpcl::geometry::sign_determinant_lambda(x, x_indices);
        CHECK(e == -1);

        // 鬼 (mad guy) testing on the correctness of the epsilon arithmetic!
        using namespace tpcl::geometry;
        using namespace tpcl::geometry::details;

        TPCL_DETERMINANTS_CHECK(
                0, 0,
                1, 0,
                2, 0,
                Depth<1>,
                POSITIVE
        );
        TPCL_DETERMINANTS_CHECK(
                0, 0,
                2, 0,
                1, 0,
                Depth<1>,
                NEGATIVE
        );

        TPCL_DETERMINANTS_CHECK(
                0, 0,
                0, 1,
                0, 2,
                Depth<2>,
                NEGATIVE
        );
        TPCL_DETERMINANTS_CHECK(
                0, 0,
                0, 2,
                0, 1,
                Depth<2>,
                POSITIVE
        );

        TPCL_DETERMINANTS_CHECK(
                1, 0,
                0, 0,
                0, 0,
                Depth<3>,
                POSITIVE
        );

        TPCL_DETERMINANTS_CHECK(
                -1, 0,
                0, 0,
                0, 0,
                Depth<3>,
                NEGATIVE
        );

        TPCL_DETERMINANTS_CHECK(
                0, 0,
                0, 0,
                0, 0,
                Depth<4>,
                POSITIVE
        );
    }
}
