Consider `CGAL::Exact_predicates_exact_constructions_kernel`.
It is an alias to `CGAL::Epeck`.

Its 2D point type `CGAL::Epeck::Point_2`, an alias to `CGAL::Point_2<CGAL::Epeck>`,
inherits from `CGAL::Epeck::Kernel_base<...>::Point_2`.

Here, `CGAL::Epeck::Kernel_base<...>` is `Lazy_kernel_base`.

Further, Lazy_kernel_base inherits Point_2 from
        Lazy_kernel_generic_base<
                              /*Exact_kernel*/Simple_cartesian<Epeck_ft>,
                              /*Approximate_kernel*/Simple_cartesian<Interval_nt_advanced>,
                              /*E2A*/ Cartesian_converter< Simple_cartesian<Epeck_ft>,
                                                   Simple_cartesian<Interval_nt_advanced> >,
                              Epeck>

According to this Lazy_kernel_generic_base, Point_2 is
Lazy<typename Approximate_kernel::Point_2, typename Exact_kernel::Point_2, E2A>.

`Lazy` is defined in Lazy.h:674.
`Lazy` holds `PTR`, which is a pointer.
It is apparently reponsible for making shallow copies of Point_2.
In the constructor, the coordinates of Point_2 are passed
to R::Construct_point_2().

When accessing Point_2
---------------------
`R_::Kernel_base::Point_2::rep().x()` is called.
`rep` apparently resolves to `CGAL::PointC2<CGAL::Simple_cartesian>`.
This is likely the actual point class storing the coordinate values.
`PointC2` holds the value in a `Vector_2_`, which is `CGAL::PointC2<CGAL::Simple_cartesian>::Vector_2_`.
`Vector_2_` converts the internal representation to the coordinate value.

Adapting the Kernel
----
See [On the Design of CGAL](https://core.ac.uk/download/pdf/210674597.pdf) Section 8.6. Examples for Adaptability through Traits Classes.

I can start from compare_lexicographically_xyC2.
We could use  Point_2::rep()::id() to compare addresses.
However, the order of vertices using this ID is inconsistent with that using `tpcl::Cell_id`.
We should therefore use `Cell_id` instead.

> Can we pass Point_2 a custom point class that inherits from PointC2?
