//
// Created by Daisuke Sakurai on 2019/12/25.
//

//
// Created by Daisuke Sakurai on 2019/12/05.
//

#include <iostream>
#include <test.h>
#include <BGLGraph.h>
#include <GraphIO.h>
#include <sstream>

#include <prettyprint.hpp>

#include <test_compare_graphs.h>

namespace {
    /*
     * graph           value
     *    2             1 + epsilon
     *    |
     *    1             1
     *    |
     *    0             0
     */
    std::string linear_graph = R"(graph G {
0 [field=0];
1 [field=2];
2 [field=4];
0--1 ;
1--2 ;
}
)";

}

template <class Graph>
bool edge_exists(const unsigned int source, const unsigned int target, const Graph& g){
    return boost::edge(
            boost::vertex(source, g),
            boost::vertex(target, g),
            g
    ).second;
}

template <class Graph>
unsigned int vertex_degree(const unsigned int v, const Graph& g) {
    unsigned int count = 0;
    auto [ai, ae] = boost::adjacent_vertices(
            boost::vertex(v, g), g);
    while (ai != ae) {
        ++count;
        ++ai;
    }
    return count;
}

/**
 * Return the ID of the vertex
 * In general, return the vertex_index as offered by the Boost Graph Library
 * @tparam Graph
 * @param g
 * @param v
 * @return
 */
template <class Graph>
auto get_id(Graph& g, typename Graph::vertex_descriptor v) {
    return get(boost::vertex_index, g, v);
}

/**
 * For graphs that have pedigree IDs, return that one
 * @tparam Graph
 * @param g
 * @param v
 * @return
 */
template <>
auto get_id<tpcl::BGLGraph_list_based>(tpcl::BGLGraph_list_based& g, typename tpcl::BGLGraph_list_based::vertex_descriptor v) {
    return g[v].pedigree_id;
}

template<class Graph>
void check_graph_reading() {
    using namespace tpcl;

    Graph g;
    using vertex_descriptor = typename boost::graph_traits<decltype(g)>::vertex_descriptor;

    std::map<vertex_descriptor, double> field;

    // load linear graph
    try {
        std::stringstream {linear_graph} >> tpcl::BGLGraphLoader<Graph>{g}.with_property_a("field", field);
    } catch (std::exception& e) {
        std::cerr <<"Exception: "<<e.what()<<" "<<"\t"<< __FILE__ << ":" << __LINE__ << std::endl;
        throw e;
    }

    unsigned int cnt = 0;
    for (auto [vi, ve] = boost::vertices(g);
         vi != ve;
         ++vi, ++cnt
            ) {
        auto id = get_id(g, *vi);
        INFO("Check that the vertex ID is properly set");
        CHECK(id == cnt);
    }
    CHECK(edge_exists(0,1, g));
    CHECK(edge_exists(1,2, g));

    CHECK(vertex_degree(0, g) == 1);
    CHECK(vertex_degree(1, g) == 2);
    CHECK(vertex_degree(2, g) == 1);

    CHECK(field[boost::vertex(0, g)] == 0);
    CHECK(field[boost::vertex(1, g)] == 2);
    CHECK(field[boost::vertex(2, g)] == 4);
}

TEST_CASE ("Test GraphIO") {
    using namespace tpcl;

    check_graph_reading<BGLGraph>();
    check_graph_reading<BGLGraph_list_based>();

    { // These should compile
        BGLGraph g;
        BGLGraph_list_based g_l;
        std::stringstream s;
        s << g;
        s << g_l;
    }
}

