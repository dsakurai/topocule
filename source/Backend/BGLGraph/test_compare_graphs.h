//
// Created by Daisuke Sakurai on 2019/12/06.
//

#ifndef TOPOCULE_TEST_COMPARE_GRAPHS_H
#define TOPOCULE_TEST_COMPARE_GRAPHS_H

#include <BGLGraph.h>
#include <GraphIO.h>

template<class Graph>
bool same_string(const std::string &g0, Graph &g1) {

    std::stringstream stream1;

    stream1 << tpcl::BGLGraphPrinter{g1};
    if (g0 != stream1.str()) {
        std::cerr <<"g0: "<<g0<<" "<<"stream1.str(): "<<stream1.str()<<" "<<"\t"<< __FILE__ << ":" << __LINE__ << std::endl;
    }

    return g0 == stream1.str();
}

// This is a poor comparison of graphs based on their string representation.
// The graphs are ideally decleared const, but it's hard due to the design of dynamic properties in Boost
template<class Graph0, class Graph1>
bool same_string(Graph0 &g0, Graph1 &g1) {

    std::stringstream stream0;

    stream0 << tpcl::BGLGraphPrinter{g0};

    return same_string(stream0.str(), g1);
}

#endif //TOPOCULE_TEST_COMPARE_GRAPHS_H
