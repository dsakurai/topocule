//
// Created by Daisuke Sakurai on 2019/12/20.
//

#ifndef TOPOCULE_GRAPHIO_H
#define TOPOCULE_GRAPHIO_H

#include <BGLGraph.h>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/property_map/dynamic_property_map.hpp>
#include <boost/property_map/vector_property_map.hpp>
#include <memory>

/** Printer functions for vanilla BGL graphs.
 * Should be left in the global namespace
 * @sa BGLGraphPrinter
 */

// we don't accept every single type as a graph
namespace tpcl {
    template<typename T>
    struct is_bgl_graph : public std::false_type {
    };
// yet we accept BGL adjacency lists as a graph
    template< class... Types>
    struct is_bgl_graph<
            boost::adjacency_list<Types...>
            > : public std::true_type {
    };

    template<typename T>
    struct is_ostream : public std::false_type {
    };
// yet we accept BGL adjacency lists as a graph
    template<>
    struct is_ostream<
            std::ostream
    > : public std::true_type {
    };
}

namespace boost
{
    // A null property is somewhat like the inverse of the constant
    // property map except that instead of returning a single value,
    // this eats any writes and cannot be read from.

    template <class Graph, class V_type>
    struct dummy_vertex_id_property_map
    {
        dummy_vertex_id_property_map (Graph& g) : g {g} {

        }
        typedef typename Graph::vertex_descriptor key_type;
        typedef V_type value_type;
        typedef value_type& reference;
        typedef boost::writable_property_map_tag category;

        Graph& g;
        value_type tmp_id;

        /**
         * `dynamic_properties` requires a get method, although for a reader does not need it.
         * This implementation provides that method, although not thread-safe.
         * It is best to avoid the use of this function unless really necessary.
         *
         * @tparam prop
         * @param pmap
         * @param key
         * @return
         */
        friend reference&
        get(const dummy_vertex_id_property_map<Graph,value_type>& pmap,
                const typename dummy_vertex_id_property_map<Graph,value_type>::key_type& key) {
            auto& p = const_cast<dummy_vertex_id_property_map<Graph,value_type>&>(pmap);
            return p.tmp_id = get(boost::vertex_index, pmap.g, key);
        }
    };

    template <class G, class value_type>
    void put(dummy_vertex_id_property_map<G,value_type>& /*pm*/,
            const typename G::vertex_descriptor& /*key*/,
            const value_type& /*value*/)
    { }

}

namespace tpcl {

    template<class Graph>
    class BGLGraphIOBase {
    public:
        using vertex_descriptor = typename boost::graph_traits<Graph>::vertex_descriptor;

        BGLGraphIOBase(
                Graph& graph,
                boost::dynamic_properties& properties
        ):
                graph (graph),
                properties (&properties)
        {
        }

        // automatic set up of the dynamic properties
        BGLGraphIOBase(
                Graph& graph
        ):
                graph (graph)
        {
            new_properties = true;
            properties = new boost::dynamic_properties;
        }
        virtual ~BGLGraphIOBase() {
            if (new_properties) delete properties;
        }

        /**
         * Create and set the node ID property
         */
        void set_node_id_property() {
            friend_set_node_id_property(this);
        }

        /**
         * Add a container as a property map. The container must have STL-style iterator and support random access.
         *
         * @tparam container: a container like STL vector
         * @param name
         * @param c
         * @return
         */
        template<class container>
        BGLGraphIOBase& with_property_r(std::string name, container& c) {
            auto begin = c.begin();
            return with_property_i(name, begin);
        }

        /**
         * Add a C-style array or some STL-style container as a property map.
         * A STL-style container must have an iterator and support random access.
         * This function accepts iterator
         * This allows for random access of properties.
         * Assigns an iterator-based property map (often using std::vector as the internal data storage).
         *
         * @tparam container
         * @param name
         * @param c
         * @return
         */
        template<class iterator>
        BGLGraphIOBase& with_property_i(std::string name, iterator& i) {
            using Vertex = typename boost::graph_traits<Graph>::vertex_descriptor;
            using VertexID_Map = typename boost::property_map<Graph, boost::vertex_index_t>::type;
            VertexID_Map vertex_id = get(boost::vertex_index, graph);
            boost::iterator_property_map<iterator, VertexID_Map> values (i, vertex_id);
            properties->property(name, values);
            return *this;
        }

        /**
         * Assign an associative property map (often by using std::map as the internal data storage).
         * Assign an associative property map using std::map as the internal data storage.
         * Use of an associative property map is required for reading properties (at least) for BGLGraph_list_based with the Boost Graph Library's
         * dot file format reader (boost::read_graphviz).
         *
         * @tparam map
         * @param name
         * @param i
         *
         * ~~~
         * std::map<tpcl::BGLGraph::vertex_descriptor,double> p;
         * bGLGraphPrinter.with_property_a("above sea level", p);
         * ~~~
         */
        template<class map>
        BGLGraphIOBase& with_property_a(std::string name, map& m) {
            boost::associative_property_map<map> property_map( m );
            properties->property(name, property_map);
            return *this;
        }

        /*
         * Use a vector. Doesn't work when using list-based vertices for an adjacency list
         * @tparam vect
         * @param name
         * @param m
         * @return
         */
//        template<class val>
//        BGLGraphIOBase& with_property_v(std::string name, boost::vector_property_map<val>& v) {
//            properties->property(name, v);
//            return *this;
//        }

        const boost::dynamic_properties* get_properties() const {
            return properties;
        }

        boost::dynamic_properties* get_properties() {
            return properties;
        }

        Graph* get_graph() const {
            return &graph;
        }

        Graph* get_graph() {
            return &graph;
        }

        virtual void read(std::istream &i) { }
        virtual void print(std::ostream &) const { }

        /* Reader functions
         *
         * @param i: input stream
         * @param graph
         */
        friend
        std::istream &operator>>(std::istream &i, BGLGraphIOBase &graph) {
            graph.read(i);
            return i;
        }

        friend
        std::ostream &operator<<(std::ostream &o, const BGLGraphIOBase &graph) {
            graph.print(o);
            return o;
        }

    private:
        // It's nice to make this const, but it's very hard due to the design of dynamic properties in Boost
        Graph& graph;
        boost::dynamic_properties* properties;

        bool new_properties = false;

        // copy not allowed due to the fragile management of boost::dynamic_properties;
        BGLGraphIOBase(BGLGraphIOBase&);
    };

    /**
     * set the vertex index map as a dynamic property for BGLGraph_list_based
     */
    template <class Graph>
    inline void friend_set_node_id_property(BGLGraphIOBase<Graph>* graph){
        graph->get_properties()->property("node_id",
                                          boost::get(&Vertex_property::pedigree_id, *graph->get_graph())
        );
    }

    /**
     * This class can be used for temporary (r-value) instanciation, like:
     * ~~~
     *  std::cerr <<"g: "<<BGLGraphPrinter(g);
     * ~~~
     *
     * To add properties in the output, you may do...
     * ~~~
     *  std::cerr <<"g: "<<BGLGraphPrinter(g).with_property_r("some_name", some_vector);
     * ~~~
     *
     * Passing around is not in design.
     * @tparam Graph
     */
    template<class Graph>
    class BGLGraphPrinter: public BGLGraphIOBase<Graph>  {
    public:
        BGLGraphPrinter(
                Graph& graph,
                boost::dynamic_properties& properties
        ):
                BGLGraphIOBase<Graph>(graph, properties)
        {
        }

        // automatic set up of the dynamic properties
        BGLGraphPrinter(
                Graph& graph
        ):
                BGLGraphIOBase<Graph> (graph)
        {
            // ignore virtual-ness inside a constructor!
            BGLGraphIOBase<Graph>::set_node_id_property();
        }

        void print(std::ostream &o) const override {
            boost::write_graphviz_dp(o, *this->get_graph(), *this->get_properties());
        }
    };


    template<class Graph>
    class BGLGraphLoader: public BGLGraphIOBase<Graph>  {
    public:
        using SuperClass = BGLGraphIOBase<Graph>;
        using vertex_descriptor = typename SuperClass::vertex_descriptor;

        using default_node_id_map_type = std::map<vertex_descriptor,std::string>;

        BGLGraphLoader(
                Graph& graph,
                boost::dynamic_properties& properties
        ): BGLGraphIOBase<Graph> (graph, properties) { }

        // automatic set up of the dynamic properties
        BGLGraphLoader(Graph& graph): BGLGraphIOBase<Graph> (graph) {
            BGLGraphIOBase<Graph>::set_node_id_property();

            auto& dp = *this->get_properties();
        }

        void read(std::istream &i) override {
            /// @todo reading a contour tree from .dot file is experimental and thus error-prone
            boost::read_graphviz(i, * this->get_graph(), * this->get_properties());
        }
    };

    template<class Graph>
    class BGLGraphWrapper {
    public:
        BGLGraphWrapper(
                std::shared_ptr<Graph>& graph,
                std::shared_ptr<boost::dynamic_properties>& properties
        ):
                graph (graph),
                properties (properties)
        {
        }

    private:
        std::shared_ptr<Graph> graph;
        std::shared_ptr<boost::dynamic_properties> properties;

        friend
        std::ostream &operator<<(std::ostream &o, const tpcl::BGLGraphWrapper<Graph> &wrapper) {
            return o << BGLGraphPrinter<Graph>(*wrapper.graph, *wrapper.properties.get());
        }
    };
}

/**
 * It's hard to allow const graphs as inputs due to the implementation of the
 * design of the dynamic properties in Boost.
 */
template <class Graph>
typename std::enable_if<
        /* if it's a BGL graph*/tpcl::is_bgl_graph<Graph>::value,
        /*use this return type*/ std::ostream& >::type
/* ostream& */ operator<<( std::ostream& o, Graph &graph ) {
    return o << tpcl::BGLGraphPrinter{graph};
}


#endif //TOPOCULE_GRAPHIO_H
