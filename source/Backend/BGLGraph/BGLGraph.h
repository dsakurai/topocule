//
// Created by Daisuke Sakurai on 2019/11/23.
//

#ifndef TOPOCULE_BGLGRAPH_H
#define TOPOCULE_BGLGRAPH_H

#include <boost/graph/adjacency_list.hpp>

namespace tpcl {

    struct Pedigree_id {
        unsigned int pedigree_id = -1;
    };

    struct Edge_property/*: public Pedigree_id*/ {
    };

    struct Vertex_property: public Pedigree_id {
    };

    using BGLGraph =
    boost::adjacency_list<
            boost::vecS, // edge
            boost::vecS, // vertex
            boost::undirectedS,
            Vertex_property,
            Edge_property
            >;

    using BGLGraph_list_based =
    boost::adjacency_list<
            boost::listS, // edge
            boost::listS, // vertex
            boost::undirectedS,
            Vertex_property,
            Edge_property
    >;
}

#endif //TOPOCULE_BGLGRAPH_H
