//
// Created by Daisuke Sakurai on 2020-07-21.
//

#ifndef TOPOCULE_MULTI_FIELDS_H
#define TOPOCULE_MULTI_FIELDS_H

#include <May_handle_simplicial_complex.h>
#include <Tessellation.h>
#include <Field_values.h>

#include <memory>
#include <list>

namespace tpcl {
    class Multi_fields:
            public May_handle_simplicial_complex,
            public Has_another_simplicial_complex_handler
            {
    public:
        void set_tessellation(const std::shared_ptr<Tessellation> &tessellation) {
            this->tessellation = tessellation;
        }

        const std::shared_ptr<Tessellation>& get_tessellation() {
            return tessellation;
        }

        [[nodiscard]]
        std::shared_ptr<const Tessellation> get_tessellation() const {
            return tessellation;
        }

        void add_field_values(const std::shared_ptr<Field_values>& field_values) {
            this->field_values.emplace_back(field_values);
        }

        const std::shared_ptr<Field_values>& get_field_values(std::string name) {
            for (auto& field: field_values) {
                if (field->get_name() == name) return field;
            }
            throw std::runtime_error {"could not find field named " + name};
        }

        const std::list<std::shared_ptr<Field_values>>& get_field_values() {
            return field_values;
        }

    private:

        bool is_for_simplicial_complex() const override {
            return delegate_simplicial_complex_verification(tessellation.get());
        }

        std::shared_ptr<Tessellation> tessellation;
        std::list<std::shared_ptr<Field_values>> field_values;
    };
}



#endif //TOPOCULE_MULTI_FIELDS_H
