//
// Created by Daisuke Sakurai on 2020-07-21.
//

#ifndef TOPOCULE_FIELD_VALUES_H
#define TOPOCULE_FIELD_VALUES_H

#include <vector>
#include <memory>
#include <string>

namespace tpcl {
    class Field_values {
    public:
        Field_values(){}
        Field_values(std::string name) : name{name} {}

        void set_name(std::string name) { this->name = name; }

        std::string get_name() const {return name;}

        virtual ~Field_values(){}

    private:
        std::string name;
    };

    template<class value_type_>
    class Field_values_of_type : public Field_values {
    public:
        using value_type = value_type_;
        using Field_values::Field_values;

        void set_values(std::shared_ptr<std::vector<value_type>> values) { this->values = values; }

        value_type& operator[](size_t i) {
            return (*values)[i];
        }

    private:
        std::shared_ptr<std::vector<value_type>> values;
    };
}

#endif //TOPOCULE_FIELD_VALUES_H
