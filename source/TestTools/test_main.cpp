//
// Created by Daisuke Sakurai on 2019/11/03.
//


#define DOCTEST_CONFIG_IMPLEMENTATION_IN_DLL
#define DOCTEST_CONFIG_IMPLEMENT
#include <doctest/doctest.h>

int main(int argc, char *argv[])
{
    doctest::Context context(argc, argv);

    int doctestResult = context.run();

    if (context.shouldExit())
        return doctestResult;
}


