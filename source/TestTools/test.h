//
// Created by Daisuke Sakurai on 2019/11/03.
//

#ifndef TOPO_MOLECULE_STATES_TEST_H
#define TOPO_MOLECULE_STATES_TEST_H

#include <Example_dataset_reader.h>

#include <iostream>
#include <prettyprint.hpp>
#include <nlohmann/json.hpp>

#include <checker.h>
#include <Printer.h>

#include <doctest/doctest.h>

#ifndef TPCL_IS_COMPILING_TEST
static_assert(false, "This test should be compiled with the TPCL_IS_COMPILING_TEST macro defintion, but it is undefined.");
#endif

#endif //TOPO_MOLECULE_STATES_TEST_H
