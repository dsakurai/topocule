//
// Created by Daisuke Sakurai on 2020-11-20.
//

#include <QApplication>
#include <GUI_basics.h>
#include <Space_widget.h>

int main(int argc, char** argv) {
    using namespace tpcl;

    QApplication application( argc, argv );

    tpcl::set_qt_surface_format();

    Space_widget widget;
    widget.run();
    widget.show();

    return application.exec();
}
