//
// Created by Daisuke Sakurai on 2/23/19.
//

#include <Compute_contour_tree.h>
#include <ComputeDelaunay.h>
#include <GraphIO.h>
#include <MoleculeReader.h>
#include <SimplifyContourTree.h>
#include <TriangulationFromDelaunay.h>

#include <CLI/CLI.hpp>
#include <string>
#include <thread>

#ifdef TPCL_ENABLE_GUI
#include <QApplication>
#include <QSurfaceFormat>
#include <GraphViewerWidget.h>
#include <GUI_basics.h>
#include <LayoutGraph.h>
#endif


template<class Graph>
bool same_string(const std::string &g0, Graph &g1) {

    std::stringstream stream1;

    stream1 << tpcl::BGLGraphPrinter{g1};
    if (g0 != stream1.str()) {
        std::cerr <<"g0: "<<g0<<" "<<"stream1.str(): "<<stream1.str()<<" "<<"\t"<< __FILE__ << ":" << __LINE__ << std::endl;
    }

    return g0 == stream1.str();
}

// This is a poor comparison of graphs based on their string representation.
// The graphs are ideally decleared const, but it's hard due to the design of dynamic properties in Boost
template<class Graph0, class Graph1>
bool same_string(Graph0 &g0, Graph1 &g1) {

    std::stringstream stream0;

    stream0 << tpcl::BGLGraphPrinter{g0};

    return same_string(stream0.str(), g1);
}

namespace {
    template<class clock_type>
    std::ostream& operator<<(std::ostream& o, const typename std::chrono::time_point<clock_type>& stamp) {

        auto time = clock_type::to_time_t(stamp);
        struct tm tm;
        localtime_r(&time, &tm);

        return o << std::put_time(&tm, "%c");
    }

    template <class mapped_type>
    void load_graph(
            const std::string dot_file,
            std::string property_name_y,
            tpcl::BGLGraph_list_based& g,
            std::map<tpcl::BGLGraph_list_based::vertex_descriptor, mapped_type>& ys
            ) {
        std::ifstream ifs (dot_file, std::ifstream::in);

        { // load data
            ifs >> tpcl::BGLGraphLoader{g}.with_property_a(property_name_y, ys);
        }
    }

    std::string extension(std::string file_name) {
        return file_name.substr(file_name.find_last_of("."));
    }

}

void load_some_graph_file(
        std::string& graph_file_name,
        tpcl::BGLGraph_list_based& g,
        std::map<tpcl::BGLGraph_list_based::vertex_descriptor, double>& ys
) {
    using namespace std;
    if (extension(graph_file_name) != ".dot") throw runtime_error {"Unknown file extension."};
    std::cerr << "Loading the contour tree\t" << __FILE__ << ":" << __LINE__ << std::endl;
    std::string property_name_y = "chi";

    load_graph(graph_file_name, property_name_y, g, ys);
}

void simplify_degree_two(tpcl::BGLGraph_list_based& g,
                          std::map<tpcl::BGLGraph_list_based::vertex_descriptor, double>& ys
) {
    using namespace tpcl;
    using Graph = tpcl::BGLGraph_list_based;
    using vertex_descriptor = Graph::vertex_descriptor;
    using Traits = Contour_tree_traits<double>;

    std::shared_ptr<std::vector<vertex_descriptor>> dummy {nullptr};
    Graph_util<Traits> graph_util{
            g,
            ys,
            dummy
    };

    if (boost::num_vertices(g) != ys.size()) throw std::runtime_error {"number of vertexes doesn't match the size of property"};

    // for vertices
    for (auto map_iter = ys.begin(); map_iter != ys.end();) {
        const auto cur_iter = map_iter;
        ++map_iter;

        auto vd = cur_iter->first;

        // This works for a serial computation.
        // This cannot extend to parallelism without a clever trick like atomicity or divide-and-conquer.
        if (auto two_out_edges = tpcl::out_degree_two(vd, g)) {
            auto& edges = *two_out_edges;

            merge_edges(
                    boost::target(edges[0], g),
                    vd,
                    boost::target(edges[1], g),
                    edges[0],
                    edges[1],
                    graph_util
            );
        }
    }
}

class Range_filter: public tpcl::Dat_file_reader::Line_filter {
public:
    Range_filter(){}
    Range_filter(const size_t begin, const size_t end) : begin {begin}, end {end}{ }
    void set_begin(const size_t begin){ this->begin = begin;}
    void set_end(const size_t end){ this->end = end;}
    void operator() (
            __attribute__((unused))/*not in use*/ std::string& row,
            size_t line_count) override
    {
        bool inside = (begin <= line_count) && (line_count < end);
        if (!inside) row = ""; // skip this line
    }

private:
    size_t begin = 0;
    size_t end = -1;
};

template <class Graph>
[[nodiscard]]
inline std::shared_ptr<tpcl::Tessellation> tessellation_from_file(
        std::shared_ptr<Graph> &out,
        std::vector<double> &chi,
        std::array<std::shared_ptr<tpcl::Dat_file_reader>,2>& file_readers,
        const std::string file_coords,
        const std::string file_field
) {

    using namespace tpcl;

    auto points = std::make_shared<std::vector<std::vector<double>>>();

    MoleculeReader reader;
    reader.set_reader(MoleculeReader::coords, file_readers[0]);
    reader.set_reader(MoleculeReader::field, file_readers[1]);

    reader.set_file_name(MoleculeReader::coords, file_coords);
    reader.set_file_name(MoleculeReader::field, file_field);


    size_t dim, num_points;
    {
        // @todo support vector<vector<>> in MoleculeReader
        std::vector<double> tmp_coords;

        {
            reader.get_array_shape(MoleculeReader::coords, dim, num_points);

            tmp_coords.resize(dim * num_points);

            auto data = tmp_coords.data();
            reader.set_pointer(MoleculeReader::coords, data);
        }

        {
            size_t columns, num_field_elements;
            reader.get_array_shape(MoleculeReader::field, columns, num_field_elements);

            chi.resize(columns * num_field_elements);
            auto data = chi.data();
            reader.set_pointer(MoleculeReader::field, data);
        }

        reader.doIt();

        points->resize(num_points);
        for (auto &p : *points) {
            p.resize(dim);
        }
        for (int p = 0; p < num_points; ++p) {
            for (int x = 0; x < dim; ++x) {
                points->at(p).at(x) = tmp_coords[x + dim * p];
            }
        }
    }

    ComputeDelaunay<decltype(points)::element_type> adjacency;
    adjacency.setPoints(points);
    adjacency.doIt();

    Ttk_nd_triangulation triangulation;

    triangulationFromDelaunay(adjacency, triangulation);

    return triangulation.get_tpcl_triangulation();
}


int main(int argc, char** argv) {
    using namespace std;

    std::shared_ptr<tpcl::BGLGraph_list_based> g;
    std::map<tpcl::BGLGraph_list_based::vertex_descriptor, double> ys;

    // allocate the graph
    auto allocate_graph = [](decltype(g)& g_){
        g_ = std::make_shared<tpcl::BGLGraph_list_based>();
    };

    // load a graph file

    optional<string> graph_file_name;
    auto load_graph_file = [&]() {
        allocate_graph(g);

        if (!graph_file_name) throw runtime_error{"File name unset"};
        load_some_graph_file(*graph_file_name, *g, ys);
    };


    // compute the graph

    std::shared_ptr<Range_filter> filter = nullptr;
    // first and last point coordinates to be loaded.
    // Basically the first and the last line to be processed in a simple ASCII file,
    // although headers etc. may shift the lines.
    // The numbering starts from 1 to follow the convention of text editors.
    optional<int> first_coordinates, last_coordinates;

    optional<string> coords_file_name;
    optional<string> field_file_name;
    auto compute_graph = [&]() {
        std::cerr << "Computing the contour tree\t" << __FILE__ << ":" << __LINE__ << std::endl;

        if (first_coordinates) {
            if(!filter) filter = std::make_shared<typename decltype(filter)::element_type>();
            filter->set_begin(*first_coordinates - 1);
        }
        if (last_coordinates) {
            if(!filter) filter = std::make_shared<typename decltype(filter)::element_type>();
            filter->set_end(*last_coordinates - 1 + 1);
        }

        std::vector<double> chi;
        allocate_graph(g);

        std::array<std::shared_ptr<tpcl::Dat_file_reader>,2> readers;
        for (auto& r: readers) {
            r = std::make_shared<tpcl::Dat_file_reader>();
            if (filter) r->set_filter(filter);
        }

        auto triangulation = tessellation_from_file(
                g,
                chi,
                readers,
                *coords_file_name,
                *field_file_name
        );

        tpcl::compute_contour_tree(triangulation, g, chi, std::thread::hardware_concurrency() / 2/*half of them are virtual cores*/);

        int id = 0;
        for (auto[vi, ve] = boost::vertices(*g);
             vi != ve;
             ++vi, ++id
                ) {
            (*g)[*vi].pedigree_id = id; // This should be set already, but just to be on the safe side...
            ys[*vi] = chi[id];
        }
    };


    // Simplify the graph

    optional<int> simplify_by_num_leaves;

    auto simplify = [&]() {
        if (!simplify_by_num_leaves) throw runtime_error {"error: unexpected combination of options."};
        if (!g) throw runtime_error {"error: graph is not allocated."};

        using mapped_type = typename decltype(ys)::mapped_type;
        using traits = tpcl::Contour_tree_traits<mapped_type>;

        tpcl::SimplifyContourTree<traits> simplifier;
        tpcl::End_with_leaf_count<decltype(simplifier)> ending_condition;
        ending_condition.set_num_leaves_in_the_end(*simplify_by_num_leaves);
        simplifier.set_ending_condition(&ending_condition);
        simplifier.set_graph(*g, ys);
        simplifier.doIt();

        simplify_degree_two(*g, ys);
    };

    // Write the graph to a file

    auto write_file = [&](const string& file_out) {
        ofstream stream {file_out};
        tpcl::BGLGraphPrinter printer{*g};
        printer.with_property_a("chi", ys);

        stream << printer;
        stream.close();
    };


    // Show the graph in a GUI window

    bool show_gui = true; // won't take effect without Qt
#ifdef TPCL_ENABLE_GUI
    auto run_qt_app = [&]() {
        QApplication application( argc, argv );

        tpcl::set_qt_surface_format();

        GraphViewerWidget widget;

        // show the graph?
        if (g) {
            std::vector<std::array<double,3>> coords;
            std::array<std::array<double,2>,3> bounds;
            std::cerr <<"Layouting the contour tree\t"<< __FILE__ << ":" << __LINE__ << std::endl;
            layout_graph(*g, ys, coords);

            std::cerr <<"Displaying the contour tree\t"<< __FILE__ << ":" << __LINE__ << std::endl;
            widget.setGraph(g, coords);
        }

        widget.show();
        return application.exec();
    };
#endif // TPCL_ENABLE_GUI

    //
    // Parse program arguments
    //

    CLI::App app{"Demos for topological analysis for molecule"};

    auto demo_command = app.add_subcommand("demo", "Run demo");
    optional<string>  demo_name = "two_vertices";
    demo_command->add_option(
            "demo_name",
                demo_name,
                "Name of a demo to choose from")
                ->check(CLI::IsMember{{"two_vertices", "pentane", "pentane_with_computation"}})
            ->required();
    demo_command->callback([&](){
        if (demo_name == "two_vertices") {
            g = std::make_shared<tpcl::BGLGraph_list_based>();

            // set demo
            boost::add_vertex(*g);
            boost::add_vertex(*g);

            auto v0 = boost::vertex(0,*g);
            (*g)[v0].pedigree_id = 0;

            auto v1 = boost::vertex(1,*g);
            (*g)[v1].pedigree_id = 1;

            ys[v0] = 0.0;
            ys[v1] = 1.0;

            boost::add_edge(v0,v1,*g);
        } else if (demo_name == "pentane") {
            graph_file_name = TPCL_PENTANE_PROCESSED_DATA_DIR "/pentane-contour-tree.dot";
            load_graph_file();
        } else if (demo_name == "pentane_with_computation") {
            coords_file_name = PENTANE_DATA_DIR "/" "R.txt";
            field_file_name  = PENTANE_DATA_DIR "/" "chi.txt";
            compute_graph();
        } else {
            throw std::runtime_error {"Unknown demo"};
        }
    });

    auto compute_command = app.add_subcommand(
            "compute", "Compute the contour tree");
    compute_command->add_option(
                    "coords_file_name", coords_file_name, "coordinates file name")
                    ->required();
    compute_command->add_option(
                    "field_file_name", field_file_name, "field file name")
                    ->required();
    compute_command->add_option("--first_coordinates", first_coordinates, "skip point coordinates before this one (numbering starts from 1)");
    compute_command->add_option("--last_coordinates", last_coordinates, "skip point coordinates after this one (numbering starts from 1)");
    compute_command->callback(compute_graph);

    auto file_command = app.add_subcommand("graph_file", "Read file");
    file_command->add_option(
            "file_name", graph_file_name, "contour tree file name (.dot)");
    file_command->callback(load_graph_file);

    app.add_option("--simplify_by_num_leaves", simplify_by_num_leaves, "Simplify the graph by the number of vertices");

    optional<string> file_out;
    app.add_option("--file_out", file_out, "Save the result as a file");

    CLI11_PARSE(app, argc, argv);

    if(!g) {
        std::cerr <<"Found no graph to process further.\t"<< __FILE__ << ":" << __LINE__ << std::endl;
    }

    if (simplify_by_num_leaves) simplify();

    if (file_out) write_file(*file_out);

    //
    // Show GUI
    //
#ifdef TPCL_ENABLE_GUI
    if (show_gui) return run_qt_app();
#endif
    return 0;
}

