//
// Created by Daisuke Sakurai on 2020-11-20.
//

#include "Space_widget.h"
#include <iostream>
#include <osg/ShapeDrawable>

void tpcl::Space_widget::run() {
    auto scene_data = dynamic_cast<osg::Group*>(getMainView()->getSceneData());
    if (!scene_data){
        std::cerr <<"error: could not get scene data node as a group node.\t"<< "\x1b[37m" << __FILE__ << ":" << __LINE__ << "\x1b[0m" << std::endl;
        return;
    }

    osg::Group *objects_group = new osg::Group;
    objects_group->setName("Root group");

    std::vector<osg::Vec3> centers = {
            osg::Vec3{0.0, 0.0, 0.0},
            osg::Vec3{1.0, 0.0, 0.0},
            osg::Vec3{0.0, 1.0, 0.0},
            osg::Vec3{1.0, 1.0, 0.0},
            osg::Vec3{0.0, 0.0, 1.0},
            osg::Vec3{1.0, 0.0, 1.0},
            osg::Vec3{0.0, 1.0, 1.0},
            osg::Vec3{1.0, 1.0, 1.0},
    };

    auto register_sphere = [&objects_group](osg::Vec3& coord) {
        osg::Sphere *sphere = new osg::Sphere(coord, 0.25);

        auto drawable = new osg::ShapeDrawable(sphere);
        drawable->setColor(osg::Vec4(1.f, 0.f, 0.f, 1.f));
        objects_group->addChild(drawable);
    };

    for (auto& center: centers) {
        register_sphere(center);
    }

    scene_data->addChild(objects_group);

    auto view_matrix = getCameraOfMainView()->getViewMatrix();
    auto projection_matrix = getCameraOfMainView()->getProjectionMatrix();

    // For using the matrices have a look at...
    // - SO https://stackoverflow.com/a/26237348/1414763
    // - OSG Source https://github.com/openscenegraph/OpenSceneGraph/blob/master/src/osgUtil/PolytopeIntersector.cpp

    getViewer()->getView(0)->home();
    getViewer()->realize();
}

