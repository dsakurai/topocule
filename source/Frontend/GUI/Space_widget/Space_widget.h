//
// Created by Daisuke Sakurai on 2020-11-20.
//

#ifndef TOPOCULE_SPACE_WIDGET_H
#define TOPOCULE_SPACE_WIDGET_H

#include <DomainOSGWidget.h>

namespace tpcl {

    class Space_widget: public DomainOSGWidget {
        Q_OBJECT
    public:
        void run();
    };

}


#endif //TOPOCULE_SPACE_WIDGET_H
