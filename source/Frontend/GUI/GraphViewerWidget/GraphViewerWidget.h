//
// Created by Daisuke Sakurai on 2020/01/07.
//

#ifndef TOPOCULE_GRAPHVIEWERWIDGET_H
#define TOPOCULE_GRAPHVIEWERWIDGET_H

#include <BGLGraph.h>

#include <array>
#include <DomainOSGWidget.h>
#include <vector>
#include <iostream>
#include <osg/LineWidth>
#include <osg/Program>
#include <osgDB/ReadFile>
#include <osg/ShapeDrawable>
#include <QToolTip>
#include <QMouseEvent>
#include <QEvent>
#include <QHelpEvent>

namespace details {
    template<class Graph>
    int vertex_degree (typename Graph::vertex_descriptor& v, Graph& g) {
        int cnt = 0;
        for (auto[ai, ae] = boost::adjacent_vertices(v, g);
             ai != ae;
             ++ai, ++cnt){
        }
        return cnt;
    }

    template<class V, class G>
    unsigned int get_id(V v, G &g) {
        return g[v].pedigree_id;
    }
}

class Identifiable {
public:
    Identifiable(int id): id {id} {}
    int get_id() {
        return id;
    }
private:
    int id;
};

/**
 * The assumption of this class is that we don't have so many vertices
 * to be displayed. Indeed, the limit of graph visualization is merely ~1000 vertices.
 * This means access speed to its property is more important than the memory size.
 */
template<class Graph>
class DrawableGraphVertex: public osg::ShapeDrawable, public Identifiable {
public:
    DrawableGraphVertex (osg::Sphere* s, std::shared_ptr<Graph>& g, int id) :
            osg::ShapeDrawable {s},
            g {g},
            Identifiable {id}
    {}
private:
    std::shared_ptr<Graph> g = nullptr;
};


class GraphViewerWidget: public DomainOSGWidget {
Q_OBJECT
public:
    GraphViewerWidget (QWidget *parent = nullptr, Qt::WindowFlags f = 0);

    // Not in use
    void setDemoGraph();

    template <class Graph>
    void setGraph(
            std::shared_ptr<Graph>& g,
            const std::vector<std::array<double,3>>& coords
    ) {
        auto scene_data = dynamic_cast<osg::Group*>(getMainView()->getSceneData());
        if (!scene_data){
            std::cerr <<"error: could not get scene data node as a group node.\t"<< "\x1b[37m" << __FILE__ << ":" << __LINE__ << "\x1b[0m" << std::endl;
            return;
        }

        std::array<std::array<double, 2>,3> bounds;

//        pos.emplace_back("set spheres",std::chrono::system_clock::now(), __FILE__, __LINE__);
        { // I can probably use the position transformer in OSG

            for (auto[vi, ve] = boost::vertices(*g);
                 vi != ve;
                 ++vi) {
                auto vid = details::get_id(*vi, *g);
                auto& coord = coords.at(vid);
                for (int x = 0; x < 3; ++x) {
                    bounds[x][0] = std::min(coord[x], bounds[x][0]);
                    bounds[x][1] = std::max(coord[x], bounds[x][1]);
                }
            }
            osg::Group *graphgroup = new osg::Group;
            graphgroup->setName("graph");

            // create shader
            {
                osg::ref_ptr<osg::Program> program = new osg::Program;

                osg::ref_ptr<osg::Shader> fragShader = osgDB::readRefShaderFile(
                        TPCL_SHADERS_FLAT_SHADE
                );
                if (!fragShader)
                    throw std::runtime_error {"Could not read FRAGMENT shader from file"};

                program->addShader(fragShader);

                graphgroup->getOrCreateStateSet()->setAttributeAndModes(program.get(), osg::StateAttribute::ON);
            }

            bool show_all = false;
            if (show_all) {
                for (auto [vi, ve] = boost::vertices(*g); vi != ve; ++vi) {
                    auto vid = (*g)[*vi].pedigree_id;
                    auto vec = osg::Vec3(coords[vid][0], coords[vid][1], coords[vid][2]);
                    osg::Sphere *sphere = new osg::Sphere(vec, 0.01 * (bounds[0][1] - bounds[0][0]));
                    osg::ShapeDrawable *sd = new DrawableGraphVertex{sphere, g, (int) vid};
                    sd->setColor(osg::Vec4(0.f, 0.f, 0.f, 1.f));
                    sd->setName("node");
                    graphgroup->addChild(sd);
                }
            } else {
                for (auto[vi, ve] = boost::vertices(*g);
                     vi != ve;
                     ++vi) {
                    auto vid = details::get_id(*vi, *g);
                    auto vec = osg::Vec3(coords.at(vid)[0], coords.at(vid)[1], coords.at(vid)[2]);
                    osg::Sphere *sphere = new osg::Sphere(vec, 0.04 * (bounds[0][1] - bounds[0][0]));
                    osg::ShapeDrawable *sd = new DrawableGraphVertex{sphere, g, (int) vid};
                    if (details::vertex_degree(*vi, *g) == 1) {
                        sd->setColor(osg::Vec4(1.f, 0.f, 0.f, 1.f));
                        sd->setName("node");
                        graphgroup->addChild(sd);
                    } else {
                        sd->setColor(osg::Vec4(0.f, 0.f, 0.f, 1.f));
                        sd->setName("node");
                        graphgroup->addChild(sd);
                    }
                }
            }
            scene_data->addChild(graphgroup);
        }

        { // axes
            // lambda function
            auto add_v = [&scene_data](osg::Vec3 vec, osg::Vec4 col) {
                osg::Sphere *sphere = new osg::Sphere(vec, 0.03f);
                osg::ShapeDrawable *sd = new osg::ShapeDrawable(sphere);
                sd->setColor(col);
                sd->setName("node");
                scene_data->addChild(sd);
            };
//            add_v(osg::Vec3(0.0, 0.0, 0.0), osg::Vec4(0.5, 0.5, 0.5, 1.0));
//        add_v(osg::Vec3(1.0, 0.0, 0.0), osg::Vec4(1.0, 0.0, 0.0, 1.0));
//        add_v(osg::Vec3(0.0, 1.0, 0.0), osg::Vec4(0.0, 1.0, 0.0, 1.0));
        }

//        pos.emplace_back("register edges",std::chrono::system_clock::now(), __FILE__, __LINE__);
        { // pass graph to OSG
            osg::ref_ptr<osg::Vec3Array> points = new osg::Vec3Array;
            points->resize(coords.size());

            auto p = points->begin();
            for (auto [vi, ve] = boost::vertices(*g); vi != ve; ++vi) {
                auto vid = (*g)[*vi].pedigree_id;
                points->at(vid).x() = coords.at(vid)[0];
                points->at(vid).y() = coords.at(vid)[1];
                points->at(vid).z() = coords.at(vid)[2];
            }

            osg::ref_ptr<osg::Vec4Array> color = new osg::Vec4Array;
            color->push_back(osg::Vec4(0.5,0.5,0.5,1.0));

            osg::ref_ptr<osg::Geometry> edges_geometry (new osg::Geometry);
            edges_geometry->setVertexArray(points.get());
            edges_geometry->setColorArray(color.get());

/// @todo deprecated
            edges_geometry->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);

            osg::ref_ptr < osg::UIntArray > array = new osg::UIntArray();

            for ( auto [ei, ee] = boost::edges(*g);
                  ei != ee;
                  ++ei ) {
                auto v0 = boost::source(*ei,*g);
                auto v1 = boost::target(*ei,*g);
                auto id0 = details::get_id(v0,*g);
                auto id1 = details::get_id(v1,*g);
                array->push_back(id0);
                array->push_back(id1);
            }

            auto lines = new osg::DrawElementsUInt{osg::PrimitiveSet::LINES, /*size*/ (unsigned int) array->size(), & array->front() };

            edges_geometry->addPrimitiveSet(lines);
            edges_geometry->getOrCreateStateSet()->setAttribute(new osg::LineWidth{10.0f});

            scene_data->addChild(edges_geometry);
        }

        std::array<double, 3> center, delta;
        for (int i = 0; i < 3; ++i) {
            center[i] = (bounds[i][1] + bounds[i][0]) / 2.0;
            delta[i]  = bounds[i][1] - bounds[i][0];
        }

        // add margin to the boundary
        delta[0] *= 1.1;
        delta[1] *= 1.1;

        getCameraDelegate()->setUpOrthoGraphicCamera(
                center,
                delta[0],
                delta[1]
        );
        /// set the camera position
        getViewer()->getView(0)->home();
        getViewer()->realize();
    }


    void overlayToolTip(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event) override;
};


#endif //TOPOCULE_GRAPHVIEWERWIDGET_H
