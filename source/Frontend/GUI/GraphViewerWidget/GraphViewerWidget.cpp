//
// Created by Daisuke Sakurai on 2020/01/07.
//

#include "GraphViewerWidget.h"
#include <iostream>
#include <QToolTip>

#include <map>
#include <iostream>
#include <chrono>
#include <ctime>

#include <fstream>
#include <iomanip>
#include <LayoutGraph.h>

#include <QDebug>

GraphViewerWidget::GraphViewerWidget (QWidget *parent, Qt::WindowFlags f) :
        DomainOSGWidget (parent, f)
{

// set up the tool tip
    this->setMouseTracking( true );
}

void GraphViewerWidget::setDemoGraph() {
    auto g = std::make_shared<tpcl::BGLGraph_list_based>(2);

    auto v0 = boost::vertex(0,*g);
    (*g)[v0].pedigree_id = 0;

    auto v1 = boost::vertex(1,*g);
    (*g)[v1].pedigree_id = 1;

    std::map<tpcl::BGLGraph_list_based::vertex_descriptor, double> ys;
    ys[v0] = 0.0;
    ys[v1] = 1.0;

    boost::add_edge(v0,v1,*g);

    std::vector<std::array<double,3>> coords;

    layout_graph(*g, ys, coords);

    setGraph(g, coords);
}

void GraphViewerWidget::mouseMoveEvent(QMouseEvent *event) {
    OSGWidget::mouseMoveEvent(event);

    // overlay tool tip
    // @todo for cleaner code, install an event handler that will be processed within the event() method
    overlayToolTip(event);
}

void GraphViewerWidget::overlayToolTip(QMouseEvent *event) {
    auto height = this->height();
    auto view = getViewer()->getView(0);
    auto pos = event->pos();
    pos.setY(height - pos.y()); // this is for OpenSceneGraph
    pos *= devicePixelRatio();

    osg::ref_ptr<osgUtil::PolytopeIntersector> intersector =
            new osgUtil::PolytopeIntersector(osgUtil::Intersector::WINDOW,
                                             pos.x(), pos.y(), pos.x(), pos.y());
    osgUtil::IntersectionVisitor iv( intersector.get() );
    iv.setTraversalMask( ~0x1 );
    view->getCamera()->accept( iv );

    if ( intersector->containsIntersections() ) {
        auto result = intersector->getIntersections().begin();
        if (result != intersector->getIntersections().end()) {
                if (auto v = dynamic_cast<DrawableGraphVertex<tpcl::BGLGraph_list_based >*>(result->drawable.get())) {
                    int id = v->get_id();
                    // refresh the tool tip
                    QToolTip::showText(event->globalPos(), QString::number(id));
                }
        }
    }
}
