//
// Created by Daisuke Sakurai on 2020-11-20.
//

#include <GUI_basics.h>

#include <QSurfaceFormat>

void tpcl::set_qt_surface_format() {
    QSurfaceFormat format;
    // OpenGL modes
    format.setVersion(2, 1);
    format.setProfile( QSurfaceFormat::CompatibilityProfile );

    // @todo OpenGL in core profile (needs OpenSceneGraph specifically built for the core profile)
//        format.setVersion(3, 3);
//        format.setProfile( QSurfaceFormat::CoreProfile );

    // anti-aliasing
    format.setSamples(10);

    // set the rendering format
    QSurfaceFormat::setDefaultFormat(format);
}
