//
// Created by Daisuke Sakurai on 2020-11-20.
//

#ifndef TOPOCULE_GUI_BASICS_H
#define TOPOCULE_GUI_BASICS_H

namespace tpcl {
    void set_qt_surface_format();
}

#endif //TOPOCULE_GUI_BASICS_H
