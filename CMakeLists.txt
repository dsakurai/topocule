cmake_minimum_required(VERSION 3.13)
project(topocule)

# Ban 32-bit code on macOS (preferred by cereal)
set(CMAKE_OSX_DEPLOYMENT_TARGET 10.15)

# Enable rpath on Macs
#cmake_policy(SET CMP0068 NEW)
set(CMAKE_MACOSX_RPATH 1)

# TODO Build shared libs
set(BUILD_SHARED_LIBS No)

# Executables, libraries etc. are generated in the binary directory
# these actual paths may change in the future.
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR})

set(CMAKE_CXX_STANDARD 20)

option(TPCL_TRY_CPLUSPLUS_20 "Agressive use of C++20 features for concept checking." No)
if (NOT TPCL_TRY_CPLUSPLUS_20)
    add_definitions("-DTPCL_cplusplus_20=\(false\)")
endif(NOT TPCL_TRY_CPLUSPLUS_20)

# Make CGAL not to complain about debug mode
set(CGAL_DO_NOT_WARN_ABOUT_CMAKE_BUILD_TYPE True)

option(TPCL_ENABLE_TESTS "Enable tests." No)

if(TPCL_ENABLE_TESTS)
    set(TPCL_WITH_VTK Yes)
    enable_testing()
    include(test_with_build.cmake)
endif(TPCL_ENABLE_TESTS)

option(TPCL_WITH_CGAL "Include CGAL, which may be GPL-protected" Yes)
option(TPCL_ENABLE_GUI "Build the GUI" No)

set(TPCL_ENABLE_OPENMP Yes CACHE BOOL "Enable OpenMP. This adjusts TTK_ENABLE_OPENMP forcibly.")

if (TPCL_ENABLE_GUI)
    add_definitions("-DTPCL_ENABLE_GUI")
endif (TPCL_ENABLE_GUI)

option(TPCL_ENABLE_DOCUMENTATION "Enable documentation" No)
if(TPCL_ENABLE_DOCUMENTATION)
    # A variable in the global namespace
    set(DS_DOXYGEN_CITE_BIB_FILES ""
            CACHE INTERNAL ""
            )
    include(thirdparty/ds_doxygen/ds_doxygen-vcs/DSDoxygen.cmake)
    add_readme("${CMAKE_CURRENT_SOURCE_DIR}/README.md")
endif (TPCL_ENABLE_DOCUMENTATION)

#
# Load third party libraries
#

# We activate conan if the user has installed libraries through conan
set(TPCL_USE_CONAN No)
set(TPCL_CONAN_INCLUDE_FILE "${CMAKE_BINARY_DIR}/conan_paths.cmake")
if(EXISTS "${TPCL_CONAN_INCLUDE_FILE}")
    set(TPCL_USE_CONAN Yes)
endif()

add_subdirectory(thirdparty EXCLUDE_FROM_ALL)

if(TPCL_USE_CONAN)
    include("${TPCL_CONAN_INCLUDE_FILE}")
endif()

#
# Load resources
#
add_subdirectory(resources)

#
# Configure the project body
#

# Declare the doxygen variable
set(DS_DOXYGEN_EXCLUDE "${CMAKE_CURRENT_SOURCE_DIR}/thirdparty ${CMAKE_CURRENT_SOURCE_DIR}/resources ${DS_DOXYGEN_EXCLUDE}" )

add_subdirectory(source)

if(TPCL_ENABLE_DOCUMENTATION)
    add_doxygen_docs(doxygen_docs)
endif(TPCL_ENABLE_DOCUMENTATION)
