
add_library(prettyprint
    INTERFACE
)

target_sources(prettyprint
        INTERFACE
        prettyprint-vcs/prettyprint.hpp
)

target_include_directories(prettyprint
    INTERFACE
    "${CMAKE_CURRENT_SOURCE_DIR}/prettyprint-vcs"
)
