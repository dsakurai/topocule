//
// Created by Daisuke Sakurai on 2019/12/29.
//

#ifndef CHECK_H
#define CHECK_H

#include <unistd.h>
#include <string>
#include <iostream>
#include <csignal>
#include <map>
#include <memory>
#include <vector>

/**
 *
 *  ~~~
 *  check::stopper()->initialize(); // or reset()
 *  check::stopper()->depends_on("default check point");
 *  ...
 *  check::stopper()->break_point(); // does not stop the program because the default check point is de-activated
 *  ...
 *  check::check_point()->enable();
 *  check::stopper()->break_point(); // stops the program
 *  ~~~
 *
 * check::reset() must be called before a test case in Catch2.
 * This is because the lifetime is not identical to the clause currently.
 *
 * Check points are disabled by default; stoppers are enabled.
 */
namespace checker {

    enum config {
        default_config = 0,
        stopper_depends_on_check_point = 1
    };

    template <int = int()> // This template tricks makes this library header-only
    class Check_point_ {
    public:
        Check_point_ () {}
        Check_point_ (bool enable) : enabled_ (enable) {}

        using Check_point = Check_point_;

        void set_message(std::string &message) {
            this->message_ = message;
        }

        void enable(std::string file, int line, bool yes = true) {
            std::string name;
            for (auto& key_value : check_points_) {
                auto& [key, value] = key_value;
                if (&*value == this) {
                    name = key;
                }
            }
            std::cerr << name << " enabled at " << file << ":" << line << std::endl;
            enabled_ = yes;
        }

        void depends_on(std::string name) {
            dependencies_.push_back(get(name));
        }
        void depends_on_check_point() {
            depends_on(Check_point::default_name());
        }

        bool enabled() const {
            return enabled_;
        }

        std::string message() const {
            return message_;
        }

        const std::vector<Check_point_*>& dependencies() const {
            return dependencies_;
        }

        static Check_point *create(std::string name) {
            check_points_.insert({name, std::make_unique<Check_point>()}); // pair
            return get(name);
        }

        static auto insert_checkpoint(std::string name, std::unique_ptr<Check_point> c) {
            return check_points_.insert({name, std::move(c)});
        }

        static Check_point *get(std::string name) {
            Check_point * out = &*check_points_.at(name);
            return out;
        }
        static constexpr const char * default_name () {
            return "default check point";
        }
        virtual ~Check_point_(){}

        friend
        void reset(config c);

    private:
        bool enabled_ = false;
        std::string message_;
        std::vector<Check_point_*> dependencies_; // better weak pointers
        static std::map<std::string, std::unique_ptr<Check_point_>> check_points_; // better shared pointers
    };
    using Check_point = Check_point_<>;

    class Stopper : public Check_point {
    public:
        Stopper (): Check_point (/*enable = */true) {}

        /**
         * @todo This should be in-lined in the code with C macro because it is unfriendly to
         * stop the code execution inside this function.
         *
         * @param inline_flag: set this to false to disable this break point
         */
        void break_point(std::string file, int line, bool inline_flag = true) {

            if (!inline_flag) return;

            if (!enabled()) return;

            for (auto &d: dependencies()) {
                if (!d->enabled()) return;
            }

            char hostname[256];
            gethostname(hostname, sizeof(hostname));

            std::cerr << std::endl;
            std::cerr << "Breakpoint at " << file << ":" << line << std::endl;
            std::cerr << "Stopping until an external process triggers resume.\t" << std::endl;
            std::cerr << "PID " << getpid() << " on " << hostname << std::endl;
            std::cerr << message() << std::endl;
            std::cerr << std::endl;
            raise(SIGSTOP);
        }

        static Stopper *create(std::string name) {
            auto s = std::make_unique<Stopper>();
            insert_checkpoint(name, std::move(s)); // pair
            return get(name);
        }

        static Stopper *get(std::string name) {
            return dynamic_cast<Stopper*>(Check_point::get(name));
        }

        static constexpr const char* default_name () {
            return "default stopper";
        }
    };

    inline std::map<std::string, std::unique_ptr<Check_point>> default_checkpoints(){
        using namespace std;
        auto mp = std::map<std::string, std::unique_ptr<Check_point>> {};

        mp.emplace(Check_point::default_name(), new Check_point{false});

        mp.emplace(Stopper::default_name(), new Stopper);
        return mp;
    }
    // store the default break point
    template<int i> std::map<std::string, std::unique_ptr<Check_point_<i>>> Check_point_<i>::check_points_ = default_checkpoints();

    inline Check_point *check_point(std::string name = Check_point::default_name()) { return Check_point::get(name); }
    inline Stopper *stopper(std::string name = Stopper::default_name()) { return Stopper::get(name); }

    inline void reset(config c = default_config) {
        Check_point::check_points_ = default_checkpoints();
        if (c == stopper_depends_on_check_point)
            stopper()->depends_on_check_point();
    }
    inline void initialize(config c = default_config) {
        reset(c);
    }

}

#endif //CHECK_H
