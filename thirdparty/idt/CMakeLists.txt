
add_library(idt
        INTERFACE
        )

target_sources(idt
        INTERFACE
        idt.h
        )

target_include_directories(idt
    INTERFACE
        "${CMAKE_CURRENT_SOURCE_DIR}"
)