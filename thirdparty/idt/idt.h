//
// Created by Daisuke Sakurai on 2020/02/23.
//

#ifndef TOPOCULE_IDT_H
#define TOPOCULE_IDT_H

#include <iostream>

namespace idt {
    class Indent {
    public:
        Indent(int unit = 2): unit{unit} {}

        friend std::ostream& operator<<(std::ostream& o, const Indent& i) {
            std::string indent;
            int level = i.level;

            std::string unit (i.num_spaces(), ' ');

            while (level) {
                indent += unit;
                --level;
            }
            o << indent;
            return o;
        }
        void increase() {
            ++level;
        }
        void decrease() {
            --level;
        }
        int num_spaces() const {
            return level * unit;
        }
    private:
        int level = 0;
        int unit;
    };

    class Increase {
    public:
        Increase (Indent& indent) : indent{indent}
        {
            indent.increase();
        }
        ~Increase() {
            indent.decrease();
        }
    private:
        Indent& indent;
    };
}

//int main(int argc, char *argv[]) {
//	idt::Indent indent;
//	std::cerr << indent << 0 <<std::endl;
//	{
//		idt::Increase some {indent};
//		{
//			idt::Increase some {indent};
//			std::cerr << indent << 1 <<std::endl;
//		}
//	}
//	std::cerr << indent << 0 <<std::endl;
//}

#endif //TOPOCULE_IDT_H
