from conans import ConanFile, CMake

class TopoculeConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "boost/1.73.0", "gmp/6.2.1", "mpfr/4.1.0", "eigen/3.3.9" #, "openscenegraph/3.6.5"
    generators = "cmake_paths"
    # OpenSceneGraph is under test because its plugin system is a bit tricky for conan.
    # AFAIK OpenSceneGraph requires shared lib because of its plugin system.
    # Shared libs in conan are poorly maintained regarding the rpath, though.
    default_options = {"openssl:shared": True, "gmp:shared": True, "boost:shared": True, "mpfr:shared": True, "openscenegraph:shared": True}

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin") # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        # FIXME don't use auto-generation of conan
        cmake = CMake(self)
        cmake.configure()
        cmake.build()