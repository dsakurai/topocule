
add_library(triangle_vtk
    INTERFACE)

target_link_libraries(triangle_vtk
        INTERFACE
        Topocule_vtk
        )

add_data_info(
        triangle_vtk
        TRIANGLE_VTK_CONFIG

        "{
        \"directory\": \"${CMAKE_CURRENT_SOURCE_DIR}\",
        \"data_file\": \"${CMAKE_CURRENT_SOURCE_DIR}/triangle_vtk.vtp\",
        \"max_cell_dim\": 2,
        \"fields\": [\"PointIds\"]
        }"
)
