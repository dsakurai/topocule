//
// Created by Daisuke Sakurai on 2019/11/27.
//

#ifndef TOPOCULE_FOUR_POINTS_H
#define TOPOCULE_FOUR_POINTS_H

#include <array>
#include <vector>
#include <set>

struct four_points {
    // input points
    static auto vertexes () {
            return std::vector<std::array<double,2>>{
                    {0.0, 0.0}, // 0: 1, 2 connect
                    {1.0, 0.0}, // 1: 0, 2, 3
                    {0.0, 1.0}, // 2: 0, 1, 3
                    {1.0, 1.0}  // 3: 1, 2
            };
    }

    static auto edges () {
            return std::vector<int>{
                    0, 1, // edge 0
                    0, 2, // edge 1
                    1, 2, // ...
                    1, 3,
                    2, 3
            };
    };

    static auto vertex_adjaceny () {
            return std::vector<std::set<int>>{
                    {1, 2},
                    {0, 2, 3},
                    {0, 1, 3},
                    {1, 2}
            };
    }

    // A figure-y-shaped contour tree
    static auto field () {
        return std::vector<double>{
            2, 1, 0, 3
        };
    }
};

#endif //TOPOCULE_FOUR_POINTS_H
