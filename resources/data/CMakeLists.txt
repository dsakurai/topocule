add_subdirectory(toy)
add_subdirectory(mweber)

add_library(data
    INTERFACE
)

target_link_libraries(data
    INTERFACE
    toy
    pentane
)
